# Internet Forum Profile Text Analyser (BSC)

**This README explains how to set up the tool: Docker container, MUI and OF, etc.**

This tool is intended to get information about the interests, bio details, activities, etc. of a Stormfront participant by displaying evidences from their own posts.

## Table of Contents
- [Internet Forum Profile Text Analyser (BSC)](#internet-forum-profile-text-analyser-bsc)
  - [Table of Contents](#table-of-contents)
  - [How to set up](#how-to-set-up)
    - [Prerequisites](#prerequisites)
    - [Stand-alone operation](#stand-alone-operation)
    - [MUI integration](#mui-integration)
  - [MUI setup](#mui-setup)
    - [Import the tool](#import-the-tool)
    - [Run Test Cases](#run-test-cases)
  - [Maintainers](#maintainers)
  - [License](#license)

## How to set up

### Prerequisites

A docker server, as well as the docker-compose tool need to be installed on the host machine.

### Stand-alone operation

 * Create a new folder (e.g. ~/test) and cd to that folder.
 * Copy the *docker-compose.yml* file into the new folder.
 * Create a subfolder "ASGARD_DATA" in your new folder. You can use a different name, but then you have to update the docker-compose.yml accordingly.
 * Export the environment variable ASGARD_DATA_BASE_DIR to the ASGARD_DATA folder:

1. Login the ASGARD docker repository. (Skip if already logged in)

```bash
docker login docker-registry.darkwebmonitor.eu
```
2. Pull the docker image.

```bash
docker pull docker-registry.darkwebmonitor.eu/internet-forum-profile-text-analyser:20200615rc1stable
```

3. Test the container a <tt>docker run</tt> command:

```bash
docker run -d -p <port>:5000 -v /path/to/data/dir/:/ASGARD_DATA/ --name bsc-shelling docker-registry.darkwebmonitor.eu/internet-forum-profile-text-analyser:20200615rc1stable
``` 

### MUI integration

1. Install and setup the [Orchestrator-Framework+MUI framework](https://ci.tno.nl/gitlab/ASGARD/Integrated-OE_and_MUI).

This file could be used on its own to easily bootstrap a docker container with the provided configuration, or it can be used to copy its content into the Integrated OF and MUI docker-compose file and use the tool in combination with other tools from the MUI (see the README of the [Integrated OF and MUI](https://ci.tno.nl/gitlab/ASGARD/Integrated-OE_and_MUI#usage) for more information).

2. Copy the services of [docker-compose.yml](docker-compose.yml) into the docker-compose-tools.yml of your local Integrated-OE_and_MUI installation.

```yaml
version: '2.3'
services:
  iftpa-async:
    image: docker-registry.darkwebmonitor.eu/internet-forum-profile-text-analyser:20200615rc1stable
    container_name: internet-profile-text-analyser-corpus-generator-async
    ports:
      - "55500:5000"
    networks:
      conductornet:
        aliases:
          - bsc-iftpacg-async
    volumes:
      - ${ASGARD_DATA_BASE_DIR?Missing ASGARD_DATA_BASE_DIR env variable}:/ASGARD_DATA
    depends_on:
      - redis-async
    restart: always

############### GLOBAL STUFF ############################

  redis-async:
    image: redis:5.0.3
    networks:
      conductornet:
        aliases:
          - redis-server
    restart: always
```


3. Run the Orchestrator-Framework+MUI

```bash
./asgard-docker-compose.sh up -d --remove-orphans
```

## MUI setup
### Import the tool

In order to use any ASGARD tool from within the MUI, you need to configure it using the MUI itself. You can the tool at the MUI using its configurations file:

* [JSON configurations file - default](./BSCCNS_002-ifpta.json)
* [JSON configurations file - MIME Extension](./BSCCNS_002-ifpta-MIME.json)

You can import the Tool, using the following JSON configuration:

![Mui tools import](./screenshots/MUI_tools_import.png)

### Run Test Cases

See [User Guide](./README_user_part.md).

## Maintainers

| Email address                                     |
|---------------------------------------------------|
| [@Sergio Mendoza] (mailto:sergio.mendoza@bsc.es)  |
| [@Albert Farres] (mailto:XXXX@bsc.es)             |
| [@Joaquim More] (mailto:joaquim.morelopez@bsc.es) |

## License

[ASGARD License](LICENSE.md)
