# Internet Forum Profile Text Analyser (BSC)

**This README explains how to set up the tool: Docker container, MUI and OF, etc.**

This tool is intended to get information about the interests, bio details, activities, etc. of a Stormfront participant by displaying evidences from their own posts.

## Table of Contents
- [Internet Forum Profile Text Analyser (BSC)](#internet-forum-profile-text-analyser-bsc)
  - [Table of Contents](#table-of-contents)
  - [Introduction](#introduction)
  - [Objectives](#objectives)
  - [MUI](#mui)
    - [Run Test Cases](#run-test-cases)
      - [Input parameters](#input-parameters)
      - [Tool Execution](#tool-execution)
      - [Output expected](#output-expected)
  - [GUI](#gui)
    - [GUI: loading results](#gui-loading-results)
    - [GUI Interests](#gui-interests)
    - [GUI Topic Analysis](#gui-topic-analysis)
      - [GUI Topic Analysis - filtering](#gui-topic-analysis---filtering)
    - [GUI webinar](#gui-webinar)
    - [GUI Warnings](#gui-warnings)
  - [Maintainers](#maintainers)
  - [License](#license)

## Introduction
The internet-forum-profile-text-analyser (IFPTA henceforth) is a tool that filters huge amounts of texts in order to get data that are relevant to profile users of internet fora . The IFPTA tool is useful to model the users' opinions, get bio information and track the events they participate in. These are pieces of information that can even have a predictive value.
The document filtering is applied according to these criteria:

The topics the texts talk about
The sentiment polarity expressed in the documents (neutral, positive, negative)
The terms that are representative of the topics expressed in the documents
The events referred to in the documents

The IFPTA tool provides documents that are relevant according to one or more criteria in the document filtering.
As an example, let's assume that the IFPTA tool is able to detect a topic whose top relevant terms are related to weapons. Besides, let's assume that the user of the tool wants to read documents belonging to the 'weapon' topic whose polarity is negative and contains the top relevant terms as well. The IFPTA tool is able to provide these documents.
The IFPTA detects the events referred to in the documents. The event filtering and its interaction with the topic and polarity filtering is currently being discussed in developed. 

## Objectives
The tool objetives are:
- clustering documents by its contents
- obtain topics 
- obtain relevant terms of the topics
- find events referred to in the documents

The GUI was developed for accomplishing the objectives with an easy visualization of the results.

## MUI

### Run Test Cases

Requirements:
- "Team" already created (MUI > Teams > +Add)
- "Case" already created (MUI > Cases > +Add)

#### Input parameters

Go to "MUI > Cases > Tools" and select the internet-forum-profile-text-analyser tool:

1. Upload file. You will need using the **File Manager**.
2. Select input file using **Browse** .
3. **Start** execution.

The input file must: 
- contain raw text
- have document per line format: each line will be processed as a document by the tool.
- minumum 15 lines
- minimum 1000 words

It's a tool algorithm is for clustering big amount of documents. The algorithm is not for too small file content (e.g. 10 lines with 3 words per line).

An example of file would be [this](./docs/14Words_of_Truth_min.txt).

#### Tool Execution

Click the start button and wait until the task is Completed.
![Mui case tool](./screenshots/MUI_completed.png)

#### Output expected
Check the results clicking the "Show" button.
![output file](screenshots/MUI_gui_url.png)

The output contains the parameters:
* **gui-url**: is the URL for viewing results on the Browser. The data visualization it will be easier.

* **topics**: topics score that match the input file. Higher score, means more matching with the topic.

* **out-file**: is the path of the output file in JSON format, which contains an array of objects with text and identifier. [Here](docs/output_all.json.txt) you have the whole output data. File has *.txt extension and contents with json format.


```json
{
  "gui-url": "http://127.0.0.1.xip.io:55500/gui?infile=/ASGARD_DATA/case_1/output-14Words_of-20200612_15.33.03.json.txt",
  "out-file": "/ASGARD_DATA/case_1/output-14Words_of-20200612_15.33.03.json.txt",
  "topics": {
    "weapons": 0.1589743319235521,
    "drugs": 0.017538560979962176,
    "sex": 0.02390546849869259,
    "politics": 0.029426138523856046
  }
}
```

The **out-file** data has a structure, in JSON format, which contains an array of objects with text and identifier. It may look like that:
```
{
  "t0_p-1": {
    "posts": [
      {
        "text": "Now there has been an explosion in a restaurant during a music festival in Ansbach, at least one known dead, accident or muslim?",
        "doc_id": 14,
        "events": []
      }
    ],
    "relevant_terms": {
      "text": [
        "air",
        "white",
        "nazi",
        "real",
        "target",
        "handgun",
        "day",
        "merkel",
        "festival",
        "men"
      ],
      "prob": [
        0.016591068357229233,
        0.012675225734710692,
        0.01031050831079483,
        0.007890705950558186,
        0.007810298353433609,
        0.007749835960566997,
        0.0074717788957059375,
        0.007341957651078702,
        0.007334711030125619,
        0.007116819266229869
      ]
    }
  },
  ...
}
```
 
## GUI

### GUI: loading results

Open a Web Browser (e.g.Firefox) and access to ```http://<mui-server-ip>:55500/gui?infile=<file-path>```.
* The ```<file-path>```  must be the file path returned by the tool  as ```out-file```.
* Use the the ```gui-url``` parameter, returned after running the tool (e.g."http://127.0.0.1.xip.io:55500/gui?infile=/ASGARD_DATA/case_4/output-14Words_of-20191025_17.39.51.json.txt")



### GUI Interests
The tab "Interests" shows a tag-cloud with the interests of the user in the input file. Calculating a new score for the Internet Forum Profile Text Analyser. This score is the Rating by Interest Score Calculation of a score that rates the forum users according to his/her interest on weapons, drugs, sex and politics.

Bigger size, means more matching with the interest.

![GUI - internet forum profile text analyser](screenshots/MUI_gui_interests_html.png)

### GUI Topic Analysis
Text processing tool for clustering multiple documents by topic:
1. clusters the documents by topic
2. shows the topic terms used for clustering
3. allows filtering by polarity
4. detects events referred in the documents
5. most relevant documents according to the topics selected

* Note: Term-Cloud: Bigger term size, implies more relevance of the term in the selected topic.

![GUI - internet forum profile text analyser](screenshots/MUI_gui_lda.png)



#### GUI Topic Analysis - filtering

* The GUI offers filtering for the processed documents by:
- **Topic**: the documents are clusterized by topics.
- **Polarity**: positive/neutral/negative/
- **Text search**

* **relevants terms**: shown after the filtering options. Each topic has its relevant terms.
* **Term-cloud**: visualizacion of the terms by relevance. Bigger size means more relevance in the topic.
![output file](./screenshots/GUI_doc_1.png).

### GUI webinar
There's a short webinar about how to use the GUI: [video tutorial](tutorials/IFTAP-GUI-tutorial-v0.mp4) .

### GUI Warnings
The GUI is just for demonstration purposes, it is not secure and should only be used in a secure environment.

## Maintainers

| Email address                                     |
|---------------------------------------------------|
| [@Sergio Mendoza] (mailto:sergio.mendoza@bsc.es)  |
| [@Albert Farres] (mailto:XXXX@bsc.es)             |
| [@Joaquim More] (mailto:joaquim.morelopez@bsc.es) |

## License

[ASGARD License](LICENSE.md)