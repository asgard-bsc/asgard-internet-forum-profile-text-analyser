#!/usr/bin/env python
# -*- coding: utf-8 -*-

import nltk
from nltk.corpus import stopwords
from nltk import word_tokenize
from nltk.corpus import wordnet as wn
from nltk.stem.wordnet import WordNetLemmatizer

#Import spacy, an NLP module like NLTK but lighter and faster
import spacy       
#The Spacy English model (nlp) must be downloaded: python -m spacy download en_core_web_sm
import en_core_web_sm
import re

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

import modules.elasticasgard.asgard_post as asgpost

import codecs
import json

####### DICTIONARIES

freq = {} #Word frequency of words in English Wikipedia
rank = {} #Rank of words in English Wikipedia according to word frequency

nlp = 'nil'

####### STOPWORDS

stop = list(stopwords.words('english'))
stop = stop + ['https','www', 'rt', 'com', 'http', 'html', 'ww', 'pdf', 'ebook', 'mb', 'thank', 'password', 'email',
               'file', 'bestselling', 'page', 'guide', 'format']

####### TEXT TRANSFORMATIONS

def load_word_frequency(lang):
    with open('./DB/enwiki-20190320-words-frequency.txt') as ef:
             freq = {}
             rank = {}
             lines = ef.readlines()
             for i in range(len(lines)):                   
                 wfs = lines[i].strip().split()
                 freq[wfs[0]] = wfs[1]
                 rank[wfs[0]] = i
    return rank

def kw_sentence_rank(text):
    #Extraction of quite specfific nouns and proper nouns as keywords. The keywords joined will be the string
    #to vectorize in order to calculate its distance to the Silkroad topics.
    keyword = []
    pos_tag = ['PROPN', 'NOUN']#, 'VERB']
    doc = nlp(text.lower())
    for token in doc:
        if(token.text in nlp.Defaults.stop_words or token.text in punctuation):
            continue
        if token.lemma_ in rank:
            if rank[token.lemma_] > 1000 and token.pos_ in pos_tag:
                keyword.append(token.lemma_)
        #else:
        #    if re.match("^[a-z]+.*", token.lemma_):
        #        keyword.append(token.lemma_)        
              
    return ' '.join(keyword)

def prepare_post_string(process_file):
    sentences_posts = []
    with codecs.open(process_file, 'r', encoding='utf-8') as pf:
         ls = pf.readlines()
         for l in ls:
             sentences_posts.append(l.strip())
    kw_posts = [kw_sentence_rank(sp) for sp in sentences_posts]
    post_string = " ".join(kw_posts)
    #Sergio: Assegura't que kw_posts es un string
    return kw_posts

def prepare_topic_dataset():
    #Loading silkroad posts on weapons
    weapon_agora_df = pd.read_csv('./DBs/WEAPON-SILKROAD.csv', names = ['Vendor', 'Category', 'Item', 'Item Description', 
                                                                  'Price',  'Origin', 'Destination', 'Rating', 
                                                                  'Remarks'])

    weapon_posts = weapon_agora_df['Item Description'].tolist()
    #Loading silkroad posts on drugs
    drug_agora_df = pd.read_csv('./DBs/DRUGS-SILKROAD.csv', names = ['Vendor', 'Category', 'Item', 'Item Description', 'Price',
                                                      'Origin', 'Destination', 'Rating', 'Remarks'])
    drug_posts = drug_agora_df1['Item Description'].tolist()
    #Loading silkroad posts on sex
    sex_agora_df = pd.read_csv('./DBs/SEX-SILKROAD.csv', names = ['Vendor', 'Category', 'Item', 'Item Description', 'Price',
                                                      'Origin', 'Destination', 'Rating', 'Remarks'])
    sex_posts = sex_agora_df['Item Description'].tolist()                                                      
    #Loading silkroad posts on politics
    politics_agora_df = pd.read_csv('./DBs/POLITICS-SILKROAD.csv', names = ['Vendor', 'Category', 'Item', 'Item Description', 'Price',
                                                      'Origin', 'Destination', 'Rating', 'Remarks'])
    politics_posts = politics_agora_df['Item Description'].tolist()   
    print("EXTRACTING SILK ROAD WEAPON KEYWORDS")
    kw_wp_skrs = [kw_sentence_rank(s) for s in weapon_posts]
    print("EXTRACTING SILK ROAD DRUG KEYWORDS")
    kw_dg_skrs = [kw_sentence_rank(d) for d in drug_posts]
    print("EXTRACTING SILK ROAD SEX KEYWORDS")
    kw_sx_skrs = [kw_sentence_rank(sx) for sx in sex_posts]
    print("EXTRACTING SILK ROAD POLITICS KEYWORDS")
    kw_pp_skrs = [kw_sentence_rank(pp) for pp in politics_posts]
    weapons_string = " ".join(kw_wp_skrs)
    drugs_string = " ".join(kw_dg_skrs)
    sex_string = " ".join(kw_sx_skrs)
    politics_string = " ".join(kw_pp_skrs)
    return weapons_string, drugs_string, sex_string, politics_string

#MATCHING FILES HARDCORED

dict_match_files = {'matching_files': ['vWjw_GUBCp2DG5mgYe-V', 'xWjw_GUBCp2DG5mgYu8K', '0Gjw_GUBCp2DG5mgYu-I', '0Wjw_GUBCp2DG5mgYu-Q', '72jw_GUBCp2DG5mgY-_T', '92jw_GUBCp2DG5mgZO8Z', '-2jw_GUBCp2DG5mgZO8_', '_Wjw_GUBCp2DG5mgZO9X', '_mjw_GUBCp2DG5mgZO9s', 'A2jw_GUBCp2DG5mgZPCb']}

profiler_match_files_json = json.dumps(dict_match_files)

#CREATE INPUT FILE FOR TOPIC PROFILER

def get_text_from_file_ids(file_id):
    p = asgpost.Post.get(file_id)
    text = p.data
    return text

def create_file2process(matching_files):
    s = asgpost.PostSearch(using=asgpost.es_client, index='stormfrom', doc_type=asgpost.Post)
    texts = [get_text_from_file_ids(mf) for mf in matching_files]
    process_file = 'ASGARD-process-file.tmp'
    with open(process_file, 'w') as af:
         for t in texts:
             af.write(t + "\n")
    return process_file

#=====

#MAIN

#DISTANCE CALCULATION BETWEEN USER AND TOPIC POSTS

def calculate_distance(text1, text2):
    vectorizer_w = TfidfVectorizer(
      analyzer= 'word', stop_words=set(stop)
    )
    posts_slkr_w = vectorizer_w.fit_transform([text1, text2])
    post_p_v = posts_slkr_w[0].toarray()
    slkr_w_v = posts_slkr_w[1].toarray()
    distance = cosine_similarity(post_p_v, slkr_w_v)[0][0]
    return distance


def perform_profiling(process_file):
    #Prepare the strings to vectorize topic strings
    weapons_string, drugs_string, sex_string, politics_string = prepare_topic_dataset()
    #Prepare the strings to vectorize post string    
    posts_string = prepare_post_string(process_file)
    weapon_distance = calculate_distance(weapons_string, posts_string)
    drug_distance = calculate_distance(drug_string, posts_string)
    sex_distance = calculate_distance(sex_string, posts_string)
    politics_distance = calculate_distance(politics_string, posts_string)
    return weapon_distance, drug_distance, sex_distance, politics_distance

def topic_profiler(json_match_files):
    print ("INPUT_FILE_JSON:", json_match_files)
    data = json.loads(json_match_files)
    # Load configuration
    config_profiler = {'asgard': {'lang': ['en'],
                              'id': 'asg',
                              'num_topics': 7,
                              'default_stopwords': list(set(stopwords.words('english'))),
                              'project_stopwords': ['quote', 'posted', 'https', 'http'],
                              'default_stoptags': ['DT', 'IN', 'PRP', 'PRP$', 'CC', 'CD', 'MD', 'WRB', 'Fit', 'Fg', 'POS', 'RB', 'WP', 'TO', 'WDT'],
                              'lemmatizer': 'Wordnet Lemmatizer',
                              'profiler': 'yes' #We will see if this can be configured from the GUI
                             }
                  }
    print("PROFILER CONFIGURATION\n",config_profiler)
    # Let's put the match file contents in a single file
    process_file = create_file2process(data['matching_files'])
    # Let's perform the profiler from the process file 
    if config_profiler['asgard']['profiler'] == 'yes':
       lang =  config_profiler['asgard']['profiler']['lang'][0]
       if  lang == 'en':
          nlp = en_core_web_sm.load() 
          #Loading a word freq table from the English Wikipedia
          w_freq = load_word_frequency(lang)
          #Performance of the topic profiler
          weapon_distance, drug_distance, sex_distance, politics_distance = perform_profiling(process_file)
          #The output with the scores is saved in a csv file OR JSON AS YOU PREFER
          with open('PROFILING-SCORES.csv', 'w') as of:
              of.write('Weapon_Score\tDrug_Score\tSex_Score\tPolitics_Score\nweapon_distance, drug_distance, sex_distance, politics_distance')
       else:
          print("NO WORD FREQUENCY FILE FOR LANGUAGE", lang)
    else:
        print("NO PROFILING")
    #else:
    #We will see what to do


def main():
    topic_profiler(profiler_match_files_json)

if __name__ == "__main__":
    main()






  
