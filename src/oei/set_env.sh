#!/bin/sh
# Creates the needed .env file

echo "Enter the IP used to access the framework (i.e. internal IP to access from LAN, external IP for WAN; defaults to localhost):"
read NGINX_IP
echo "Enter the port (defaults to 80 if empty):"
read NGINX_PORT

cat >.env << EOF
#############################################
### Environment variables for the compose ###
#############################################
NGINX_IP=${NGINX_IP:-127.0.0.1}
NGINX_PORT=${NGINX_PORT:-80}

# Endpoints definition
FORENSICS_ENDPOINT=
FORESIGHT_ENDPOINT=
INTELLIGENCE_ENDPOINT=
KEXPORT_ENDPOINT=http://kexport.${NGINX_IP:-127.0.0.1}.xip.io:${NGINX_PORT:-80}
GITLAB_URL=https://ci.tno.nl/gitlab/ASGARD/kexport/blob/master/README.md

# Docker logs environment variables
DOCKER_LOGS_PROJECT_DIR=$(pwd)

# Base dir for volumes
ASGARD_DATA_BASE_DIR=./ASGARD_DATA

# Tool specific
COMPOSE_HTTP_TIMEOUT=120
ASGARD_CONTAINER_DIR=/ASGARD_DATA
EOF
