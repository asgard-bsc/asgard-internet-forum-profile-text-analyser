#!/bin/bash
# Simple wrapper to avoid having to write -f .. -f ..
docker-compose -f docker-compose-base.yml -f docker-compose-tools.yml "$@"