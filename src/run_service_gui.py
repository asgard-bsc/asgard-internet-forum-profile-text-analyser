#!/usr/bin/env python

import chardet
import web
import sys
import os
import errno
import warnings
import json
import subprocess
from inspect import currentframe, getframeinfo

from datetime import datetime

class log:
    debug = True

    @staticmethod
    def dbg(msg):
        if log.debug:
            cf = currentframe()
            filename = getframeinfo(cf).filename
            print("DEBUG %s:%03d -- %s" % (filename, cf.f_back.f_lineno, msg))
            sys.stdout.flush()

    @staticmethod
    def msg(msg):
        print(msg)
        sys.stdout.flush()

    @staticmethod
    def msg_noendl(msg):
        print(msg, sys.stdout.flush())


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else:
            raise

def process_file(fname):
    mkdir_p(semgraph_in_dir)
    command = "analyzer_client %d" % (shelling_port)
    with open(freeling_in_dir + "/" + fname, 'r') as infile, open(semgraph_in_dir + "/" + fname + ".xml", 'w') as outfile:
        subprocess.call(command, shell=True, stdin=infile, stdout=outfile)


warnings.simplefilter('ignore')

urls = (
    "/gui", "MyGUI",
    "/viz", "MyGUI",
    "/app/(.*)", "ImageDisplay",
      #"/(.*)", "MUIServiceLisboa"
)

app = web.application(urls, globals())


class MyGUI:
    default_html = """
        <html><body><h1>The Stormfront UI should go HERE!</h1>
        <p>This is the default web page for this server.</p>
        <p>The web server software is running but no content has been added, yet.</p>
        </body></html>
        """

    def __init__(self):
        self.render = web.template.render('gui/')

    def get_files_events(self, fpath):
        with open(fpath) as json_file:
            data = json.load(json_file)
        return json.dumps(data)

    def GET(self):
        topic = 0
        polarity = -1

        #text_list = self.get_file(topic, polarity)
        # return self.render.index("The RESULTS must go HERE!", text_list)
        #fpath = None
        
        # default file path (symbolic_link to the last execution)
        fpath = './output/last-output.json'

        data = web.input()
        if "infile" not in data:
            #fpath = "./output/gui-default-events.json"
            fpath = './output/last-output.json'
        else:
            fpath = data.infile

        if not os.path.exists(fpath):
            return "File '"+fpath+"' cannot be found."

        print("File to be visualized: ", fpath)
        text_lists = self.get_files_events(fpath)
        #text_list = self.get_file(topic, polarity)
        '''
        import html.parser as htmlparser
        parser = htmlparser.HTMLParser()
        parser.unescape(text_lists)
        '''
        text_list = {}
        print(type(text_lists))
        # , text_lists.keys, text_lists.values)
        return self.render.index("The RESULTS must go HERE!", text_list, text_lists)

    def POST(self):
        web.header('Access-Control-Allow-Origin', '*')
        web.header('Access-Control-Allow-Credentials', 'true')
        return json.dumps({'message': 'GET OK!'})


class ImageDisplay(object):
    def GET(self, fileName):
        imageBinary = open(fileName, "rb").read()
        return imageBinary

if __name__ == "__main__":
    app.run()
