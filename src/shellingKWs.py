import time
import sys
import getopt

import shelling as sh
import shelling_phrase as shp
from nltk.corpus import wordnet as wn
import json
import spacy
import en_core_web_sm


class shellingKWS:
    # Testing whether the term is a nominal term
    def isnp(self, t):
        v = False
        # If multiword, the term is assumed to be a nominal not included in Wordnet yet
        if ' ' in t:
            v = True
        # if the term has no synset in Wordnet, it is assumed to be a nominal not in Wordnet yet
        elif wn.synsets(t) == []:
            v = True
        # If a multiword
        else:
            try:
                # If a general synset exists when the term is nominal, then the term is nominal
                wn.synset(t + '.n.01')
                v = True
            except:
                pass
        return v

    # Get named entities from sentence texts whose type is stated in the configuration file (config['project']['ner'])

    def get_nes(self, sentence, config, stopwords, nlp):
        tags = config['project']['ner']
        ne_sentences = []
        if len(tags) > 0:
            doc = nlp(sentence)
            dt = [(X.text, X.label_) for X in doc.ents if X.label_ in tags]
            for ne in dt:
                ne = ne[0].lower()
                if ' ' in ne:
                    nesw = ne.split(' ')
                    if nesw[0] in stopwords:
                        nesw = nesw[1:]
                    if nesw[-1] in stopwords:
                        nesw = nesw[:-1]
                    ne_sentences.append("_".join(nesw))
                else:
                    ne_sentences.append(ne)
        return ne_sentences

    # Getting np keyword terms from a list of phrases

    def get_np_keywords(self, sentence_phrases, config, stopwords):
        keywords = []
        # Getting nominal keywords
        nps = [ph.replace(' ', '_')
               for ph in sentence_phrases if self.isnp(ph) == True]
        keywords = nps
        return keywords

    def kws2json(self, nps_in_sentences, ne_in_sentences, config,):
        if config['project']['docline'] == 'yes':
            dict_kw = {'nps_in_sentences': nps_in_sentences}
            if len(config['project']['ner']) > 0:
                dict_kw['ne_in_sentences'] = ne_in_sentences
            return dict_kw
        else:
            return {}

    def get_kws(self, config, outputfile):
        dict_kw = self.get_kws(config)
        with open(outputfile, 'w') as outjsonfile:
            json.dump(dict_kw, outjsonfile)

    def get_kws(self, config):
        """
        Extrae keywords de un fichero de texto.
        Keywords son:
        - sintagmas nominales (SN)
        - named entities (NE) del tipo o tipos establecidos por el usuario (EVENT, LOC, etc.)

        Args:
            config (json): json with the project execution configuration

        Returns:
            json : json with the noun phrases (SN) and namned entities (NE)
        """
        stopwords = []
        # Get stopwords_list
        if config['project']['default_stopwords'] == 'yes':
            stopwords = sh.get_default_stopwords(config['project']['lang'])
        stopwords = stopwords + config['project']['project_stopwords']
        # Get stoptags
        stoptags = config['project']['default_stoptags']
        # Get lemmatizer
        if config['project']['lemmatizer'] == 'Wordnet Lemmatizer':
            lemmatizer = 'Wordnet_Lemmatizer'
        print("BUILDING PHRASE MODEL")
        file = config['project']['matching_files'][0]
        # Creating the term phrase model for the documents in the file. Phrases appear more than a freq threshold.
        # Returning the phrases and the sentences tokenized as well
        sentences_tokenized, phrases, sentences = shp.build_phrase_model_from_file(
            file, config)
        print("PHRASE MODEL BUILT")
        # Each line is a document
        if config['project']['docline'] == 'yes':
            print("TRANSFORMING FILE. EACH LINE EQUALS A DOCUMENT")
        # The sentences are transformed to a list of good lemmatized terms taking the phrase model as a resource
            start = time.time()
            transformed_sentences = [shp.transform_sentence(
                st, phrases, stopwords, stoptags, lemmatizer) for st in sentences_tokenized]
            print("SENTENCES TRANSFORMED. ", (time.time()-start)," sec.")
        # Creating a list of np terms for each sentence
            start = time.time()
            print("GETTING NP KEYWORDS")
            # if len(sh.get_nominals(ts)) > 0]
            nps_in_sentences = [self.get_np_keywords(
                ts, config, stopwords) for ts in transformed_sentences]
            print("GETTING NP KEYWORDS. Finished.", (time.time()-start), " sec.")
        # Creating a list of named entities for each sentence whose type is stated in the configuration file
            start = time.time()
            nlp = en_core_web_sm.load()
            print("en_core_web_sm.load()", time.time() - start, " sec.")

            start = time.time()
            print("GETTING NAMED ENTITIES")
            ne_in_sentences = []
            if len(config['project']['ner']) > 0:
                ne_in_sentences = [self.get_nes(s, config, stopwords, nlp)
                                   for s in sentences]

            print("GETTING NAMED ENTITIES. Finished.", (time.time()-start), " sec.")

        # Creating the output: JSON file with the keywords of each sentence
            start = time.time()
            out_json = self.kws2json(nps_in_sentences, ne_in_sentences, config)
            print("kws2json. Finished:", (time.time()-start), " sec.")
            return out_json
        else:
            return {}

    def main(self, config_json_str, argv):
        inputfile = ''
        outputfile = ''
        try:
            opts, args = getopt.getopt(argv, "hi:o:", ["ifile=", "ofile="])
        except getopt.GetoptError:
            print(
                'ERROR: shellingKWs.py -i <CONFIGURATION FILE.json> -o <OUTPUT_FILE.json>')
            sys.exit(2)
        for opt, arg in opts:
            if opt == '-h':
                print(
                    'shellingKWs.py -i <CONFIGURATION FILE.json> -o <OUTPUT_FILE.json>')
                sys.exit()
            elif opt in ("-i", "--confile"):
                confile = arg
            elif opt in ("-o", "--ofile"):
                outputfile = arg
        print('Configuration file is "', confile)
        print('Output file is "', outputfile)
        with open(confile) as f:
            self.loaded_confile = json.load(f)
            self.get_kws(loaded_confile, outputfile)


if __name__ == "__main__":
    shKWs = shellingKWs()
    shKWs.shellingKWs_exec()
