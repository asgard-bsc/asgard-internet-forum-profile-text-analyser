import time
import codecs
import re
import nltk
from nltk.corpus import wordnet as wn
from nltk.stem.wordnet import WordNetLemmatizer
from nltk import word_tokenize
import pandas as pd

# Dictionary: key is the lemma, values are the forms of this lemma
lem_forms = {}


# Getting unigram and bigramp phrases
def build_phrase_model_from_file(process_file, config):
    min_count = config['project']['threshold_freq_terms']
    print("FILE TO PROCESS", process_file)
    import gensim
    import gensim.corpora as corpora
    from gensim.models.phrases import Phraser
    from gensim.models import Phrases
    # List of sentences from the process file
    sentences = []
    # useful ?

    # A) option for simple docline
    with open(process_file, 'r') as pf:
        ls = pf.readlines()
        for l in ls:
            sentences.append(l.strip())

    '''
    # B) option for csv with stormfront id (corpus/csv_55084.csv)
    docs = pd.read_csv(process_file)
    ls = docs['contents'].tolist()
    for l in ls:
        l = str(l)
        sentences.append(l.strip())
    '''
    # sentence_stream: list of lists of tokens in each sentence
    sentence_stream = [word_tokenize(sentence.lower())
                       for sentence in sentences]
    # List of lists of phrases from the sentence stream
    phrases = Phrases(sentence_stream, threshold=2,
                      delimiter=b' ', min_count=min_count)
# threshold (float) – The minimum score for a bigram to be taken into account
# min_count – Ignore all words and bigrams with total collected count lower than this value
    return sentence_stream, phrases, sentences


# Testing whether a phrase is ok; that is, a phrase with alphabetical characters, with no stopwords at the beginning and the end
# with PoS tags that can be present at the beginning and the end
def good_candidate(t, postag, stopwords, no_pos_in):
    v = False
    # If multiword phrase
    if ' ' in t:
        # Phrase token list
        tl = t.split(' ')
        # the initial and final tokens must be alphabetical and cannot be in the stopword list...
        if re.match("^[a-z]+.*", tl[0]) and re.match("^[a-z]+.*", tl[-1]) and tl[0] not in stopwords and tl[1] not in stopwords:
            # ... and their PoS cannot be present in the no_pos_in list (PoS tags not to be in the first and final words)
            if postag[0][1] not in no_pos_in and postag[-1][1] not in no_pos_in:
                v = True
    # If uniword phrase
    else:
        # the phrase must be alphabetical, and the uniword cannot be in the stopwords list...
        if t not in stopwords and re.match("^[a-z]+.*", t):
            # and its PoS cannot be present in the no_pos_in list
            if postag[0][1] not in no_pos_in:
                v = True
    return v


# Unifying variations of the same term
def unify(l):
    term_unified = l
    if l == 'mr. trump' or l == 'donald trump':
        term_unified = 'trump'
    elif l == 'dpt':
        term_unified = 'department'
    return term_unified


# Tag the PoS of a word with the Wordnet PoS format
def get_wn_pos(pos):
    if re.match(r'^N', pos):
        wn_pos = 'n'
    elif re.match(r'^V', pos):
        wn_pos = 'v'
    else:
        wn_pos = 'n'  # In English, the lemmas of terms that are not verbs and nouns have the same PoS as nouns
    return wn_pos


# Lemmatize phrases according to their PoS tags with the WordNet lemmatizer and relate the lemmas with their forms
def wnlemmatize(t, postag):
    lemma = ""
    t = t.replace(' ', '_')
    # Lemmatizer definition",
    lem = WordNetLemmatizer()
    # If uniword, its lemma is obtained with the WordNet lemmatizer according to its Po
    if '_' not in t:
        lemma = lem.lemmatize(t, get_wn_pos(postag[0][1]))
    # If multiword, its lemma is obtained as if it were a noun, applying the WordNet lemmatizer
    else:
        lemma = lem.lemmatize(t, 'n')
    # Relating lemma-forms in the lem_forms dictionary
    lem_forms.setdefault(lemma, []).append(t)
    return lemma


# Transform sentences into term phrases
# TODO: better performance?
def transform_sentence(sentence_tokenized, phrases, stopwords, stoptags, lemmatizer):
    # Getting the terms model from the sentences of the documents
    sentence_terms = phrases[sentence_tokenized]
    # Stripping the terms of the sentence terms
    # st.encode('utf8', 'replace') python2.7
    stripped_candidates = [st.strip('\".,;:-():!?- ') for st in sentence_terms]
    # Terms are unified
    terms_unified = [unify(sc).lstrip(' ') for sc in stripped_candidates]
    if lemmatizer == 'Wordnet_Lemmatizer':
        # Lemmatizing terms with the Wordnet lemmatizer if terms are ok
        sentence_transformed = [wnlemmatize(ut, nltk.pos_tag(word_tokenize(ut))) for ut in terms_unified if good_candidate(
            ut, nltk.pos_tag(word_tokenize(ut)), stopwords, stoptags) == True]
    elif lemmatizer == '':
        sentence_transformed = terms_unified
    return sentence_transformed
