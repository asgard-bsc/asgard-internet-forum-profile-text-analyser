"""

Service interfaces

The following service endpoints are defined in this script:

A) For enqueuing a new task:
   An HTTP POST at http://DOCKER_SERVER_IP:POST/task
   with a Content-type: application/json
   and a JSON POST body like the following:

    - "operation":                      "REQ_START"
    - "reason":                         "Start tool processing"
    - "data.taskId":                    taskId, e.g. "9fb1ab02-f48b-11e8-8eb2-f2801f1b9fd1"
    # ! # If you provide a brokerInfo object, the tool will behave asynchronously (ASYNC), else synchronously (SYNC)
    - "data.brokerInfo.brokerURL":      broker URL, e.g. "stomp://activemq:61616"
    - "data.brokerInfo.brokerQueue":    broker queue, e.g. "queue-1234567"
    - "data.brokerInfo.brokerUser":     broker user, e.g. "username"
    - "data.brokerInfo.brokerPassword": broker password, e.g. "password"
    # ! # The next parameters are application specific
    - "data.location":                  path to an image or video
    - "data.outputFolder":              a folder to write results to

  The POST call returns one of the following self-explanatory JSONs:

    202 {"operation": "RES_STARTED", "reason": "Execution accepted"}
    400 {"operation": "RES_FAILED", "reason": "Execution rejected", "message": "operation, reason, data.taskId and data.brokerInfo must be provided"}

  If it was a SYNC call, then the tool will wait until the end of the processing and then return something like:
    200 {"operation": "RES_COMPLETED", "reason": "Finished", "data": {"outputFile": "/ASGARD_DATA/result/test_out.mp4"}}

B) For retrieving the status of an already posted task:
   An HTTP GET at http://DOCKER_SERVER_IP:POST/task/:taskId:
   where :taskId: the task ID of the task to be queried.

   The GET call returns one of the following self-explanatory JSONs:

    200 {"operation": "RES_FAILED", "reason": "Task id not found", "message": "Task id not found"}
    200 {"operation": "RES_FAILED", "reason": "Execution rejected", "message": "a non-empty location must be provided"}
    200 {"operation": "RES_FAILED", "reason": "Invalid input", "message": "Image dimensions too big"}
    200 {"operation": "RES_STARTED", "reason": "Execution accepted"}
    200 {"operation": "INF_PROCESSING", "reason": "Execution started"}
    200 {"operation": "INF_PROCESSING", "reason": "Video frames are processed", "message": "Video frames are processed", "percentage": 0.9023, "eta": 124}
    200 {"operation": "RES_COMPLETED", "reason": "Finished", "data": {"outputFile": "/ASGARD_DATA/result/test_out.mp4"}}
    200 {"operation": "RES_CANCELLED", "reason": "Execution cancelled"}

C) For cancelling an already posted task:
   An HTTP DELETE at http://DOCKER_SERVER_IP:POST/task/:taskId:
   where :taskId: the task ID of the task to be cancelled

   The DELETE call returns one of the following self-explanatory JSONs:

    200 {"operation": "RES_FAILED", "reason": "Task id not found", "message": "Task id not found"}
    200 {"operation": "RES_CANCELLED", "reason": "Execution cancelled"}

"""


from flask import Flask, render_template
from flask import jsonify, request
import json
import logging

logging.basicConfig(level=logging.INFO)

app = Flask(__name__, static_url_path='')

logging.info("Tool class initialized")


@app.route('/odd', methods=['GET'])
def post_task():
    logging.info("post called")

    infile = request.args.get('infile')

    import BSCoddness as bso
    input_file = './input/oddness/TOSCOREODDNESS.txt'
    #input_file = './input/oddness/DRUGS-TEXTS.txt'
    lang = 'en'
    output_file = './input/oddness/SCOREODDNESS.csv'
    bso.oddness_score_text_file(input_file, 'en', output_file)

    import pandas as pd
    # reading csv file
    pd.set_option('display.max_colwidth', -1)
    df = pd.read_csv(output_file, sep='\t', names=["doc_id", "contents", "oddness"], index_col="doc_id")
    df_html = df.to_html()

    return render_template("oddness/index.html", df_html=df_html)


logging.info("Listener started")

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5088)
