import sys
import getopt
import json
import umap
import numpy as np
import pandas as pd
import gensim
import gensim.corpora as corpora
from gensim.models.ldamodel import LdaModel


def get_doc_topic_dist(model, corpus, num_topics, kwords=False):
    '''
    LDA transformation, for each doc only returns topics with non-zero weight
    This function makes a matrix transformation of docs in the topic space.
    '''
    import numpy as np
    from collections import OrderedDict
    top_dist = []
    keys = []
    for d in corpus:
        tmp = {i: 0 for i in range(num_topics)}
        tmp.update(dict(model[d]))
        vals = list(OrderedDict(tmp).values())
        top_dist += [np.array(vals)]
        if kwords:
            keys += [np.array(vals).argmax()]

    return np.array(top_dist), keys


def filter_hm_topics_terms(ldamodel, ldacorpus, hm, keys, terms):
    hm_filtered = []
    id_filtered = []
    topics_filtered = []
    terms_filtered = []
    for i in range(len(hm)):
        sorted_hm = -np.sort(-hm[i])
        # If the score of the most probable topic of a document is > 0.5
        if sorted_hm[0] > 0.5:
            # Put the score in the hm_filtered list
            hm_filtered.append(hm[i])
            # Put the most probable topic in the topics_filtered list
            d_topic = keys[i]
            id_filtered.append(i)
            topics_filtered.append(d_topic)
            indexes = [vi[0] for vi in ldacorpus[i]]
            terms1 = [terms[ind] for ind in indexes]
            terms_filtered.append(" ".join(terms1))
    return hm_filtered, id_filtered, topics_filtered, terms_filtered


def filter_hm_topics_terms_old(ldamodel, ldacorpus, hm, keys, terms):
    hm_filtered = []
    topics_filtered = []
    terms_filtered = []
    dict_term = {}
    dict_prob = {}
    for i in range(len(hm)):
        sorted_hm = -np.sort(-hm[i])
        # If the score of the most probable topic of a document is > 0.5
        if sorted_hm[0] > 0.5:
            # Put the score in the hm_filtered list
            hm_filtered.append(hm[i])
            # Put the most probable topic in the topics_filtered list
            d_topic = keys[i]
            topics_filtered.append(d_topic)
            # The terms of the document whose most probable topic score is > 0.5 are put in the terms_filtered list
            indexes = [vi[0] for vi in ldacorpus[i]]
            terms1 = [terms[ind] for ind in indexes]
            keywords1 = []
            for kw1 in terms1:
                prob = ldamodel.get_term_topics(
                    kw1, minimum_probability=0.000001)
                if len(prob) > 0:
                    sort_kw = sorted(
                        prob, key=lambda tup: tup[1], reverse=True)
                    term_topic = sort_kw[0][0]
                    term_prob = sort_kw[0][1]
                    if d_topic == term_topic:
                        # The dict_term dictionary relates the term with its most probable topic
                        dict_term[kw1] = d_topic
                        # The dict_prob dictionary relates the term with its probability to belong to the topic
                        dict_prob[kw1] = term_prob
                        keywords1.append(kw1)
            terms_filtered.append(" ".join(keywords1))
    return hm_filtered, topics_filtered, terms_filtered, dict_term, dict_prob


def doc_topic_frame(reduced_data, id_filtered, topics_filtered):
    embedding = pd.DataFrame(reduced_data, columns=['x', 'y'])
    embedding['id'] = id_filtered
    embedding['label'] = topics_filtered
    return embedding


def term_topic_frame(ldamodel, topics_filtered, terms_filtered):
    d1 = pd.DataFrame()
    d1['topics'] = topics_filtered
    d1['terms'] = terms_filtered
    print(d1)
    topics_op = list(set(topics_filtered))
    # print(topics_op)
    # Preparing the frame
    columns_d2 = ['Term']
    for i in range(len(topics_op)):
        columns_d2.append('Fq-' + str(i))
    for i in range(len(topics_op)):
        columns_d2.append('Tfidf-' + str(i))
    for i in range(len(topics_op)):
        columns_d2.append('Prob-' + str(i))
    # Preparing the term documents
    topic_documents = []
    for i in range(len(topics_op)):
        d3 = d1[d1['topics'] == topics_op[i]]
        topic_documents.append(" ".join(d3['terms'].tolist()))
    # Get the count-vector and tfidf-vector matrix
    from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
    count_vectorizer = CountVectorizer(analyzer='word')
    Xc = count_vectorizer.fit_transform(topic_documents)
    tfidf_vectorizer = TfidfVectorizer(analyzer='word')
    Xt = tfidf_vectorizer.fit_transform(topic_documents)
    matrix_terms = count_vectorizer.get_feature_names()
    Xc_array = Xc.toarray()
    Xt_array = Xt.toarray()
    info = {}
    for i in range(len(matrix_terms)):
        w = matrix_terms[i]
        try:
            prob = ldamodel.get_term_topics(w, minimum_probability=0.000001)
            freq_topics = []
            tfidf_topics = []
            for j in range(0, Xc_array.shape[0]):
                freq_topics.append(Xc_array[j][i])
                tfidf_topics.append(Xt_array[j][i])
            probs = []
            pt = {}
            for t, p in prob:
                pt[t] = p
            for it in range(len(topics_op)):
                if it in pt:
                    probs.append(pt[it])
                else:
                    probs.append('0.0')
            freq_tfidf_prob = [w] + freq_topics + tfidf_topics + probs
            info[w] = freq_tfidf_prob
        except:
            pass
    d2 = pd.DataFrame.from_dict(info, orient='index', columns=columns_d2)
    return d2


def create_lda_dataframe(ldamodel, ldacorpus, ntopics, terms, doc_output_file, term_output_file):
    hm, keys = get_doc_topic_dist(ldamodel, ldacorpus, ntopics, True)
    # Filtering documents which are very likely to belong to a topic. We get
    # hm_filtered: The topic probability scores
    # topics_filtered: The most probable topics
    # terms_filtered: The terms most likely to belong tot the topics
    # dict_term: Dictionary where the document terms are related to their topic
    #hm_filtered, topics_filtered, terms_filtered, dict_term, dict_prob = filter_hm_topics_terms(ldamodel,ldacorpus,hm,keys,terms)
    hm_filtered, id_filtered, topics_filtered, terms_filtered = filter_hm_topics_terms(
        ldamodel, ldacorpus, hm, keys, terms)
    import umap
    X = hm_filtered
    reduced_data = umap.UMAP(n_neighbors=5,
                             min_dist=0.3,
                             metric='euclidean').fit_transform(X)
    topic_df = doc_topic_frame(reduced_data, id_filtered, topics_filtered)
    topic_df.to_csv(doc_output_file)
    term_df = term_topic_frame(ldamodel, topics_filtered, terms_filtered)
    term_df.to_csv(term_output_file)

    return topic_df, term_df

# Merge NPs and named entities in a keyword list


def merge_nps_nes(nps_in_sentences, nes_in_sentences):
    keyword_sentences_merged = []
    for i in range(len(nps_in_sentences)):
        keyword_sentence_merged = nps_in_sentences[i] + nes_in_sentences[i]
        keyword_sentences_merged.append(keyword_sentence_merged)
    return keyword_sentences_merged

# Creation of the LDA model


def perform_lda(kw_sents, ntopics):
    id2word = corpora.Dictionary(kw_sents)
# Create Corpus
    texts = kw_sents
    # print(texts)
# Term Document Frequency
    corpus = [id2word.doc2bow(text) for text in texts]
    # Terms: token list corresponding to index terms
    terms = []
    for i in range(len(id2word)):
        terms.append(id2word.get(i))
    # Model creation
    print("Making LDA model with", ntopics, "topics")
    ldamodel = gensim.models.ldamodel.LdaModel(corpus,  # Term frequency in each document
                                               num_topics=ntopics,  # Topic number
                                               random_state=1,  # Pre-definied initial value to maintain coherence
                                               id2word=id2word,  # Vocabulary
                                               chunksize=1000,
                                               passes=1)  # The more pases, the more consistency
    return ldamodel, corpus, terms


def lda(json_kws, ntopics, doc_output_file, term_output_file):
    nps = json_kws['nps_in_sentences']
    # Create Dictionary
    if ('ne_in_sentences' in json_kws.keys()):
        nes = json_kws['ne_in_sentences']
        if len(nps) == len(nes):
            # merged_nps_nes is an array of arrays: each array contains the keywords of a document. E.g: [['white_genocide', 'jew', 'feminist], ['texas', 'bush'],
            # [...], ...]
            # ntopics: number of topics
            merged_nps_nes = merge_nps_nes(nps, nes)
    else:
        merged_nps_nes = nps
        print("CANNOT PAIR NPs AND NEs IN SENTENCES")

    # The output of the LDA creation is the model, the corpus the model is based on and the terms of the model
    ldamodel, ldacorpus, terms = perform_lda(merged_nps_nes, ntopics)
    # Export LDA info to dataframe
    topic_df, term_df = create_lda_dataframe(ldamodel, ldacorpus, ntopics,
                                             terms, doc_output_file, term_output_file)

    return topic_df, term_df


def main(argv):
    output_doc_topic_file = ''
    output_term_topic_file = ''
    try:
        opts, args = getopt.getopt(
            argv, "hi:k:d:t:", ["ifile=", "k=", "ofile=", "otfile="])
    except getopt.GetoptError:
        print('ERROR: shellingLDA.py -i <FILE_WITH_DOCUMENT_KEYWORDS.json> -k <NUMBER OF TOPICS> -d <DATAFRAME-DOCS-TOPICS.csv/json> \
             -t <DATAFRAME-TERMS-TOPICS.csv/json')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('shellingLDA.py -i <FILE_WITH_DOCUMENT_KEYWORDS.json> -k <NUMBER OF TOPICS> -d <DATAFRAME-DOCS-TOPICS.csv/json>\
                 -t <DATAFRAME-TERMS-TOPICS.csv/json')
            sys.exit()
        elif opt in ("-i", "--ifile"):
            kwfile = arg
        # K: number of topics
        elif opt in ("-k", "k"):
            k = int(arg)
        elif opt in ("-d", "--ofile"):
            output_doc_topic_file = arg
        elif opt in ("-t", "--otfile"):
            output_term_topic_file = arg
    print('Documents file is ', kwfile)
    print('Dataframe file for doc-topic relations is', output_doc_topic_file)
    print('Dataframe file for term-topic relations is', output_term_topic_file)
    with open(kwfile) as f:
        # The keywords in the JSON file are loaded
        loaded_docs = json.load(f)
    lda(loaded_docs, k, output_doc_topic_file, output_term_topic_file)


if __name__ == "__main__":
    main(sys.argv[1:])
