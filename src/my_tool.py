"""

This is the actual tool class with the application specific code.
During class construction, it initializes the model and starts a new thread.
This thread listens to Redis (through the Communicator) for new tasks.
When a new task arrives, it is sent to the appropriate processing function.
The Communicator interfaces are used for updating Redis and notifying the orchestrator queue.

Functions:
__init__             : class constructor
mkdir_p              : help function
process_task         : main thread code, listens to Redis, distribute task
process_file         : processing function for a file

Important points:
- Use Communicator() to instantiate the Communicator class
- Use self.comm.getNextTask() to listen for the next task (BLOCKING)
- Use self.comm.send_inf_processing_message(...) to indicate a task status update (e.g. ETA messages)
- Use self.comm.send_res_failed_message(...) to indicate a task failure
- Use self.comm.isCancelled(taskId) to check if a task has been cancelled
- Use self.comm.send_res_cancelled_message(...) to indicate a task cancelation
- Use self.comm.send_res_completed_message(...) to indicate a task completion

"""
import asyncio
import os
import errno
from datetime import timedelta
import time
import json
import glob
import threading
import logging
import traceback
from communicator import Communicator

logging.basicConfig(level=logging.INFO)

# ! # Use your own SRS name
SRS = "BSC_001"

# ! # Every how many seconds should an updated ETA message be sent?
SEND_ETA_EVERY_SECS = 10

# Set the signal handler and a 5-second alarm


def state_actual(self, taskId=1):
    isCancelled = self.comm.isCancelled(taskId)
    if isCancelled and str(isCancelled) == "True":
        self.comm.send_res_cancelled_message(
            taskId, "Execution cancelled")
    else:
        self.comm.send_inf_processing_message(
            taskId, "Task is processed", "Still processing", 0, 100)
    return


class MyTool():

    def __init__(self):
        logging.info("Entering init")

        self.worker = threading.Thread(
            target=self.process_task, args=()).start()
        logging.info("Thread started")

    def mkdir_p(self, path):
        try:
            os.makedirs(path)
        except OSError as exc:
            logging.info(traceback.format_exc())
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                pass
            else:
                raise

    def validate_input_data(self, data):
        logging.info("validate_input_data()....")
        taskId = data["taskId"]
        logging.info(">>>>>>>>>>>>"+str(isinstance(data["input_path"], str)))
        if "input_path" in data and data["input_path"] and isinstance(data["input_path"], str) and data["input_path"].strip():
            if not os.path.exists(data["input_path"]):
                self.comm.send_res_failed_message(
                    taskId, "Execution rejected", "input file does not exist")
                return False
        else:
            self.comm.send_res_failed_message(
                taskId, "Execution rejected", "empty input_path")
            return False

        if "num_topics" in data and data["num_topics"] and data["num_topics"]:
            ntopics = data["num_topics"]
        else:
            # calculate best number of topics
            ntopics = True

        logging.info("validate_input_data() FINISHED")
        return True

    # This is the thread code, which listens for new tasks, validates the input and distributes them to the appropriate processing function
    def process_task(self):
        logging.info("Entering process_task()")
        self.comm = Communicator(SRS)

        while True:
            try:
                json_task = self.comm.getNextTask()
                logging.info("getNextTask()")

                operation = json_task["operation"]
                reason = json_task["reason"]
                data = json_task["data"]
                taskId = data["taskId"]

                isCancelled = self.comm.isCancelled(taskId)
                if isCancelled and str(isCancelled) == "True":
                    self.comm.send_res_cancelled_message(
                        taskId, "Execution cancelled")
                    logging.info("execution is cancelled")
                    continue

                if self.validate_input_data(data):
                    logging.info("execution started")
                    self.comm.send_inf_processing_message(
                        taskId, "Execution started")
                    self.process_document_TPE(taskId, data["input_path"])

                else:
                    logging.info("execution rejected")
                    self.comm.send_res_failed_message(
                        taskId, "Execution rejected", "a non-empty location must be provided")
            except Exception as err:
                logging.info(traceback.format_exc())
                logging.info("execution rejected")
                self.comm.send_res_failed_message(
                    taskId, "Task Execution failed: ", str(err))
                pass

    def process_document_TPE(self, taskId, input_path):
        try:
            from concurrent.futures import ThreadPoolExecutor
            executor = ThreadPoolExecutor(5)

            mini = "./corpus/14Words_of_Truth_min.txt"
            from iftpa_tool import ServiceInterface
            si = ServiceInterface()
            task = executor.submit(si.test_case_v1_3, input_path, 5)
            logging.info("Starting processing")

            time_pred = 0
            WOF_14_FSIZE = 683075 # bytes
            WOF_14_TEXEC = 180 # seconds
            TPRED = WOF_14_TEXEC / WOF_14_FSIZE
            import os
            if os.path.isfile(input_path):
                fsize = os.path.getsize(input_path)           
            time_pred = (fsize*TPRED)
            time_pred = time_pred + 10 + time_pred*0.2 # added 10sec + 30% time error

            total = 1
            done = 0
            eta = total

            self.comm.send_inf_processing_message(
                taskId, "Task to be processed", "Ready for processing", eta = 0, percentage = 0)

            t_start = time.time()
            t_init = time.time()

            while not task.done():

                isCancelled = self.comm.isCancelled(taskId)
                if isCancelled and str(isCancelled) == "True":
                    self.comm.send_res_cancelled_message(
                        taskId, "Execution cancelled")
                    return

                # simulate some work
                time.sleep(1)

                done = done + 1
                percentage = done / float(total)
                t_stop = time.time()

                # send ETA every SEND_ETA_EVERY_SECS secs
                if t_stop - t_start > SEND_ETA_EVERY_SECS:
                    logging.info("Processed so far " + str(done))
                    #percentage = done / float(total)
                    #eta = total - done
                    percentage = (time.time() - t_init) / time_pred # work done  /100
                    logging.info("PERCENTAGE={}".format(percentage))
                    if percentage > 1:
                        percentage = percentage % 1.0
                    elif percentage > 0.8:
                        percentage = 0.8 + (0.2 * percentage) # cheat for not superate 100%
                    
                    logging.info("time_pred = {}".format(time_pred))
                    logging.info("time_passed = {}".format(time.time() - t_init))
                    self.comm.send_inf_processing_message(
                        taskId, "Task is processed", "Still processing", eta=percentage*100, percentage=percentage)
                    t_start = t_stop

            logging.info("done")
            task_result = task.result()

            self.comm.send_res_completed_message(
                taskId, "Finished", task_result)

        except IOError as e:
            errno, strerror = e.args
            errstr = "I/O error({0}): {1}".format(errno, strerror)
            logging.info("some error occured: " + errstr)
            logging.info(traceback.format_exc())
            self.comm.send_res_failed_message(taskId, "Error", errstr)

    def thread_pool_process():

        from iftpa_tool import ServiceInterface
        mini = "./corpus/14Words_of_Truth_min.txt"
        si = ServiceInterface()
        time.sleep(10)
        output = si.test_case_v1_3(mini, 5)

        async_result = pool.apply_async(
            si.test_case_v1_3, (mini, 5))  # tuple of args for foo

        # do some other stuff in the main process

        output = async_result.get()  # get the return value from your function.

        return output
