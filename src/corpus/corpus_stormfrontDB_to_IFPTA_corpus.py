#!/usr/bin/env python3

""" Generate input corpus for IFPTA (https://ci.tno.nl/gitlab/ASGARD/internet-forum-profile-text-analyser)

"""

import re
import sys
import argparse
import mysql.connector
from inspect import currentframe, getframeinfo
from bs4 import BeautifulSoup
from datetime import datetime


__author__ = "Albert Farres"
__copyright__ = "Copyright 2019, BSC"
#__credits__ = [""]
#__license__ = ""
__version__ = "0.0.1"
__maintainer__ = "Albert Farres"
__email__ = "albert.farres@bsc.es"
__status__ = "Development"

cleanr = re.compile('<.*?>')


class log:
	debug = False

	@staticmethod
	def dbg( msg ):
		if log.debug:
			cf = currentframe()
			filename = getframeinfo(cf).filename
			print( "DEBUG %s:%03d -- %s" % (filename, cf.f_back.f_lineno, msg), flush=True )

	@staticmethod
	def msg( msg ):
		print( msg, flush=True )

	@staticmethod
	def msg_noendl( msg ):
		print( msg, end='', flush=True )


def valid_date(s):
	try:
		return datetime.strptime(s, "%Y-%m-%d")
	except ValueError:
		msg = "Not a valid date: '{0}'.".format(s)
		raise argparse.ArgumentTypeError(msg)


def get_args( ):
	parser = argparse.ArgumentParser( description=" Generate input corpus for IFPTA (https://ci.tno.nl/gitlab/ASGARD/internet-forum-profile-text-analyser)" )
	parser.add_argument( "user_id", help="user_id in MySQL DB", nargs=1, type=int )
	parser.add_argument( "-o", "--out-file", help="Output file. Default stdout.", type=argparse.FileType( 'w' ), default=sys.stdout )
	parser.add_argument( "-s", "--date-start", help="The start date - format YYYY-MM-DD", required=False, type=valid_date )
	parser.add_argument( "-e", "--date-end", help="The end date - format YYYY-MM-DD", required=False, type=valid_date )
	parser.add_argument( "--mysql-user", help="MYSQL DB user. Default 'asgard'", required=False, type=str, default="asgard")
	parser.add_argument( "--mysql-pass", help="MYSQL DB password. Default 'asgard'", required=False, type=str, default="asgard")
	parser.add_argument( "--mysql-host", help="MYSQL DB host. Default 'localhost'", required=False, type=str, default="localhost")
	parser.add_argument( "--mysql-port", help="MYSQL DB port. Default '3306'", required=False, type=int, default="3306")
	parser.add_argument( "--mysql-db", help="MYSQL DB name. Default 'asgard'", required=False, type=str, default="stormfront_preprocessed")

	parser.add_argument( "--debug", action="store_true" )
	return parser.parse_args( )


def cleanhtml( raw_html ):
	#return re.sub( cleanr, '', raw_html )

	soup = BeautifulSoup(raw_html, 'html.parser')
	try:
		# Remove quotes
		soup.find('div', attrs={'class': 'smallfont'}).decompose()
		soup.table.decompose()
	except AttributeError:  # No smallfont class found, so no quote!
		pass
	return soup.get_text()


def filter( s ):
	# less than 6 words
	words = re.split( '[^0-9A-Za-z]+', s )
	words.remove( '' )
	if( len( words ) < 7 ):
		return True
	# only URI
	return False


def run_query( args ):
	try:
		conn = mysql.connector.connect(user=args.mysql_user, password=args.mysql_pass, host=args.mysql_host, database=args.mysql_db, port=args.mysql_port)
		cursor = conn.cursor()
		query = "select contents from asgard.dbpost where user_id = %d" % (args.user_id[0])
		if args.date_start:
			query += " and date > '%s'" % (args.date_start.strftime('%Y-%m-%d %H:%M:%S'))
		if args.date_end:
			query += " and date < '%s'" % (args.date_end.strftime('%Y-%m-%d %H:%M:%S'))
		#query += " limit 10"
		log.dbg( query )

		cursor.execute( query )
		row = cursor.fetchone()

		while row is not None:
			log.dbg( "NEXT POST" )
			text = cleanhtml( row[0] )
			if not filter( text ):
				args.out_file.write( text.replace('\n', ' ').replace('\r', '').replace('\t', ''))
				args.out_file.write("\n")
			row = cursor.fetchone()

	except mysql.connector.Error as e:
		log.msg(e)

	finally:
		cursor.close()
		conn.close()


if __name__ == '__main__':
	args = get_args( )
	log.debug = args.debug
	log.dbg( args )
	run_query( args )
