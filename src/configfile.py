
from pathlib import Path
import json
import os


class ConfigFile:
    # config_file in json format
    cf_json = None
    lang = ["en"]
    project_name = "asgard"
    ElasticSearch = True
    docline = True
    num_topics = 5
    default_stopwords = "yes"
    project_stopwords = ["quote", "posted", "https", "http"]
    default_stoptags = ["DT", "IN", "PRP", "PRP$", "CC", "CD",
                        "MD", "WRB", "Fit", "Fg", "POS", "RB", "WP", "TO", "WDT"]
    lemmatizer = "Wordnet Lemmatizer"
    threshold_freq_terms = 10
    matching_files = ["./corpus/14Words_of_Truth"]
    ner = ["EVENT"]

    def load_config_file(self, fpath=None):
        """
        Loads the config file as an a module attribute
        """
        my_file = Path(fpath)
        if my_file.is_file():
            with open(fpath, 'r') as f:
                self.config_file = json.load(f)
            self.set_attributes(self.config_file['project'])
        else:
            # file does not exists, broken link, etc.
            pass
        return True

    def set_attributes(self, cf_json):
        """
        Sets the class attributes with the object specified by the json.

        Args:
            cf_json (json): contains the json dictionary with the project execution configuration.
                The json parameter must have the following format:
                {
                    "lang": ["en"],
                    "project_name": "asgard",
                    "ElasticSearch": "yes",
                    "docline": "yes",
                    "num_topics": 5,
                    "default_stopwords": "yes",
                    "project_stopwords": ["quote", "posted", "https", "http"],
                    "default_stoptags": ["DT","IN","PRP","PRP$","CC","CD","MD","WRB","Fit","Fg","POS","RB","WP","TO","WDT"],
                    "lemmatizer": "Wordnet Lemmatizer",
                    "threshold_freq_terms": 10,
                    "matching_files": ["./corpus/14Words_of_Truth"],
                    "ner": ["EVENT"]
                }

        Returns:
            bool : True if the attr assignment had no exception. False Otherwise.
        """

        try:
            self.lang = cf_json["lang"]
            self.project_name = cf_json["project_name"]
            self.ElasticSearch = cf_json["ElasticSearch"]
            self.docline = cf_json["docline"]
            self.num_topics = cf_json["num_topics"]
            self.default_stopwords = cf_json["default_stopwords"]
            self.project_stopwords = cf_json["project_stopwords"]
            self.default_stoptags = cf_json["default_stoptags"]
            self.lemmatizer = cf_json["lemmatizer"]
            self.threshold_freq_terms = cf_json["threshold_freq_terms"]
            self.matching_files = cf_json["matching_files"]
            self.ner = cf_json["ner"]
            return True
        except:
            return False
        pass


if (__name__ == '__main__'):
    json_str = '{"project": {"lang": ["en"], "project_name": "asgard", "ElasticSearch": "yes", "docline": "yes", "num_topics": XXX***, "default_stopwords": "yes", "project_stopwords": ["quote", "posted", "https", "http"], "default_stoptags": ["DT", "IN", "PRP", "PRP$", "CC", "CD", "MD", "WRB", "Fit", "Fg", "POS", "RB", "WP", "TO", "WDT"], "lemmatizer": "Wordnet Lemmatizer", "threshold_freq_terms": 10, "matching_files": ["./corpus/14Words_of_Truth"], "ner": ["EVENT"]}}'
    cf = ConfigFile()
    cf.load_config_file(fpath='.config_file_default')
    print(cf.project_name)
    print(ConfigFile.__dict__)