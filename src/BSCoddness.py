from glove import Corpus, Glove
from nltk import word_tokenize
from nltk.corpus import stopwords
import numpy as np
import spacy  



def vector_distance(u, v):
    """
    Cosine similarity reflects the degree of similariy between u and v
        
    Arguments:
        u -- a word vector of shape (n,)          
        v -- a word vector of shape (n,)

    Returns:
        cosine_similarity -- the cosine similarity between u and v defined by the formula above.
    """
    
    distance = 0.0
    
    ### START CODE HERE ###
    # Compute the dot product between u and v (≈1 line)
    dot = np.dot(u, v)
    # Compute the L2 norm of u (≈1 line)
    norm_u = np.sqrt(np.sum(u**2))
    
    # Compute the L2 norm of v (≈1 line)
    norm_v = np.sqrt(np.sum(v**2))
    # Compute the cosine similarity defined by formula (1) (≈1 line)
    distance = dot / np.dot(norm_u, norm_v)
    ### END CODE HERE ###
    
    return distance



def load_docs(docs_file):
    file = open(docs_file, 'r')
    text = file.read()
    file.close()
    docs = text.split('\n')
    #What about loading very big files?
    return docs

def doc_model_vectorize(doc):
    corpus1 = Corpus()
    corpus1.fit([doc], window=10)
    gl1 = Glove(no_components=100, learning_rate=0.05)
    gl1.fit(corpus1.matrix, epochs=30, no_threads=4, verbose=True)
    gl1.add_dictionary(corpus1.dictionary)
    vector = []
    for w1 in gl1.dictionary:
        # Vector of the word dictionary according to the doc model
        v1 = gl1.word_vectors[gl1.dictionary[w1]]
        for w2 in gl1.dictionary:
            # Vector of another word dictionary
            v2 = gl1.word_vectors[gl1.dictionary[w2]]
            # Distance between the two vectors
            distance = vector_distance(v1, v2)
            # The distance is put as a dimension of the vector
            vector.append(distance)
    return vector


def language_model_vectorize(doc, nlp):
    vector = []
    tokens = nlp(" ".join(doc))
    for token1 in tokens:
        for token2 in tokens:
            vector.append(token1.similarity(token2))
    return vector

def tokenize_doc(doc, stopwords):
    doc_tokens = [t for t in word_tokenize(doc.lower()) if t not in stopwords]
    return doc_tokens

def oddness_score_text_file(text_file, lang, output_file):
    stopWords = []
    nlp = False
    if lang == 'en':
        stopWords = list(set(stopwords.words('english')))

       # If the model is not ready, download it
       # python -m spacy download en_vectors_web_lg
        nlp = spacy.load("en_vectors_web_lg")

    #Open input text file and get the docs to score (doc per line)
    docs2score = load_docs(text_file)
    #Tokenize the docs wiping out stopwords
    doc_tokens = [tokenize_doc(doc.lower(), stopWords) for doc in docs2score]
    #Vectorize the document according to its model. The vector is made up by calculating the distance between the vector word in the doc vocabulary and all the words in the
    #vocabulary
    vectors_text_model = [doc_model_vectorize(d) for d in doc_tokens]
    #Vectorize the document according to the language model. The vector is made up with the same criterion as doc_model_vectorize but the distances according to the 
    # language model are assumed to be different
    vectors_language_model = [language_model_vectorize(d, nlp) for d in doc_tokens]
    # Report and save the results
    with open(output_file,'w') as of:
      for i in range(len(vectors_text_model)):
          print(i, docs2score[i], vector_distance(np.array(vectors_text_model[i]), np.array(vectors_language_model[i])))
          of.write(str(i) + "\t" + docs2score[i] + "\t" + str(vector_distance(np.array(vectors_text_model[i]), np.array(vectors_language_model[i]))) + "\n")

def main():
    oddness_score_text_file(text_file, lang, output_file)
    
if __name__ == "__main__":
    main()



