import sys
import getopt
import json
import matplotlib.pyplot as plt
import gensim.corpora as corpora
from gensim.models.ldamodel import LdaModel
from gensim.models.coherencemodel import CoherenceModel


def compute_coherence_values(dictionary, corpus, texts, limit, start=2, step=3):
    """
    Compute c_v coherence for various number of topics

    Parameters:
    ----------
    dictionary : Gensim dictionary
    corpus : Gensim corpus
    texts : List of input texts
    limit : Max num of topics

    Returns:
    -------
    model_list : List of LDA topic models
    coherence_values : Coherence values corresponding to the LDA model with respective number of topics
    """
    coherence_values = []
    model_list = []
    for num_topics in range(start, limit, step):
        # random_state es la seed the  (https://radimrehurek.com/gensim/models/ldamodel.html)
        model = LdaModel(corpus=corpus, id2word=dictionary,
                         num_topics=num_topics, random_state=100)
        model_list.append(model)
        # coherence {'u_mass', 'c_v', 'c_uci', 'c_npmi'}
        '''
        model (BaseTopicModel, optional) – Pre-trained topic model, should be provided if topics is not provided. Currently supports LdaModel, LdaMulticore, LdaMallet and LdaVowpalWabbit. Use topics parameter to plug in an as yet unsupported model.
        topics (list of list of str, optional) – List of tokenized topics, if this is preferred over model - dictionary should be provided.
        texts (list of list of str, optional) – Tokenized texts, needed for coherence models that use sliding window based (i.e. coherence=`c_something`) probability estimator .
        corpus (iterable of list of (int, number), optional) – Corpus in BoW format.
        dictionary (Dictionary, optional) – Gensim dictionary mapping of id word to create corpus. If model.id2word is present, this is not needed. If both are provided, passed dictionary will be used.
        window_size (int, optional) – Is the size of the window to be used for coherence measures using boolean sliding window as their probability estimator. For ‘u_mass’ this doesn’t matter. If None - the default window sizes are used which are: ‘c_v’ - 110, ‘c_uci’ - 10, ‘c_npmi’ - 10.
        coherence ({'u_mass', 'c_v', 'c_uci', 'c_npmi'}, optional) – Coherence measure to be used. Fastest method - ‘u_mass’, ‘c_uci’ also known as c_pmi. For ‘u_mass’ corpus should be provided, if texts is provided, it will be converted to corpus using the dictionary. For ‘c_v’, ‘c_uci’ and ‘c_npmi’ texts should be provided (corpus isn’t needed)
        topn (int, optional) – Integer corresponding to the number of top words to be extracted from each topic.
        processes (int, optional) – Number of processes to use for probability estimation phase, any value less than 1 will be interpreted as num_cpus - 1.
        '''
        coh_default = 'c_v'
        coherencemodel = CoherenceModel(
            model=model, texts=texts, dictionary=dictionary, coherence='c_v')
        coherence_values.append(coherencemodel.get_coherence())

    return model_list, coherence_values

# Merge NPs and named entities in a keyword list


def merge_nps_nes_hsh(nps_in_sentences, nes_in_sentences, hsh=[]):
    keyword_sentences_merged = []
    for i in range(len(nps_in_sentences)):
        keyword_sentence = []
        keyword_sentence = nps_in_sentences[i]
        for ne in nes_in_sentences[i]:
            if ne not in keyword_sentence:
                keyword_sentence.append(ne)
        '''
        for hs in hsh[i]:
            if hs not in keyword_sentence:
                keyword_sentence.append(hs)
        '''
        keyword_sentences_merged.append(keyword_sentence)
    return keyword_sentences_merged


def bestklda(kw_dict):
    # Create Dictionary
    nps = kw_dict['nps_in_sentences']
    nes = kw_dict['ne_in_sentences']
    if 'hashtags' in kw_dict:
        hsh = kw_dict['hashtags']
    # merged_nps_nes is an array of arrays: each array contains the keywords of a document. E.g: [['white_genocide', 'jew', 'feminist], ['texas', 'bush'],
    # [...], ...]
    # ntopics: number of topics
    kw_sents = merge_nps_nes_hsh(nps, nes)
    id2word = corpora.Dictionary(kw_sents)
    # Create Corpus
    texts = kw_sents
    # print(texts)
    # Term Document Frequency
    corpus = [id2word.doc2bow(text) for text in texts]
    # print(corpus)
    print("CALCULATING COHERENCE VALUES")
    # Can take a long time to run.
    model_list, coherence_values = compute_coherence_values(
        dictionary=id2word, corpus=corpus, texts=texts, start=2, limit=20, step=3)
    # Show graph
    limit = 20
    start = 2
    step = 3
    x = list(range(start, limit, step))
    # print(x)
    # print(coherence_values)
    bestklda_json = {"topics": x, "coherence_values": coherence_values}
    with open("/tmp/bestklda_14wordsofthruth.json", "w") as f:
        json.dump(bestklda_json, f)

    best_ntopics = bestklda_ntopics(x, coherence_values)
    print("N-TOPICS = ", best_ntopics)

    plt.plot(x, coherence_values)
    plt.xlabel("Num Topics")
    plt.ylabel("Coherence score")
    plt.legend(("coherence_values"), loc='best')
    plt.show()

    return best_ntopics


def bestklda_ntopics(list_num_topics, list_coherence_values):
    list_cv_round = []

    for i in range(0, len(list_coherence_values)):
        # round the coherence at two digits
        list_cv_round.append(round(list_coherence_values[i], 2))

    print("n-topics = ", list_num_topics)
    print("list_coherence_values = ", list_cv_round)

    if len(list_cv_round) > 0:
        max_tmp = list_cv_round[0]
    for i in range(0, len(list_cv_round)):
        if list_cv_round[i] < max_tmp:
            ind = list_cv_round.index(max_tmp)
            return list_num_topics[ind]
        elif list_cv_round[i] > max_tmp:
            max_tmp = list_cv_round[i]

    ind = list_cv_round.index(max(list_cv_round))
    return list_num_topics[ind]

    # get the index of the first intance of the max
    ind = list_cv_round.index(max(list_cv_round))
    print("N-TOPICS = ", list_num_topics[ind])

    return list_num_topics[ind]


def main(argv):
    inputfile = ''
    outputfile = ''
    try:
        opts, args = getopt.getopt(argv, "hn:", ["nfile="])
    except getopt.GetoptError:
        print('ERROR: bestklda.py -n <KEYWORDs_FILE>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('bestklda.py -n <KEYWORDS_FILE>')
            sys.exit()
        elif opt in ("-n", "--nfile"):
            kwfile = arg
    print('KEYWORDs file is "', kwfile)
    with open(kwfile) as f:
        loaded_confile = json.load(f)
        bestklda(loaded_confile)


if __name__ == "__main__":
    # main(sys.argv[1:])
    kwfile = "./exec_20190701_10.23.53/output-shellingKWs.json"
    with open(kwfile) as f:
        loaded_confile = json.load(f)
        bestklda(loaded_confile)
