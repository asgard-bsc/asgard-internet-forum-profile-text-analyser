#!/usr/bin/env python
# -*- coding: utf-8 -*-

import nltk
from nltk.corpus import stopwords
from nltk import word_tokenize
from nltk.corpus import wordnet as wn
from nltk.stem.wordnet import WordNetLemmatizer

# Import spacy, an NLP module like NLTK but lighter and faster
import spacy
# The Spacy English model (nlp) must be downloaded: python -m spacy download en_core_web_sm
import en_core_web_sm
import re

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity

import codecs
import json

import pandas as pd
import string

import argparse
import sys
import traceback
import logging
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

# DICTIONARIES

freq = {}  # Word frequency of words in English Wikipedia
rank = {}  # Rank of words in English Wikipedia according to word frequency

#nlp = 'nil'
nlp = en_core_web_sm.load()

# TOPIC DATASETS
topic_dataset_files = {
    'weapons': './DBs/WEAPON-SILKROAD.csv',
    'sex': './DBs/SEX-SILKROAD.csv',
    'politics': './DBs/POLITICS-SILKROAD.csv',
    'drugs': './DBs/DRUGS-SILKROAD.csv',
}

# STOPWORDS

stop = list(stopwords.words('english'))
stop = stop + ['https', 'www', 'rt', 'com', 'http', 'html', 'ww', 'pdf', 'ebook', 'mb', 'thank', 'password', 'email',
               'file', 'bestselling', 'page', 'guide', 'format']

# TEXT TRANSFORMATIONS


def load_word_frequency(lang):
    with open('./DBs/enwiki-20190320-words-frequency.txt') as ef:
        freq = {}
        rank = {}
        lines = ef.readlines()
        for i in range(len(lines)):
            wfs = lines[i].strip().split()
            freq[wfs[0]] = wfs[1]
            rank[wfs[0]] = i
    return rank


rank = load_word_frequency('en')


def kw_sentence_rank(text):
    # Extraction of quite specfific nouns and proper nouns as keywords. The keywords joined will be the string
    # to vectorize in order to calculate its distance to the Silkroad topics.
    #logging.info("text: {}".format(text))
    keyword = []
    pos_tag = ['PROPN', 'NOUN']  # , 'VERB']
    try:
        doc = nlp(text.lower())
        #logging.info("doc = {}\nnlp = {}".format(doc,text))
        for token in doc:
            #logging.debug("TOKEN = {}".format(token))
            if(token.text in nlp.Defaults.stop_words or token.text in string.punctuation):
                continue
            #logging.info("TOKEN {} OK".format(token))
            if token.lemma_ in rank:
                if rank[token.lemma_] > 5000 and token.pos_ in pos_tag:
                    keyword.append(token.lemma_)
                else:
                    #logging.debug("{} cleaned by WIKIPEDIA rank.".format(token))
                    pass

            else:
                #logging.debug("{} token not in rank".format(token))
                pass

            # else:
            #    if re.match("^[a-z]+.*", token.lemma_):
            #        keyword.append(token.lemma_)
    except Exception as e:
        logging.error('Failed to process text: {}\nException message: {}'.format(
            text, traceback.print_exc()))
        logging.info("kw_sentence_rank({})={}".format(text, keyword))

    res = ' '.join(keyword)
    #logging.info("kw_sentence_rank({})={}".format(text, res))
    return res


def prepare_post_string(process_file):
    sentences_posts = []
    with codecs.open(process_file, 'r', encoding='utf-8') as pf:
        ls = pf.readlines()
        for l in ls:
            sentences_posts.append(l.strip())
    kw_posts = [kw_sentence_rank(sp) for sp in sentences_posts]
    post_string = " ".join(kw_posts)
    # Sergio: Assegura't que kw_posts es un string
    #logging.info("post_string={}".format(post_string))
    # return kw_posts
    return post_string


def prepare_topic_dataset(prepare_dataset=False):
    topic_ds = {}
    if prepare_dataset:
        # Loading silkroad posts on weapons
        weapon_agora_df = pd.read_csv(topic_dataset_files['weapons'], names=['Vendor', 'Category', 'Item', 'Item Description',
                                                                             'Price',  'Origin', 'Destination', 'Rating',
                                                                             'Remarks'])

        weapon_posts = weapon_agora_df['Item Description'].tolist()
        # Loading silkroad posts on drugs
        drug_agora_df = pd.read_csv(topic_dataset_files['drugs'], names=['Vendor', 'Category', 'Item', 'Item Description', 'Price',
                                                                         'Origin', 'Destination', 'Rating', 'Remarks'])
        drug_posts = drug_agora_df['Item Description'].tolist()
        # Loading silkroad posts on sex
        sex_agora_df = pd.read_csv(topic_dataset_files['sex'], names=['Vendor', 'Category', 'Item', 'Item Description', 'Price',
                                                                      'Origin', 'Destination', 'Rating', 'Remarks'])
        sex_posts = sex_agora_df['Item Description'].tolist()
        # Loading silkroad posts on politics
        politics_agora_df = pd.read_csv(topic_dataset_files['politics'], names=['Vendor', 'Category', 'Item', 'Item Description', 'Price',
                                                                                'Origin', 'Destination', 'Rating', 'Remarks'])
        politics_posts = politics_agora_df['Item Description'].tolist()
        print("EXTRACTING SILK ROAD WEAPON KEYWORDS")
        kw_wp_skrs = [kw_sentence_rank(s) for s in weapon_posts]
        print("EXTRACTING SILK ROAD DRUG KEYWORDS")
        kw_dg_skrs = [kw_sentence_rank(d) for d in drug_posts]
        print("EXTRACTING SILK ROAD SEX KEYWORDS")
        kw_sx_skrs = [kw_sentence_rank(sx) for sx in sex_posts]
        print("EXTRACTING SILK ROAD POLITICS KEYWORDS")
        kw_pp_skrs = [kw_sentence_rank(pp) for pp in politics_posts]

        topic_ds['weapons_string'] = " ".join(kw_wp_skrs)
        topic_ds['drugs_string'] = " ".join(kw_dg_skrs)
        topic_ds['sex_string'] = " ".join(kw_sx_skrs)
        topic_ds['politics_string'] = " ".join(kw_pp_skrs)

        with open('DBs/prepared_dataset.json', 'w+') as f:
            json.dump(topic_ds, f)
    else:
        # Already prepared, just load file
        with open('DBs/prepared_dataset.json', 'r') as f:
            topic_ds = json.load(f)
    # return topic_ds['weapons_string'], topic_ds['drugs_string'], topic_ds['sex_string'], topic_ds['politics_string']
    return topic_ds

# MATCHING FILES HARDCORED


dict_match_files = {'matching_files': ['vWjw_GUBCp2DG5mgYe-V', 'xWjw_GUBCp2DG5mgYu8K', '0Gjw_GUBCp2DG5mgYu-I', '0Wjw_GUBCp2DG5mgYu-Q',
                                       '72jw_GUBCp2DG5mgY-_T', '92jw_GUBCp2DG5mgZO8Z', '-2jw_GUBCp2DG5mgZO8_', '_Wjw_GUBCp2DG5mgZO9X', '_mjw_GUBCp2DG5mgZO9s', 'A2jw_GUBCp2DG5mgZPCb']}

profiler_match_files_json = json.dumps(dict_match_files)

# CREATE INPUT FILE FOR TOPIC PROFILER


def get_text_from_file_ids(file_id):
    p = asgpost.Post.get(file_id)
    text = p.data
    return text


def create_file2process(matching_files):
    s = asgpost.PostSearch(using=asgpost.es_client,
                           index='stormfrom', doc_type=asgpost.Post)
    texts = [get_text_from_file_ids(mf) for mf in matching_files]
    process_file = 'ASGARD-process-file.tmp'
    with open(process_file, 'w') as af:
        for t in texts:
            af.write(t + "\n")
    return process_file

# =====

# MAIN

# DISTANCE CALCULATION BETWEEN USER AND TOPIC POSTS


def calculate_distance(text1, text2):
    distance = None
    try:
        #logging.info('Failed to process text: calculate_distance({}, {})'.format(text1, text2))
        vectorizer_w = TfidfVectorizer(
            analyzer='word', stop_words=set(stop)
        )
        posts_slkr_w = vectorizer_w.fit_transform([text1, text2])
        post_p_v = posts_slkr_w[0].toarray()
        slkr_w_v = posts_slkr_w[1].toarray()
        distance = cosine_similarity(post_p_v, slkr_w_v)[0][0]
    except Exception as e:
        logging.error(
            'Failed to process text: calculate_distance({}, {})\nException message: {}'.format(text1, text2, traceback.format_exc()))
    else:
        pass
        #logging.info('Failed to process text: calculate_distance({}, {})'.format(text1, text2))
    finally:
        #logging.info('calculate_distance({}, {})={}'.format(text1, text2, distance))
        return distance
    # return distance


def perform_profiling(process_file, topic_ds):
    # logging.debug("RUNNING... perform_profiling({},{})".format(process_file, topic_ds))
    # Prepare the strings to vectorize post string
    posts_string = prepare_post_string(process_file)
    weapon_distance = calculate_distance(
        topic_ds['weapons_string'], posts_string)
    drug_distance = calculate_distance(topic_ds['drugs_string'], posts_string)
    sex_distance = calculate_distance(topic_ds['sex_string'], posts_string)
    politics_distance = calculate_distance(
        topic_ds['politics_string'], posts_string)
    return weapon_distance, drug_distance, sex_distance, politics_distance

# get the main topic of the file
# input:
#   - process_file: raw text file
# output:
#   - topics % of the file


def topic_profiler(process_file, prepare_dataset=False):
    out = {}
    logging.debug("INPUT RAW_FILE:".format(process_file))

    # Load configuration
    config_profiler = {'asgard': {'lang': ['en'],
                                  'id': 'asg',
                                  'num_topics': 7,
                                  'default_stopwords': list(set(stopwords.words('english'))),
                                  'project_stopwords': ['quote', 'posted', 'https', 'http'],
                                  'default_stoptags': ['DT', 'IN', 'PRP', 'PRP$', 'CC', 'CD', 'MD', 'WRB', 'Fit', 'Fg', 'POS', 'RB', 'WP', 'TO', 'WDT'],
                                  'lemmatizer': 'Wordnet Lemmatizer',
                                  'profiler': 'yes'  # We will see if this can be configured from the GUI
                                  }
                       }
    print("PROFILER CONFIGURATION\n", config_profiler)
    # Let's put the match file contents in a single file
    # Let's perform the profiler from the process file
    if config_profiler['asgard']['profiler'] == 'yes':
        #lang =  config_profiler['asgard']['profiler']['lang'][0]
        lang = config_profiler['asgard']['lang'][0]
        if lang == 'en':
            nlp = en_core_web_sm.load()
            # Loading a word freq table from the English Wikipedia
            #w_freq = load_word_frequency(lang)
            rank = load_word_frequency(lang)
            # Generate the topic-dataset
            topic_ds = prepare_topic_dataset(prepare_dataset)
            # Performance of the topic profiler
            weapon_distance, drug_distance, sex_distance, politics_distance = perform_profiling(
                process_file, topic_ds)
            # The output with the scores is saved in a csv file OR JSON AS YOU PREFER
            with open('DBs/PROFILING-SCORES.csv', 'w') as of:
                #of.write('Weapon_Score\tDrug_Score\tSex_Score\tPolitics_Score\nweapon_distance, drug_distance, sex_distance, politics_distance')
                of.write('Weapon_Score, Drug_Score, Sex_Score, Politics_Score\n')
                of.write('{},{},{},{}'.format(weapon_distance,
                                              drug_distance, sex_distance, politics_distance))
            out = {}
            out['weapons'] = weapon_distance
            out['drugs'] = drug_distance
            out['sex'] = sex_distance
            out['politics'] = politics_distance

        else:
            print("NO WORD FREQUENCY FILE FOR LANGUAGE", lang)
    else:
        print("NO PROFILING")
    # else:
    # We will see what to do
    return out

def topic_service(infile):
    res = topic_profiler(infile)
    return res


def main(args):
    res = topic_profiler(args.infile, prepare_dataset=args.prepare)
    print(res)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--infile', '-i', type=str,
                        help='path to the file in raw text', required=True)
    parser.add_argument('--prepare', '-p', action="store_true",
                        help='prepare the topic dataset', required=False)
    args = parser.parse_args()
    main(args)
