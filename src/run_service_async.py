"""

Service interfaces

The following service endpoints are defined in this script:

A) For enqueuing a new task:
   An HTTP POST at http://DOCKER_SERVER_IP:POST/task
   with a Content-type: application/json
   and a JSON POST body like the following:

    - "operation":                      "REQ_START"
    - "reason":                         "Start tool processing"
    - "data.taskId":                    taskId, e.g. "9fb1ab02-f48b-11e8-8eb2-f2801f1b9fd1"
    # ! # If you provide a brokerInfo object, the tool will behave asynchronously (ASYNC), else synchronously (SYNC)
    - "data.brokerInfo.brokerURL":      broker URL, e.g. "stomp://activemq:61616"
    - "data.brokerInfo.brokerQueue":    broker queue, e.g. "queue-1234567"
    - "data.brokerInfo.brokerUser":     broker user, e.g. "username"
    - "data.brokerInfo.brokerPassword": broker password, e.g. "password"
    # ! # The next parameters are application specific
    - "data.location":                  path to an image or video
    - "data.outputFolder":              a folder to write results to

  The POST call returns one of the following self-explanatory JSONs:

    202 {"operation": "RES_STARTED", "reason": "Execution accepted"}
    400 {"operation": "RES_FAILED", "reason": "Execution rejected", "message": "operation, reason, data.taskId and data.brokerInfo must be provided"}

  If it was a SYNC call, then the tool will wait until the end of the processing and then return something like:
    200 {"operation": "RES_COMPLETED", "reason": "Finished", "data": {"outputFile": "/ASGARD_DATA/result/test_out.mp4"}}

B) For retrieving the status of an already posted task:
   An HTTP GET at http://DOCKER_SERVER_IP:POST/task/:taskId:
   where :taskId: the task ID of the task to be queried.

   The GET call returns one of the following self-explanatory JSONs:

    200 {"operation": "RES_FAILED", "reason": "Task id not found", "message": "Task id not found"}
    200 {"operation": "RES_FAILED", "reason": "Execution rejected", "message": "a non-empty location must be provided"}
    200 {"operation": "RES_FAILED", "reason": "Invalid input", "message": "Image dimensions too big"}
    200 {"operation": "RES_STARTED", "reason": "Execution accepted"}
    200 {"operation": "INF_PROCESSING", "reason": "Execution started"}
    200 {"operation": "INF_PROCESSING", "reason": "Video frames are processed", "message": "Video frames are processed", "percentage": 0.9023, "eta": 124}
    200 {"operation": "RES_COMPLETED", "reason": "Finished", "data": {"outputFile": "/ASGARD_DATA/result/test_out.mp4"}}
    200 {"operation": "RES_CANCELLED", "reason": "Execution cancelled"}

C) For cancelling an already posted task:
   An HTTP DELETE at http://DOCKER_SERVER_IP:POST/task/:taskId:
   where :taskId: the task ID of the task to be cancelled

   The DELETE call returns one of the following self-explanatory JSONs:

    200 {"operation": "RES_FAILED", "reason": "Task id not found", "message": "Task id not found"}
    200 {"operation": "RES_CANCELLED", "reason": "Execution cancelled"}

"""


from flask import Flask, render_template
from flask import jsonify, request
import json
import logging
from communicator import Communicator

# ! # Put your own python script and class here
#from super_resolution import SuperResolution
from my_tool import MyTool

logging.basicConfig(level=logging.INFO)

app = Flask(__name__, static_url_path='')

# ! # Use your own SRS name
SRS = "BSC_001"

# ! # Initialize your class
#super_res = SuperResolution()

my_tool = MyTool()
logging.info("Tool class initialized")


@app.route('/task', methods=['POST'])
def post_task():
    logging.info("post called")

    json_req = request.get_json()

    if "operation" in json_req and "reason" in json_req and "data" in json_req and "taskId" in json_req["data"]:
        taskId = json_req["data"]["taskId"]
        comm = Communicator(SRS)
        response = comm.pushTask(taskId, json_req)

        # This is the ASYNC standard case
        if "brokerInfo" in json_req["data"]:
            logging.info("Called ASYNC")
            return jsonify(response), 202  # ACCEPTED
        # This is the SYNC case, for stand-alone tools
        else:
            logging.info("Called SYNC")
            response = comm.waitForTask(taskId)
            return jsonify(response), 200  # OK
    else:
        response = {"operation": "RES_FAILED", "reason": "Execution rejected",
                    "message": "operation, reason, data.taskId must be provided"}
        return jsonify(response), 400  # BAD REQUEST


@app.route('/task/<taskId>', methods=['GET'])
def get_task(taskId):
    logging.info("get called with task id " + taskId)

    comm = Communicator(SRS)
    response = comm.getTaskStatus(taskId)
    return jsonify(response), 200


@app.route('/task/<taskId>', methods=['DELETE'])
def delete_task(taskId):
    logging.info("delete called with task id " + taskId)

    comm = Communicator(SRS)
    response = comm.cancelTask(taskId)
    return jsonify(response), 200


# Web GUI
@app.route("/gui", methods=['GET'])
def home():
    comm = Communicator(SRS)

    if request.args.get('infile') is None:
        return "infile parameter nor provided!"

    infile = request.args.get('infile')
    dict_test = {"t0_p-1": {"posts": [{"text": "Now there has been an explosion in a restaurant during a music festival in Ansbach, at least one known dead, accident or muslim?", "doc_id": 14, "events": []}], "relevant_terms": {"text": ["air", "white", "nazi", "real", "target", "handgun", "day", "merkel", "festival", "men"], "prob": [0.016591068357229233, 0.012675225734710692, 0.01031050831079483, 0.007890705950558186, 0.007810298353433609, 0.007749835960566997, 0.0074717788957059375, 0.007341957651078702, 0.007334711030125619, 0.007116819266229869]}}, "t0_p0": {"posts": [{"text": "When some normal everyday muslim begins slaughtering white folks, why doesn't anyone in Germany reach in their pocket, pull out a handgun and just shoot the jihadi?", "doc_id": 40, "events": []}, {"text": "Trump does not send out hired goons to silence protesters, the crowd does that for him. That is a very important point, don't overlook it! This is a real political movement of white folks that are really angry and will not cow tow to the anti-whites.", "doc_id": 52, "events": []}, {"text": "If we had an army of Tom Metzger's, I know that the Africans would be in Africa, the Mexicans in Mexico, the Asians in Asia, and we would have our White Homeland!", "doc_id": 76, "events": []}], "relevant_terms": {"text": ["air", "white", "nazi", "real", "target", "handgun", "day", "merkel", "festival", "men"], "prob": [0.016591068357229233, 0.012675225734710692, 0.01031050831079483, 0.007890705950558186, 0.007810298353433609, 0.007749835960566997, 0.0074717788957059375, 0.007341957651078702, 0.007334711030125619, 0.007116819266229869]}}, "t0_p1": {"posts": [{"text": "The Jewish leaders of the Neocon movement have turned the office of the federal president into a dictatorship, with rules that make it perfectly legal for President Trump to send a drone after the anti-white proponent of white genocide, Jew George Soros, any place that he is found on Earth.", "doc_id": 4, "events": []}, {"text": "I just checked the updates to the schedule for the North Texas Irish Festival, and it appears that the headliner, Altan, is playing only once to close the festival on Sunday.", "doc_id": 17, "events": []}], "relevant_terms": {"text": ["air", "white", "nazi", "real", "target", "handgun", "day", "merkel", "festival", "men"], "prob": [0.016591068357229233, 0.012675225734710692, 0.01031050831079483, 0.007890705950558186, 0.007810298353433609, 0.007749835960566997, 0.0074717788957059375, 0.007341957651078702, 0.007334711030125619, 0.007116819266229869]}}, "t1_p-1": {"posts": [], "relevant_terms": {"text": ["white", "rifle", "america", "fact", "question", "usa", "video", "range", "jew", "picture"], "prob": [0.011077913455665113, 0.010794444009661673, 0.010210427455604076, 0.010152696631848812, 0.010146564804017544, 0.010141897015273571, 0.010136265307664871, 0.009117980487644672, 0.0075638885609805575, 0.007426774129271506]}}, "t1_p0": {"posts": [{"text": "An AK works very well in CQC, but so does an AR-10. The AR-10 works great out to 500 yards (further for some folks) but not an AK (some super humans claim to be able to get hits at long range with an AK, but everyone else can not, the limit is less than 200 yards for most). An AK is cheap, the ammo is cheap, works great at short range. Much more likely to take a hit at short range. You decide. I bought an AR-10. Who would you bet on?  A whole lot of Muds armed with short range rifles, or a few White Marksmen armed with long range rifles, at long range.", "doc_id": 48, "events": []}, {"text": "The MIGHTY JEW is afraid of a tiny little white girl, just because she is not afraid to tell the truth in public. Let the Jew quake and let the truth be known!", "doc_id": 72, "events": []}, {"text": "FN built a plant here in America, and has been running an advertisement that they are supplying 70% of the small arms for the US Military now. I was talking to a soldier on leave a while back and he said he was totally happy with the weapon issued to him, and it was an FN. I don't know what the Colt percentage is, but it must be less than 30%. Barrett and Alexander Arms are supplying small arms as well. There are some SASS rifles not supplied by Colt, not sure who is supplying those.", "doc_id": 45, "events": []}, {"text": "They should field some select fire AR-10's in .308 the way that God and Eugene Stoner originally intended. My father got to shoot one of those original AR-10's before the adoption of the M-16, and he thought it was the greatest rifle in the world;  he felt that with that rifle, Jewish Communism was done! Then the Pentagon intervened.", "doc_id": 47, "events": []}, {"text": "The Crooked Road is a regional destination covering 19 counties, four cities, and over 50 towns and communities where heritage music is celebrated year round. With nine Major Venues, over 60 Affiliated Venues and Festivals, and over 25 Wayside Exhibits, visitors have many opportunities to experience one of the richest music traditions in the world. This is where America's music was born ... and lives on!", "doc_id": 9, "events": []}, {"text": "Many of the Bloated HolyCo$t Industry \"\"facts\"\" sound so outlandish that they should be verified before acceptance.  If the \"\"facts\"\" will not bear the scrutiny of Math, Science, or Logic, then they are not \"\"facts\"\".", "doc_id": 11, "events": []}, {"text": "Dr. David Duke, PhD. asks an important a question in this video.", "doc_id": 0, "events": []}, {"text": "Abu Dhabi Murderer caught on security video", "doc_id": 7, "events": []}, {"text": "What is a Jew?", "doc_id": 27, "events": []}], "relevant_terms": {"text": ["white", "rifle", "america", "fact", "question", "usa", "video", "range", "jew", "picture"], "prob": [0.011077913455665113, 0.010794444009661673, 0.010210427455604076, 0.010152696631848812, 0.010146564804017544, 0.010141897015273571, 0.010136265307664871, 0.009117980487644672, 0.0075638885609805575, 0.007426774129271506]}}, "t1_p1": {"posts": [{"text": "I believe that a .308 AR-10 is the best all around rifle that this is available today.", "doc_id": 26, "events": []}], "relevant_terms": {"text": ["white", "rifle", "america", "fact", "question", "usa", "video", "range", "jew", "picture"], "prob": [0.011077913455665113, 0.010794444009661673, 0.010210427455604076, 0.010152696631848812, 0.010146564804017544, 0.010141897015273571, 0.010136265307664871, 0.009117980487644672, 0.0075638885609805575, 0.007426774129271506]}}, "t2_p-1": {"posts": [], "relevant_terms": {"text": ["white", "world", "arm", "supply", "great", "message", "man", "america", "rifle", "fake"], "prob": [0.012990036979317663, 0.011843952350318432, 0.011549925431609155, 0.00950525514781475, 0.008810047060251236, 0.008614099584519863, 0.008469247259199621, 0.0069423881359398365, 0.006670717149972916, 0.006658583879470825]}}, "t2_p0": {"posts": [{"text": "Finally the chant of \"\"USA, USA, USA\"\" goes up, and it is appropriate. This political movement and Trump really do represent the USA. White Americans can make America great again!", "doc_id": 49, "events": []}, {"text": "So far every third world medical \"\"professional\"\" has claimed they did nothing wrong but just caught Ebola anyway, as if by magic.", "doc_id": 29, "events": []}, {"text": "Someone was asking about accuracy a while back, and I happened across these customer supplied targets on the DPMS website. http://www.dpmsinc.com/awards/zone/targets/index.aspx", "doc_id": 57, "events": []}, {"text": "There is a sort of tribute CD \"\"Landser in English\"\" that you might like. Landser is great! The Occupation Government that subjects Germany to the same sort of treatment that Dixie receives from the Washington Government, threw Landser into prison for singing songs. For the sake of our Children, we must be free of the Federal Beast! Deo Vindice!", "doc_id": 88, "events": []}, {"text": "Great job! Always take pride in everything you do.", "doc_id": 90, "events": []}], "relevant_terms": {"text": ["white", "world", "arm", "supply", "great", "message", "man", "america", "rifle", "fake"], "prob": [0.012990036979317663, 0.011843952350318432, 0.011549925431609155, 0.00950525514781475, 0.008810047060251236, 0.008614099584519863, 0.008469247259199621, 0.0069423881359398365, 0.006670717149972916, 0.006658583879470825]}}, "t2_p1": {"posts": [{"text": "I wonder what the grand palace of the great Negus was like, was it like the projects?", "doc_id": 16, "events": []}], "relevant_terms": {
        "text": ["white", "world", "arm", "supply", "great", "message", "man", "america", "rifle", "fake"], "prob": [0.012990036979317663, 0.011843952350318432, 0.011549925431609155, 0.00950525514781475, 0.008810047060251236, 0.008614099584519863, 0.008469247259199621, 0.0069423881359398365, 0.006670717149972916, 0.006658583879470825]}}, "t3_p-1": {"posts": [{"text": "Africans murder three children", "doc_id": 8, "events": []}, {"text": "If she rips that hair do out, then I guess that African is going to have to teach her a lesson, and that might involve a beat down, stabbing, shooting, drinking some Draino, being set on fire, and a posthumous rape.", "doc_id": 56, "events": []}], "relevant_terms": {"text": ["crimea", "russian", "african", "ukrainian", "federal", "white", "government", "child", "ukraine", "honor"], "prob": [0.025935785844922062, 0.019009621813893318, 0.016846224665641785, 0.0145784979686141, 0.01270589418709278, 0.010537801310420036, 0.008870490826666355, 0.007990552112460136, 0.006984574254602194, 0.006926314905285835]}}, "t3_p0": {"posts": [{"text": "Arms should not be banned! An armed Society is a polite Society! The Federal Government should be banned! Secession now, Freedom forever!", "doc_id": 86, "events": []}, {"text": "Alex Jones repeats the same lies about Hitler, Nazis, and WW II, that the majority of normal every day White Men in America already believe. The average White Man in America hates Hitler, the SS, the Gestapo, the Third Reich, and Nazis. That is why when an individual shows up waving a Nazi flag, they drive normal White Men away from joining the White Nationalist cause. White Family Men supporting a Future for White Children should be the most natural thing on Earth. Why haven't the majority of White Family Men joined us?  Nazi flags.", "doc_id": 68, "events": ["white_men_in_america"]}, {"text": "Please fire everyone who does not show up for work on that day, and hire a white man in their place.", "doc_id": 92, "events": []}, {"text": "Yes, it is time for the White Man to stop being the world's slave and working to support every third world savage on Earth! Let the savages live in the homes they build, eat the food they grow, and drive the cars they build. From now on the White Man benefits from his own hard labor.", "doc_id": 97, "events": []}, {"text": "Finally, someone is bothering to point out actual Jewish Media Mafia lies and address them item by item! One of the most important ones you pointed out: National Socialists are not Globalists. New World Order Communists (Jewish One World Government) are not Nazis--  they are the opposite of Nazis! It is about time someone said so. Keep up the good work. Fight Jewish Communist Lies with Truth!", "doc_id": 71, "events": []}], "relevant_terms": {"text": ["crimea", "russian", "african", "ukrainian", "federal", "white", "government", "child", "ukraine", "honor"], "prob": [0.025935785844922062, 0.019009621813893318, 0.016846224665641785, 0.0145784979686141, 0.01270589418709278, 0.010537801310420036, 0.008870490826666355, 0.007990552112460136, 0.006984574254602194, 0.006926314905285835]}}, "t3_p1": {"posts": [], "relevant_terms": {"text": ["crimea", "russian", "african", "ukrainian", "federal", "white", "government", "child", "ukraine", "honor"], "prob": [0.025935785844922062, 0.019009621813893318, 0.016846224665641785, 0.0145784979686141, 0.01270589418709278, 0.010537801310420036, 0.008870490826666355, 0.007990552112460136, 0.006984574254602194, 0.006926314905285835]}}, "t4_p-1": {"posts": [{"text": "If a shooter is trying to compensate for the recoil as the cartridge fires, he will often shoot low.", "doc_id": 5, "events": []}], "relevant_terms": {"text": ["handgun", "shoot", "african", "trump", "white", "carry", "range", "fire", "america", "jones"], "prob": [0.010892976075410843, 0.010610630735754968, 0.009789864532649515, 0.009406434372067451, 0.0092019597068429, 0.0073928264901041985, 0.006741099525243044, 0.006544976495206356, 0.006173504050821066, 0.005742059089243412]}}, "t4_p0": {"posts": [{"text": "My first handgun reloads were for my snub nose.", "doc_id": 13, "events": []}, {"text": "A black African terrorist wants a race war in America. Finally one of them has said something that normal everyday average Americans can agree with. The odds are about 100% that a race war, communist uprising, uprising of Mexican illegals, free for all, will take place later this year. Just be prepared to survive. The survivors get to decide the future of America.", "doc_id": 54, "events": []}, {"text": "When the allies were \"\"liberating\"\" France, one bombing campaign is said to have killed 40,000 French (the ones the allies were there to save). The French survivors were then raped, especially French children, buy the allies US federal Africans. The allies' Turkish troops were also allowed to rape and pillage, and then the Asians of the Russian rape gangs as well. Does anyone else need saving?", "doc_id": 96, "events": []}, {"text": "It was a great speech! Trump spoke the truth.  There is nothing that the Jew fears more than a man who speaks the truth in public. He does not side step any issues, he does not beat around the bush, he does not speak one message for one group and a different message for a different group, he has one message, and it is a good message. Trump has the support of white America and he is unstoppable! Let's make America great again.", "doc_id": 50, "events": []}, {"text": "Massachusetts? Liberal progressive anti-conservative Massachusetts? This proves how fed up white America is with the anti-white trend in America. If Trump can pack them in there, then he can pack them in anywhere!", "doc_id": 51, "events": []}, {"text": "Colonial Patriots fought for Freedom against a tyrannical government. The descendants of those very Patriots fought against the Federal Mafia which had grown tyrannical.  The very words of the butchers who attacked the Confederate Patriots prove this to be the case. For the Federal Mafia butchers, no deed was too low, no treachery was beyond the limits of their &#147;honor&#148;. By the blood of murdered White Children, and the blood of murdered White Women, they meant us to fear them for all time;  that has not been the result. The rape of the South did not breed fear;  it bread an eternal contempt instead. People often say that there are two sides to every issue, but in the rape of the South, there is but one side for any man of honor.  The Federal Mafia is a bloody tyrant without honor, and therefore no man of honor will ever defend that side. The Old Man of the Mountain", "doc_id": 59, "events": []}, {"text": "I am glad she has erased her own future. Her descendants will be blacks or browns and will have no say in the future of white folks. She will be gone and forgotten as if she had never existed at all.", "doc_id": 63, "events": []}, {"text": "Okay, is she is from Canada then I guess that is why she talks that way. Anyone going to make better videos than hers then get on it. Evalion thanks for the videos, keep it white! My suggestion is that it is better to promote White Nationalism than National Socialism, but they are your vids.", "doc_id": 73, "events": []}, {"text": "I just happened to find this article today on glass beading a carry handle mount if any one is interested: AR-15 Carry Handle Optics Mounting   http://www.varminthunters.com/ar15te...emounting.html", "doc_id": 55, "events": []}, {"text": "Alex Jones reported that Deutsche Bank is on the verge of failure.  That information would be from some financial source and not from Jones himself.", "doc_id": 18, "events": []}], "relevant_terms": {"text": ["handgun", "shoot", "african", "trump", "white", "carry", "range", "fire", "america", "jones"], "prob": [0.010892976075410843, 0.010610630735754968, 0.009789864532649515, 0.009406434372067451, 0.0092019597068429, 0.0073928264901041985, 0.006741099525243044, 0.006544976495206356, 0.006173504050821066, 0.005742059089243412]}}, "t4_p1": {"posts": [], "relevant_terms": {"text": ["handgun", "shoot", "african", "trump", "white", "carry", "range", "fire", "america", "jones"], "prob": [0.010892976075410843, 0.010610630735754968, 0.009789864532649515, 0.009406434372067451, 0.0092019597068429, 0.0073928264901041985, 0.006741099525243044, 0.006544976495206356, 0.006173504050821066, 0.005742059089243412]}}}
    #ntopics = request.args.get('ntopics')
    ntopics = 5
    #dict_test = json.dumps(dict_test, ensure_ascii=False).encode('utf8')
    # print(json.dumps(dict_test))
    '''
    if request.args.get('taskId') is None:
        return "taskId not provided!"
    taskId = request.args.get('taskId')
    logging.info("taskId="+taskId)
    response = comm.getTaskStatus(str(taskId))
    logging.info("response"+json.dumps(response))
    #logging.info("jsonify(response)['data']['out-file'] = " + response['data']['out-file'])
    try:
        infile = response['data']['out-file']
    except Exception as e:
        logging.info("error with the file")
        print(str(e))
        pass
    '''

    with open(infile) as json_file:
        infile_json = json.load(json_file)

    return render_template("index.html", text_lists=infile_json, ntopics=ntopics)


logging.info("Listener started")

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)
