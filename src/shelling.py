import codecs
import re
import nltk
from nltk.corpus import stopwords
from nltk import word_tokenize
from nltk.corpus import wordnet as wn
from nltk.stem.wordnet import WordNetLemmatizer
import shelling_phrase as sp

#Dictionary: key is the lemma, values are the forms of this lemma
lem_forms = {}

#Getting the language stopwords
def get_default_stopwords(lang):
    stopword_list = []
    #If English is the language
    if lang == ['en']:
       #Loading NLTK stopwords 
       stopword_list = list(set(stopwords.words('english')))
       #Loading Google stopwords
       with open('data/stop-words_english_1_en.txt') as stf:
            stl = stf.readlines()
            for st in stl:
                stopword_list.append(st.strip())
    return stopword_list

#Get the sentence with the words lemmatized
def lemmatize_sentence(sentence):
    sentence_tokens = word_tokenize(sentence.lower())
    tokens_lemmatized = [sp.wnlemmatize(st,nltk.pos_tag(word_tokenize(st))) for st in sentence_tokens]
    sentence_lemmatized = " ".join(tokens_lemmatized)
    return sentence_lemmatized
