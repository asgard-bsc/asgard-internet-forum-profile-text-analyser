# Pandas for data management
import pandas as pd

# os methods for manipulating paths
from os.path import dirname, join

# Bokeh basics 
from bokeh.io import curdoc, show
from bokeh.models.widgets import Tabs


# Each tab is drawn by one script
from docplots import lda_sa_plot_tab
from termplots import w2vec_plot_tab

# Read data into dataframes

# Create each of the tabs
tab1 = lda_sa_plot_tab() # tab 1 is returned by the function lda_sa_plot_tab in docplots.py
tab2 = w2vec_plot_tab()  # tab 2 is returned by the function w2vec_plot_tab in termplots.py

# Put all the tabs into one application
tabs = Tabs(tabs = [tab1, tab2])
#tabs = Tabs(tabs = [tab1])

# Put the tabs in the current document for display
curdoc().add_root(tabs)


