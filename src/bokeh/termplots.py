import os
import pandas as pd
import gensim

from bokeh.plotting import figure
from bokeh.models import (CategoricalColorMapper, HoverTool,
                          ColumnDataSource, Panel, LabelSet,
                          FuncTickFormatter, SingleIntervalTicker, LinearAxis)
from bokeh.models.widgets import (CheckboxGroup, Slider, RangeSlider, Button,
                                  Tabs, CheckboxButtonGroup, Paragraph,
                                  TableColumn, DataTable, Select, TextInput, Div)
from bokeh.layouts import column, row, WidgetBox
from bokeh.palettes import all_palettes

from bokeh.io import curdoc, show


def w2vec_plot_tab():
    # Loading the dataframe the visualisation is based on
    #df = pd.read_csv('14WORDS-W2V-WITH-TOPICS-FREQ.csv')
    #vpath = "./internet-forum-profile-text-analyser/output/input_file-ifpta/exec_20190517_13.39.17/df_lda_term.csv"
    vpath = './14WORDS-W2V-WITH-LDA.csv'
    df = pd.read_csv(vpath)
    df1 = df[df['x'] != 0.0]
    tools_plot = ['wheel_zoom', 'pan']

    p = figure(tools=tools_plot, toolbar_location='below',
               plot_height=700, plot_width=1000)

    # Definition of the dots in the plot. Dots depend on the data in 'source'. Source is the current dataframe but in
    # the ColumnDataSource format, which is the dataframe format Bokeh works with (name 'df1').

    source = ColumnDataSource(df1)

    print("SOURCE", source)

    r = p.circle(x='x', y='y', source=source, name="df1", fill_color='blue')

    # Definition of the labels accompanying the dots (the same coordintates). The labels are the terms in the column 'terms' of the soruce

    labels = LabelSet(x='x', y='y', text='Term', level='glyph',
                      x_offset=5, y_offset=5, source=source)
    p.add_layout(labels)

    # Definition of the widgets in the tab
    # 1. topic_selection: dropdown menu where the user selects the topic of the documents to be displayed. The options are the unique values
    # of the column 'label' in the dataframe

    topic_options = []
    topic_options.append('All')

    header_list = list(df)

    #fq = {}
    fs = []
    ts = []

    # Get the topic options
    for hl in header_list:
        if 'Fq-' in hl:
            t = hl.replace('Fq-', '')
            topic_options.append(str(t))
            fs = fs + df1[hl].tolist()
        elif 'Tfidf' in hl:
            ts = ts + df1[hl].tolist()

    freq_max = max(fs)

    tfidf_max = max(ts)

    doc_topic_selection = Select(
        title="Topic",  options=topic_options, value="All")

    # 2. Term selection: dropdown menu where the user selects one of the terms belonging to the topic selected

    term_selection = Select(title="Topic Terms", options=[], value="")

    # 3. Freq_term_slider: Slider to filter terms whose frequency in the topic is below a threshold

    thres_freq_slider = Slider(
        start=1, end=freq_max, step=1, value=1, title="Threshold frequency")

    # 4. Tfidf_term_slider: Slider to filter terms whose tfidf in the topic is below a threshold

    thres_tfidf_slider = Slider(
        start=0, end=tfidf_max, step=0.001, value=0, title="Threshold tfidf")

    # 5. Term_senses_slider: Slider to filter terms whose number of senses is below a threshold

    max_senses = df['number_of_senses'].max()

    term_synset_slider = Slider(
        start=1, end=max_senses, step=1, value=max_senses, title="Ambiguity Threshold")

    # 6. Event selection: dropdown menu where the user selects one of the events belonging to the topic selected

    event_selection = Select(title="Topic Events", options=[], value="")

    # 4. Term_hyponyms_slider: Slider to filter terms whose number of hyponyms is below a threshold

    #max_hypons = df['number_of_hyponyms'].max()

    #term_hypon_slider = Slider(start=1, end=max_hypons, step=1, value=max_hypons, title="Hyponyms")

    # 5. Term_prob_slider: Slider to filter terms whose probability to belong to the topic is below a threshold

    #max_prob = df['probs'].max()

    #min_prob = df['probs'].min()

    #term_prob_slider = Slider(start= min_prob, end=max_prob, step=1, value= min_prob, title="Term topic probability")

    #source1 = ColumnDataSource(df)
    #r.data_source.data = source1.data

    # Definition of the functions executed by the widgets in the tab

    # If the user selects a topic, display all the terms belonging to the topic in the dropdown menu

    def update_term_list(attr, old, new):
        tval = doc_topic_selection.value
        h = 'Fq-' + tval
        t = 'Prob-' + tval
        df2 = df1[df1[h] > 0]
        df3 = df2.sort_values([t], ascending=False)
        df3 = df3[df3[t] > 0.0]
        term_selection.options = df3['Term'].tolist()
        df4 = df3[df3['ne'] == 1]
        event_selection.options = df4['Term'].tolist()
        source2 = ColumnDataSource(df3)
        r.data_source.data = source2.data

    doc_topic_selection.on_change('value', update_term_list)

    # If the user selects a term, create the current dataframe from which the plot will be created. This dataframe is the source for
    # the plot and contains the coordinates of the most similar terms to the term selected by the user, according to a Word2Vec model.
    # The dataframe is saved.

    def update_term_plot(attr, old, new):
        # Get similar terms to the term selected
        model = gensim.models.Word2Vec.load('14WORDS-w2v.model')
        term = term_selection.value
        print(term)
        similar_terms_tuples = [st for st in model.similar_by_word(term, 50)]
        similar_terms = [term]
        print(len(similar_terms))
        for st in similar_terms_tuples:
            if st[1] > 0.8:
                print(st[0], st[1])
                similar_terms.append(st[0])
        df2 = df1[df1['Term'].isin(similar_terms)]
        source2 = ColumnDataSource(df2)
        r.data_source.data = source2.data

    term_selection.on_change('value', update_term_plot)

    def thres_freq_slider_callback(attr, old, new):
        tval = doc_topic_selection.value
        print("TVAL", tval)
        N = thres_freq_slider.value
        print(N)
        l = 'Fq-' + tval
        t = 'Prob-' + tval
        df2 = df1[df1[l] >= N]
        df3 = df2.sort_values([t], ascending=False)
        df3 = df3[df3[t] > 0.0]
        term_selection.options = df3['Term'].tolist()
        source3 = ColumnDataSource(df3)
        r.data_source.data = source3.data

    thres_freq_slider.on_change('value', thres_freq_slider_callback)

    def thres_tfidf_slider_callback(attr, old, new):
        tval = doc_topic_selection.value
        print("TVAL", tval)
        print("FREQ", thres_freq_slider.value)
        N = thres_tfidf_slider.value
        print(N)
        fl = 'Fq-' + tval
        tl = 'Tfidf-' + tval
        t = 'Prob-' + tval
        df2 = df1[df1[fl] >= thres_freq_slider.value]
        df3 = df2[df2[tl] * 10 >= N]
        df4 = df3.sort_values([t], ascending=False)
        term_selection.options = df4['Term'].tolist()
        source3 = ColumnDataSource(df4)
        r.data_source.data = source3.data

    thres_tfidf_slider.on_change('value', thres_tfidf_slider_callback)

    def synset_slider_callback(attr, old, new):
        tval = doc_topic_selection.value
        print("THR FREQ", thres_freq_slider.value)
        print("THR TFIDF", thres_tfidf_slider.value)
        print("TVAL", tval)
        N = term_synset_slider.value
        print("SYNSET SLIDE_VALUE", N)
        fl = 'Fq-' + tval
        tl = 'Tfidf-' + tval
        t = 'Prob-' + tval
        df2 = df1[df1[fl] >= thres_freq_slider.value]
        df3 = df2[df2[tl] * 10 >= thres_tfidf_slider.value]
        df4 = df3[df3['number_of_senses'] <= N]
        df5 = df4.sort_values([t], ascending=False)
        print(df5)
        term_selection.options = df5['Term'].tolist()
        source3 = ColumnDataSource(df5)
        r.data_source.data = source3.data

    term_synset_slider.on_change('value', synset_slider_callback)

    def hyponym_slider_callback(attr, old, new):
        tval = doc_topic_selection.value
        print("TVAL", tval)
        H = term_hypon_slider.value
        S = term_synset_slider.value
        print("HYPONYM SLIDE_VALUE", S)
        df2 = df1[(df1.topics == int(tval)) & (
            df1.number_of_hyponyms <= H) & (df1.number_of_senses <= S)]
        term_selection.options = df1['terms'].tolist()
        print("DFFFDSA", df2)
        source4 = ColumnDataSource(df2)
        r.data_source.data = source4.data

    #term_hypon_slider.on_change('value', hyponym_slider_callback)

#    def term_prob_slider_callback(attr, old, new):
#        tval = doc_topic_selection.value
#        S = term_prob_slider.value
#        print("TVAL", tval)
#        df4 = df[(df.topics == int(tval)) & (df.probs >= S)]
#        term_selection.options = df4['terms'].tolist()
#        source5 = ColumnDataSource(df4)
#        r.data_source.data = source5.data

#    term_prob_slider.on_change('value', term_prob_slider_callback)

# Design the layout of the tab.
# Controls: the row of widgets:
# plot: the figure where the documents are plotted

    controls = WidgetBox(doc_topic_selection, term_selection, thres_freq_slider, thres_tfidf_slider, term_synset_slider,
                         event_selection)  # , term_hypon_slider, term_selection)#term_prob_slider, )
    layout = row(controls, p)

# Definition of the tab: the layout is a child. The title of the tab is also displayed
    tab = Panel(child=layout, title='Term Filtering')
    return tab
