import os
import pandas as pd

from bokeh.plotting import figure
from bokeh.models import (CategoricalColorMapper, HoverTool,
                          ColumnDataSource, Panel,
                          FuncTickFormatter, SingleIntervalTicker, LinearAxis)
from bokeh.models.widgets import (CheckboxGroup, Slider, RangeSlider, Button,
                                  Tabs, CheckboxButtonGroup, Paragraph,
                                  TableColumn, DataTable, Select, TextInput, Div)
from bokeh.layouts import column, row, WidgetBox
from bokeh.palettes import all_palettes

from bokeh.io import curdoc, show


def lda_sa_plot_tab():
    # Loading the dataframe the visualisation is based on
    #vpath = "./internet-forum-profile-text-analyser/output/input_file-ifpta/exec_20190517_13.39.17/df_lda_topic.csv"
    vpath = 'asgard-LDA-DOCS-TERMS-SA-INDEXES.csv'
    df = pd.read_csv(vpath)

# Definition of the plot
    # Tools_plot: tools such as hovering the dots of the plot and zoom. The situation of these tools is also stated.
    # Tools such as hovering display the information from the updated dataframe specified by the value of "names". Besides,
    # the piece of information to be displayed is also stated by means of the @ symbol. For instance, @terms means we want to see the
    # contents of the column 'terms' in the dataframe when hovering.

    hover_plot = HoverTool(names=["df"], tooltips="""
			<div style="margin: 10">
				<div style="margin: 0 auto; width:300px;">
					<span style="font-size: 12px; font-weight: bold;">Terms in doc:</span>
					<span style="font-size: 12px">@terms</span>
				</div>
			</div>
		""")

    tools_plot = [hover_plot, 'wheel_zoom', 'pan']

    plot = figure(tools=tools_plot, toolbar_location='below')

    # Definition of the dots in the plot. Dots depend on the data in 'source'. Source is the current dataframe but in
    # the ColumnDataSource format, which is the dataframe format Bokeh works with (name 'df'). For this reason, previously we create a new
    # column in the dataframe with the color of each dot representing a document. The color depends on the value 'label' of the dataframe
    # ('label' is the label of the topic the document belongs to)

    df['colors'] = [all_palettes['Set1'][8][i] for i in df.label]

    source = ColumnDataSource(df)

    # The dots are circles whose coordinates are stated in the columns 'x' and 'y' of the source. The colors are the
    # color values in the source
    r = plot.circle(x='x', y='y', fill_color='colors',
                    source=source, name="df")

# Definition of the widgets in the tab
# 1. topic_selection: dropdown menu where the user selects the topic of the documents to be displayed. The options are the unique values
# of the column 'label' in the dataframe

    topic_options = []
    topic_options.append('All')

    topic_op = sorted(df['label'].unique().tolist())

    for to in topic_op:
        topic_options.append(str(to))

    topic_selection = Select(
        title="Topic",  options=topic_options, value="All")

# 2. sa_selection: dropdown menu where the user selects the sentiment analysis class of the documents to be displayed. The options are 'No
# sentiment' (default option), 'Negative', 'Positive', 'Neutral'

    sa_selection = Select(title="Filtering by sentiment analysis", value="1", options=[
                          'No Sentiment', 'Negative', 'Positive', 'Neutral'])

# 3. ev_selection: dropdown menu where the user selects the degree of eventness of the documents to be displayed (not developed yet)

    #ev_selection = Select(title="Filtering by eventness", value="1", options= [])

# 4. term_input: text box where the user can type a term and see the documents that contain it

    term_input = TextInput(value="default", title="Type keyword:")

# button: When clicking it, the contents of the documents displayed should be exported and visualised

    button = Button(label="Export docs", button_type="success")

#   Definition of the functions executed by the widgets in the tab
#   Topic selection in the dropdown topic_selection
    # If the user selects "All", copy the complete visualisation and save it as the current dataframe
    # If the user selects a number corresponding to a topic (e.g. 1 for topic 1), create a smaller dataframe with the documents whose label is
    # topic selected. Save this database as the current dataframe. The source data for plotting the dots corresponds to this new dataframe.

    def update_topic_plot(attr, old, new):
        if topic_selection.value == "All":
            df_filter = df.copy()
            df_filter.to_csv('CURRENT-DF.csv')
        else:
            df_filter = df[df['label'] == int(topic_selection.value)]
            df_filter.to_csv('CURRENT-DF.csv')
        source1 = ColumnDataSource(df_filter)
        r.data_source.data = source1.data

    topic_selection.on_change('value', update_topic_plot)

#   Sentiment class selection in the dropdown sa_selection
    # If the user selects a sentiment class, create a smaller dataframe with the documents whose value in the 'Polarity' column is the one
    # selected. Save this database as the current dataframe. The source data for plotting the dots corresponds to this new dataframe.

    def update_sa_plot(attr, old, new):
        if sa_selection.value != "No Sentiment" and os.path.isfile('CURRENT-DF.csv'):
            olddf = pd.read_csv('CURRENT-DF.csv')
            df_filter = olddf[olddf['polarity'] == sa_selection.value]
            df_filter.to_csv('CURRENT-DF.csv')
        source1 = ColumnDataSource(df_filter)
        r.data_source.data = source1.data

    sa_selection.on_change('value', update_sa_plot)

#   Term selection in the term input
    # If the user types a term, create a smaller dataframe with the documents containing the term in the 'Terms' columns in the dataframe.
    # The source data for plotting the dots corresponds to this new dataframe

    def update_term_plot(attr, old, new):
        if term_input.value != "default" and os.path.isfile('CURRENT-DF.csv'):
            olddf = pd.read_csv('CURRENT-DF.csv')
            df_filter = olddf[olddf['terms'].str.match(term_input.value)]
            df_filter.to_csv('CURRENT-DF.csv')
        source1 = ColumnDataSource(df_filter)
        r.data_source.data = source1.data

    term_input.on_change('value', update_term_plot)

# Design the layout of the tab.
# Controls: the row of widgets:
# plot: the figure where the documents are plotted

    controls = WidgetBox(topic_selection, sa_selection, term_input, button)
    layout = row(controls, plot)

# Definition of the tab: the layout is a child. The title of the tab is also displayed
    tab = Panel(child=layout, title='Document Filtering')
    return tab
