"""

This is the actual tool class with the application specific code.
During class construction, it initializes the model and starts a new thread.
This thread listens to Redis (through the Communicator) for new tasks.
When a new task arrives, it is sent to the appropriate processing function.
The Communicator interfaces are used for updating Redis and notifying the orchestrator queue.

Functions:
__init__             : class constructor
mkdir_p              : help function
psnr                 : help function
custom_loss          : help function
process_task         : main thread code, listens to Redis, distributes tasks
process_image        : processing function for images
process_image_folder : processing function for image folders
process_video        : processing function for videos

Important points:
- Use Communicator() to instantiate the Communicator class
- Use self.comm.getNextTask() to listen for the next task (BLOCKING)
- Use self.comm.send_inf_processing_message(...) to indicate a task status update (e.g. ETA messages)
- Use self.comm.send_res_failed_message(...) to indicate a task failure
- Use self.comm.isCancelled(taskId) to check if a task has been cancelled
- Use self.comm.send_res_cancelled_message(...) to indicate a task cancelation
- Use self.comm.send_res_completed_message(...) to indicate a task completion

"""

from keras.models import load_model
import numpy as np
from skimage.transform import resize
import os, cv2, errno
import keras.backend as K
import tensorflow as tf
from tensorflow.python.framework.errors_impl import ResourceExhaustedError
from urllib import urlopen
import urlparse
import pafy
from datetime import timedelta
import time
import json
import glob
import threading
import logging
from communicator import Communicator

global graph
graph = tf.get_default_graph()


gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.4)
#gpu_options = tf.GPUOptions(allow_growth=True)
sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))

logging.basicConfig(level=logging.INFO)

# Every how many seconds should an updated ETA message be sent?
SEND_ETA_EVERY_SECS = 10

class SuperResolution():

    # xfactor can be 2 or 4
    def __init__(self):
        logging.info("Entering init")

        #Load model
        sr_model = load_model('my_model_x4.hdf5', custom_objects={'custom_loss':self.custom_loss, 'psnr':self.psnr})
        logging.info("Model loaded")

        self.model = sr_model
        self.xfactor = 4

        logging.info("Starting thread")
        self.worker = threading.Thread(target=self.process_task, args=()).start()
        logging.info("Thread started")

        logging.info("SuperResolution service initialized")

    def mkdir_p(self, path):
        try:
            os.makedirs(path)
        except OSError as exc:
            if exc.errno == errno.EEXIST and os.path.isdir(path):
                pass
            else:
                raise

    def psnr(self, y_true, y_pred):
    #    return -10*K.log(K.mean(K.flatten((y_true - y_pred))**2)) / np.log(10)
        return 0.0 * y_pred

    def custom_loss(self, y_true, y_pred):
        is_mean=True
        epsilon = 1e-6
        char = K.mean(K.sqrt(K.square(y_true - y_pred)+epsilon))
        y_true_d = tf.image.resize_bilinear(y_true,(32,32))
        y_pred_d = tf.image.resize_bilinear(y_pred,(32,32))
        char_l =  K.mean(K.sqrt(K.square(y_true_d - y_pred_d)+epsilon))
        loss = char+0.5*char_l
        return loss

    # This is the thread code, which listens for new tasks, validates the input and distributes them to the appropriate processing function
    def process_task(self):

        self.comm = Communicator()

        while True:
            json_task = self.comm.getNextTask()

            operation = json_task["operation"]
            reason = json_task["reason"]
            data = json_task["data"]
            taskId = data["taskId"]

            isCancelled = self.comm.isCancelled(taskId)
            if isCancelled and str(isCancelled) == "True":
                self.comm.send_res_cancelled_message(taskId, "Execution cancelled")
                continue

            self.comm.send_inf_processing_message(taskId, "Execution started")

            if "outputFolder" in data and data["outputFolder"] and data["outputFolder"].strip():
                output_folder = data["outputFolder"]
                self.mkdir_p(output_folder)
            else:
                output_folder = "/ASGARD_DATA/"

            if "lossless" in data and data["lossless"]:
                b_lossless = data["lossless"]
            else:
                b_lossless = False

            if not isinstance(b_lossless, bool):
                if b_lossless in ["true", "True", "TRUE"]:
                    b_lossless = True

            if "startFrame" in data and data["startFrame"]:
                startFrame = int(data["startFrame"])
            else:
                startFrame = 0

            if "endFrame" in data and data["endFrame"]:
                endFrame = int(data["endFrame"])
            else:
                endFrame = 0

            if "location" in data and data["location"] and data["location"].strip():
                fpath = data["location"]
                outputFile = ""

                if fpath.endswith(('.png', '.jpg', '.jpeg', '.bmp', '.PNG', '.JPG', '.JPEG', '.BMP')):
                    self.process_image(fpath, output_folder, taskId)

                elif fpath.endswith(('.mp4', '.avi', '.wmv', '.mov', '.MP4', '.AVI', '.WMV', '.MOV')):
                    self.process_video(fpath, output_folder, startFrame, endFrame, b_lossless, taskId)

                elif os.path.isdir(fpath):
                    self.process_image_folder(fpath, output_folder, taskId)

                logging.info("task completed")
            else:
                self.comm.send_res_failed_message(taskId, "Execution rejected", "a non-empty location must be provided")
                logging.info("task failed")


    # The first of the three modes is for processing images
    def process_image(self, url, save_path, taskId):
        try:

            self.comm.send_inf_processing_message(taskId, "Single image to be processed", "Single image task retrieved from task queue", 0.0, 5)

            if url.startswith("http"):
                frame = np.asarray(bytearray(urlopen(url).read()), dtype='uint8')
                frame = cv2.imdecode(frame, cv2.IMREAD_COLOR)
            else:
                frame = cv2.imread(url, cv2.IMREAD_COLOR)

            image_ycrcb = cv2.cvtColor(frame,cv2.COLOR_BGR2YCrCb)
            image = image_ycrcb[:,:,0]/255.
            image = image.reshape(1,image.shape[0],image.shape[1],1)
            with graph.as_default():
                output,_= self.model.predict(image)
            output = output.squeeze(0)*255.
            output= output.clip(0, 255)
            image_bic = resize(image_ycrcb,(image_ycrcb.shape[0]*self.xfactor,image_ycrcb.shape[1]*self.xfactor,image_ycrcb.shape[2]),order= 3)*255.
            output = np.dstack((output[:,:,0],image_bic[:,:,1],image_bic[:,:,2]))
            output = np.around(output).astype(np.uint8)
            output = cv2.cvtColor(output,cv2.COLOR_YCrCb2BGR)
            a = urlparse.urlparse(url)
            filename = os.path.basename(a.path)
            out_file = os.path.join(save_path, filename) + '_SR' + str(self.xfactor) + '.png'
            cv2.imwrite(out_file, output)

            self.comm.send_inf_processing_message(taskId, "Single image processed", "Single image task completed", 1.0, 0)
            self.comm.send_res_completed_message(taskId, "Finished", {"outputFile": out_file})

        except IOError:
            logging.info("error loading image")
            self.comm.send_res_failed_message(taskId, "Invalid input", "Error loading image")

        except ResourceExhaustedError:
            logging.info("image dimensions too big")
            self.comm.send_res_failed_message(taskId, "Invalid input", "Image dimensions too big")

    # The second of the three modes is for processing image folders
    def process_image_folder(self, url, save_path, taskId):
        try:
            file_types = ('/*.jpg', '/*.jpeg', '/*.png', '/*.bmp', '/*.JPG', '/*.JPEG', '/*.PNG', '/*.BMP')
            imlist = []
            for files in file_types:
                imlist.extend(glob.glob(url + '/' + files))
            imlist = sorted(imlist)
            num_of_files = len(imlist)

            self.comm.send_inf_processing_message(taskId, "Image folder to be processed", "Image folder task retrieved from task queue", 0.0, 5*num_of_files)

            file_num = 0
            t_start = time.time()
            for img1 in imlist:

                isCancelled = self.comm.isCancelled(taskId)
                if isCancelled and str(isCancelled) == "True":
                    self.comm.send_res_cancelled_message(taskId, "Execution cancelled")
                    return

                logging.info("running for image " + img1)
                frame = cv2.imread(img1, cv2.IMREAD_COLOR)
                image_ycrcb = cv2.cvtColor(frame,cv2.COLOR_BGR2YCrCb)
                image = image_ycrcb[:,:,0]/255.
                image = image.reshape(1,image.shape[0],image.shape[1],1)
                with graph.as_default():
                    output,_= self.model.predict(image)
                output = output.squeeze(0)*255.
                output= output.clip(0, 255)
                image_bic = resize(image_ycrcb,(image_ycrcb.shape[0]*self.xfactor,image_ycrcb.shape[1]*self.xfactor,image_ycrcb.shape[2]),order= 3)*255.
                output = np.dstack((output[:,:,0],image_bic[:,:,1],image_bic[:,:,2]))
                output = np.around(output).astype(np.uint8)
                output = cv2.cvtColor(output,cv2.COLOR_YCrCb2BGR)
                a = urlparse.urlparse(img1)
                filename = os.path.basename(a.path)
                out_file = os.path.join(save_path, filename) + '_SR' + str(self.xfactor) + '.png'
                cv2.imwrite(out_file, output)

                file_num = file_num + 1
                percentage = file_num / float(num_of_files)
                eta = 5*(num_of_files - file_num)

                t_stop = time.time()
                # send ETA every SEND_ETA_EVERY_SECS secs
                if t_stop - t_start > SEND_ETA_EVERY_SECS:
                    self.comm.send_inf_processing_message(taskId, "Image folder is processed", "Image folder is processed", percentage, eta)
                    t_start = t_stop

            self.comm.send_res_completed_message(taskId, "Finished", {"outputFile": save_path})

        except IOError:
            logging.info("error loading images from folder")
            self.comm.send_res_failed_message(taskId, "Invalid input", "Error loading images from folder")

        except ResourceExhaustedError:
            logging.info("image dimensions too big")
            self.comm.send_res_failed_message(taskId, "Invalid input", "Image dimensions too big")

    # The third of the three modes is for processing videos
    def process_video(self, url, save_path, startFrame, endFrame, b_lossless, taskId):
        try:

            #self.comm.send_inf_processing_message(taskId, "Video to be processed", "Video task retrieved from task queue", 0.0, 60)

            if url.startswith("http"):
                video = pafy.new(url)
                best = video.getbest(preftype="mp4")
                x = time.strptime(video.duration, '%H:%M:%S')
                duration = timedelta(hours=x.tm_hour, minutes=x.tm_min, seconds=x.tm_sec).total_seconds()
                filename = os.path.join(tempfile.gettempdir(), "%s.%s" % (video.videoid, best.extension))
                if not os.path.isfile(filename):
                    filename = best.download(filepath=filename, quiet=True)
            else:
                filename = url

            #path to save the SR video
            save_name = os.path.basename(os.path.normpath(filename)).rsplit('.',1)[0]

            #Open video and calculate fps
            vid = cv2.VideoCapture(filename)
            if vid.isOpened():
                logging.info("Video is opened")
            else:
                logging.info("Video capturing error")
                self.comm.send_res_failed_message(taskId, "Invalid input", "Error opening video")
                return

            frames = int(vid.get(7))
            eta = frames

            self.comm.send_inf_processing_message(taskId, "Video to be processed", "Video frames ready for processing", 0.0, eta)

            fps = vid.get(5)
            W = int(vid.get(3))
            H = int(vid.get(4))
            logging.info("Video frames: " + str(frames) + ", fps: " +  str(fps) + ", width: " + str(W) + ", height: " + str(H))

            if startFrame < 1:
                startFrame = 1
            elif startFrame > frames:
                startFrame = frames

            if endFrame < 1 or endFrame > frames:
                endFrame = frames

            if startFrame > endFrame:
                startFrame = endFrame

            fourcc = cv2.VideoWriter_fourcc(*'XVID')
            if b_lossless:
                fourcc = cv2.VideoWriter_fourcc(*'HFYU')
            out_file = os.path.join(save_path,save_name) + '_SR' + str(self.xfactor) + '.avi'
            out = cv2.VideoWriter(out_file, fourcc, fps, (self.xfactor*W, self.xfactor*H))
            vid.set(1, startFrame - 1)
            flag = 1
            counter = 0

            t_start = time.time()

            while(flag and vid.get(1) != endFrame ):

                isCancelled = self.comm.isCancelled(taskId)
                if isCancelled and str(isCancelled) == "True":
                    self.comm.send_res_cancelled_message(taskId, "Execution cancelled")
                    out.release()
                    cv2.destroyAllWindows()
                    return

                flag,frame = vid.read()
                frame_ycrcb = cv2.cvtColor(frame,cv2.COLOR_BGR2YCrCb)
                frame = frame_ycrcb[:,:,0]/255.
                frame = frame.reshape(1,frame.shape[0],frame.shape[1],1)
                with graph.as_default():
                    output,_= self.model.predict(frame)
                output = output.squeeze(0)*255.
                output= output.clip(0, 255)
                frame_bic = resize(frame_ycrcb,(frame_ycrcb.shape[0]*self.xfactor,frame_ycrcb.shape[1]*self.xfactor,frame_ycrcb.shape[2]),order= 3)*255.
                output = np.dstack((output[:,:,0],frame_bic[:,:,1],frame_bic[:,:,2]))
                output = np.around(output).astype(np.uint8)
                output = cv2.cvtColor(output,cv2.COLOR_YCrCb2BGR)
                out.write(output)
                counter = counter + 1
                percentage = counter / float(frames)
                t_stop = time.time()

                # send ETA every SEND_ETA_EVERY_SECS secs
                if t_stop - t_start > SEND_ETA_EVERY_SECS:
                    logging.info("Processed frame " + str(counter))
                    percentage = counter / float(frames)
                    eta = frames - counter
                    self.comm.send_inf_processing_message(taskId, "Video frames are processed", "Video frames are processed", percentage, eta)
                    t_start = t_stop

            logging.info("done")
            out.release()
            cv2.destroyAllWindows()

            self.comm.send_res_completed_message(taskId, "Finished", {"outputFile": out_file})

        except ResourceExhaustedError:
            logging.info("video dimensions too big")
            self.comm.send_res_failed_message(taskId, "Invalid input", "Video frame dimensions too big")
