
#libraries
import pandas as pd
import numpy as np
from datetime import datetime
import glob
from pathlib import Path
import json
import os
import shutil
import traceback
import logging
import time

# project scripts
from configfile import ConfigFile
from shellingKWs import shellingKWS
from shelling_SA import shellingSA
import shellingLDA
import shelling_bestklda as bestklda
import topic_profiler as tp



class ServiceInterface:

    config_file_path = './config_files/config_file_default.json'
    config_file_json = None
    shelling_kws = shellingKWS()

    monouser_locked = False

    def __init__(self):
        self.monouser_locked = False
        with open(self.config_file_path, 'r') as cfile:
            self.config_file_json = json.load(cfile)

    def shellingKWs_by_par(self, lang=["en"], project_name="asgard", ElasticSearch=True, docline=True, num_topics=5, default_stopwords="yes", project_stopwords=["quote", "posted", "https", "http"], default_stoptags=["DT", "IN", "PRP", "PRP$", "CC", "CD", "MD", "WRB", "Fit", "Fg", "POS", "RB", "WP", "TO", "WDT"], lemmatizer="Wordnet Lemmatizer", threshold_freq_terms=10, matching_files=["./corpus/14Words_of_Truth"], ner=["EVENT"]):
        """
        Extracts keywords from a raw data file.
        Keywords  can be:
        - SN: Noun phrase (ES: sintagma nominal)
        - NE: Named Entities established by the user in input (EVENT, LOC, etc.)

        Args:
            param1 (int): The first parameter.
            lang (array(str)): languages in the raw data content.
            project_name (str): project name.
            ElasticSearch (str): "yes"/"no" if requires ElasticSearch.
            docline (str): "yes"/"no" each line is a document.
            num_topics (int): number of topics for the LDA.
            default_stopwords (str): 3080/asgard/execution-dev.html"yes"/"no" if requires default stopwords.
            project_stopwords (array(3080/asgard/execution-dev.htmlstr)): specific stopwords for the project (e.g.["quote", "posted", "https", "http"]).
            default_stoptags (array(s3080/asgard/execution-dev.htmltr)): defines stoptags (e.g. ["DT", "IN", "PRP", "PRP$", "CC", "CD", "MD", "WRB", "Fit", "Fg", "POS", "RB", "WP", "TO", "WDT"]).
            lemmatizer (str): defines3080/asgard/execution-dev.html the used lemmatizer (e.g."Wordnet Lemmatizer").
            threshold_freq_terms (int): ask Quim.
            matching_files (str): filepath of the analyzed files.
            ner (arr(str)): array of the named entities used.

        Returns:
            dict : dictionary with the nps_in_sentences (arr(str)) and ne_in_sentences (arr(str))
        """
        pass

    def shellingKWs(self, config_file_json):
        """
        Extracts keywords from a raw data file.
        Keywords  can be:
        - SN: Noun phrase (ES: sintagma nominal)
        - NE: Named Entities established by the user in input (EVENT, LOC, etc.)

        Args:
            config_file_path (json): json with the config file for the execution

        Returns:
            dict : dictionary with the nps_in_sentences (arr(str)) and ne_in_sentences (arr(str))
        """

        print("Executing shellingKWs.....\r")
        out_json = self.shelling_kws.get_kws(config_file_json)
        print("Executing shellingKWs..... COMPLETED")
        return out_json

        pass

    def shelling_bestklda(self, shelling_kws):
        """
        Get the best ntopics with the graph interpretation
        coherence_score/num_topics to help setting the LDA parameters.

        Args:
            config (json?): config file for the execution
            nps_in_sentences (arr(arr(str))): keywords of each file.
            ne_in_sentences (arr((str))): necessary? ASK QUIM.
        Returns:
            int : best ntopics
        """

        return self.bestklda(shelling_kws)

    def shellingLDA(self, nps_and_nevents, config_file=shelling_kws):
        """
        Generates the LDA for the input nps/ne specified.

        Args:
            config (json?): config file
            nps_and_nevents (JSON): NPs and NE results from
            ntopics (int): number of topics

        Returns:ps
            dictps
                ps
                psn UMAP
                psn UMAP
                ps
                ps
                ps
        """
        ntopics = self.config_file_json['project']['num_topics']
        output_topic_file = self.out_dir+'/doc_topic_file.csv'
        output_term_topic_file = self.out_dir+'/doc_term_file.csv'

        if not isinstance(ntopics, int) or ntopics < 0:
            raise Exception(
                'The number of topics has incorrect Value -> ', ntopics)
        if isinstance(nps_and_nevents, str):
            raise Exception('KWs OUTPUT in invalid format. It requires JSON.')

        print("Executing shellingLDA.....\r")
        topic_df, term_df = shellingLDA.lda(nps_and_nevents, ntopics,
                                            output_topic_file, output_term_topic_file)
        print("Executing shellingLDA..... COMPLETED")
        return topic_df, term_df

    def shellingSA(self, config_file_str=[]):
        """
        Returns the Sentiment Analysis of the specified document

        Args:
            config (json): config file
        Returns:
            json: it returns a json with the scores for each document ([{doc_id: X, score: Y}...])
        """

        out_json = {}
        print("Executing shellingSA..... \r")
        out_json = shellingSA(self.config_file_json)
        print("Executing shellingSA..... COMPLETED")
        return out_json

        pass

    def shellingWord2Vec(sefl,):
        pass

    def corpus_uploading(self, req_in):
        """
        This function uploads a post to ElasticSearch

        Args:
            req_in (dict): dictionary with the 'metada', 'config_file_jsondata'.

        Returns:
            The return value. True for success, False otherwise.
        """

        # post_date_str = post_date.strftime('%Y-%m-%d %H:%M:%S')
        # print((author_id, post_date, post_text))

        p = Post(author_id=req_in['author'], date=['date'], language=[
                 'language'], repository="stormfront", data=["data"])
        p.save()

        return None

    def selection_by_metada(self, req_in):
        """
        This function returns from ElasticSearch, the files which matches the specified metadata
config_file_json
config_file_json
config_file_jsondata'.
config_file_json
config_file_json
config_file_json
config_file_json
config_file_json
config_file_json
            }

        Returns:
            The elastic-search-id of the all files which matched the elastic search.
            {
                'files': [
                    file-id-1 (string),
                    file-id-2 (string),
                    ...
                    file-id-N (string),
                ]
            }
        """

        output = self.search_post.match_metadata_doc_id(req_in, self.es_client)

        return output

    def lda_execution(self, req_in):
        """
        This function upload the the files
            {
                'metadata':{
                        user_identifier:,
                },
                'files': [
                    file-id-1 (string),
                    file-id-2 (string),
                    ...
                    file-id-N (string),
                ]
            }

        Returns:
            LDA topics and the predicates for each topic. Each predicate
            {
                LDA: {
                    topic1:[ {
                            'forms': (string),
                            'lemma': (string),
                            'lda-score': 0.5 (int)
                        },
                        ..., {}
                    ],
                    ...
                    topicN:{...}
                }
            }
            'files': [
                file-id-1 (string),
                file-id-2 (string),
                ...
                file-id-N (string),
            ]
        """
        output = {}
        output_LDA = lda(req_in)
        output["LDA"] = json.loads(output_LDA)

        input_JSON = json.loads(req_in)
        output['matching-files'] = input_JSON['matching-files']
        response = json.dumps(output)

        return response

    def filter_by_keywords(self, req_in):
        """
        This function gets paragraphs with keywords data_str
        Args:
            req_in (dict): dictionary with the key(keyword) and value({forms,lemma,score})

        Returns:
            The return value. For each keywords, returns the the paragraph_id and file_id where the keyword appeared
        """
        # create dictionary dict(lemma) -> [forms]
        input_JSON = json.loads(req_in)
        dictio_lemma = {}
        arr_keywords = input_JSON['keywords']
        for k in arr_keywords:
            clean_forms = []
            for f in k['forms']:
                f = f.replace('_', ' ')
                clean_forms.append(f)
            k['forms'] = clean_forms
            dictio_lemma[k['lemma']] = k['forms']
        with open('dics/dictio_lemma.json', 'w') as outfile:
            json.dump(dictio_lemma, outfile)

        # filter by keywords
        f = FilterByKW(req_in)
        output_dict = f.get_pa()
        output_str = json.dumplda
        return output_str

    def selection_by_content_sentiment_analysis(self, input_str):
        """
        This function uplServiceInterfacee files

        Args:
            req_in (dict):ServiceInterfacey with the 'metada', 'data'.

        Returns:
            The return ServiceInterfacefor success, False otherwise.
        """

        input_JSON = json.loads(input_str)
        output_str = sentiment_analysis(input_JSON)
        return output_str

    def snippet_collector(self, req_in):
        """
        This function returns the sentiment analysis score with and the whole paragraph.

        Args:
            req_in (dict):ServiceInterfacey with the 'metada', 'data'.

        Returns:
            The return ServiceInterfacefor success, False otherwise.
        """
        sc = snippet_collector()
        output = sc.snippet_collector(req_in)

        return output

    def get_author(self, req_in):
        """
        This function returns the sentiment analysis score with and the whole paragraph.

        Args:
            req_in (dict):ServiceInterfacey with the 'metada', 'data'.

        Returns:
            The return ServiceInterfacefor success, False otherwise.
        """
        author_JSON = json.loads(req_in)
        user = SFUser.get(author_JSON['user_id'])

        return json.dumps({"username": user.username})

    def get_all_file(self, req_in):
        """
        This function returns the filter_by_keywordsis score with and the whole paragraph.

        Args:
            req_in (str): file idenfilter_by_keywordscSearch

        Returns:
            The return: for success, False otherwise.
        """
        in_JSON = json.loads(req_in)
        mypost = Post.get(in_JSON['id'], using=self.es_client)
        output = {}
        output["content"] = mypost.contents

        return json.dumps(output)

    def msg_maker(self, operation, reason, message):
        '''
        200 {"operation": "RES_FAILED", "reason": "Task id not found", "message": "Task id not found"}
        200 {"operation": "RES_FAILED", "reason": "Execution rejected", "message": "a non-empty location must be provided"}
        200 {"operation": "RES_FAILED", "reason": "Invalid input", "message": "Image dimensions too big"}
        200 {"operation": "RES_STARTED", "reason": "Execution accepted"}
        200 {"operation": "INF_PROCESSING", "reason": "Execution started"}
        200 {"operation": "INF_PROCESSING", "reason": "Video frames are processed", "message": "Video frames are processed", "percentage": 0.9023, "eta": 124}
        200 {"operation": "RES_COMPLETED", "reason": "Finished", "data": {"outputFile": "/ASGARD_DATA/result/test_out_mp4"}}
        200 {"operation": "RES_CANCELLED", "reason": "Execution cancelled"}
        '''
        if operation in ("RES_FAILED", "RES_STARTED", "INF_PROCESSING", "RES_COMPLETED", "RES_CANCELLED"):
            msg = {}
            msg['operation'] = operation
            msg['reason'] = reason
            if operation in ("RES_COMPLETED"):
                msg['data'] = message
            else:
                msg['message'] = message
            msg_str = json.dumps(msg)
            json_msg = json.loads(msg_str)
            return json_msg
        else:
            return None

    def MUI_service(self, req_in):
        # For Orchestrator MUI format

        # **************************************
        # input file from JSON request
        # **************************************
        if isinstance(req_in, str):
            req_in = json.loads(req_in)["data"]
        else:
            return {"operation": "RES_FAILED", "reason": "Execution rejected",
                    "message": "request input data is not a string"}
        if isinstance(req_in["input_path"], str):
            input_path = str(req_in["input_path"])
            if not os.path.exists(input_path):
                return {"operation": "RES_FAILED", "reason": "Execution rejected",
                        "message": "input file  cannot be found."}
        else:
            return {"operation": "RES_FAILED", "reason": "Execution rejected",
                    "message": "input data is not a dictionary"}
        if isinstance(req_in["best_cluster_size"], str):
            best_cluster_size = str(req_in["best_cluster_size"])
        else:
            return {"operation": "RES_FAILED", "reason": "Execution rejected",
                    "message": "best_cluster_size is not correct. Possible options: yes / y / 1"}
        if isinstance(req_in["ntopics"], int) and req_in["ntopics"] > 0:
            # ntopics = self.config_file_json['project']['num_topics']
            ntopics = req_in["ntopics"]
        else:
            return {"operation": "RES_FAILED", "reason": "Execution rejected",
                    "message": "Number of topics incorrect. It has to be a number bigger than 0."}
        # **************************************
        # call function
        # **************************************
        if self.monouser_locked == True:
            return {"operation": "RES_FAILED", "reason": "Execution rejected", "message": "Execution rejected. Monouser tool and another persons is executing it. Please wait."}
        else:
            self.monouser_locked = True
            try:
                json_docs, output_filepath = self.test_case_v1_3(
                    input_path, ntopics, best_cluster_size)
            except Exception as e:
                logging.info(traceback.format_exc())
                # there was an error
                self.monouser_locked = False
                return {"operation": "RES_FAILED", "reason": "Execution rejected", "message": str(e)+" // "+traceback.format_exc()}

        self.monouser_locked = False

        # MUI message
        return self.msg_maker("RES_COMPLETED", "Finished", {"myoutput": json_docs, "output_file": output_filepath})

    def corpus_filtering(self):
        """
        Executes the script for retrieving data from stormfront.
        The script uses MySQL database in a docker container shared in ASGARD

        Args:
            output_file (str): path of the output file
            user_id (int): user identifier of stormfront
            date_start (datetime): initial date of the period filtered
            date_final (datetime): final date of the period filtered

        Raises:

        Returns:
            output_file: raw data text file with the
        """
        import py_compile

        args = {
            "user_id": 572041,  # int
            "out-file": "/ASGARD_DATA/corpus_filtered_output.json",  # integer
            "date-start": "1990-01-01",  # YYYY-MM-DD
            "date-end": "2022-01-01",  # YYYY-MM-DD
            "mysql-user": 'asgard',
            "mysql-pass": 'asgard',
            "mysql-host": 'localhost',
            "mysql-port": '3306',
            "mysql-db": 'stormfront_preprocessed'
        }

        cmd_line = './corpus/corpus_stormfrontDB_to_IFPTA_corpus.py {uid} --out-file {of} --date-start {ds} --date-end {de} --mysql-user {mu} --mysql-pass{mpass} --mysql-host {h} --mysql-port {mp} --mysql-db {mdb}'
        cmd_line_formatted = cmd_line.format(uid=args["user_id"], of=args["out-file"], ds=args["date-end"],
                                             de=args["date-end"], mu=args["mysql-user"], mpass=args["mysql-pass"], h=args["mysql-host"], mp=args["mysql-port"], mdb=args["mysql-db"])
        py_compile.compile(cmd_line_formatted)

        # print('output: {}'.)

        # return json_docs, os.path.abspath(output_filepath)

    ##############################################################
    ################## SIGNAL ALARM settings #####################
    ##############################################################
    def state_handler():
        pass

    def state_sender(self, parameter_list):
        import signal
        # Set the signal handler and a 5-second alarm
        signal.signal(signal.SIGALRM, state_handler)
        signal.alarm(5)
        pass

    ##############################################################
    ################## /SIGNAL ALARM settings ####################
    ##############################################################

    def print_timer(self, name, t):
        print("######################################")
        out = "{} -> {} sec.".format(name, t)
        print(out)
        print("######################################")

    def test_case_v1_3(self, input_path, ntopics=False):
        """
        This function exectues the concatenation:
            1. selection_by_metada()
            2. lda_execution()
            3. filter_by_keywords()
            4. selection_by_content_sentiment_analysis()
            5. snippet_collector()
        """

        # process path:
        # - if path is file
        # - if path is directory, take all files in the directory
        if os.path.isdir(input_path):
            # get all files in input_path
            input_files = glob.glob(input_path)
            self.config_file_json["project"]["matching_files"] = input_files
        elif os.path.isfile(input_path):
            self.config_file_json["project"]["matching_files"] = [
                input_path]
        else:
            # then, what is it?
            # return error: not file and not directory
            pass

        # make the output directory
        now = datetime.now().strftime("%Y%m%d_%H.%M.%S")
        OUTPUT_LOCAL = "./output/ifpta-{}/exec_{}"
        OUTPUT_TMP = "/tmp/ifpta-output-{}/exec_{}"
        OUTPUT_DOCKER = "/ASGARD_DATA/BSC-iftpa/{}/exec_{}"
        OUTPUT_MUI_CASE = os.path.dirname(input_path)
        OUTPUT_MUI_EXEC = OUTPUT_MUI_CASE + '/{}_exec_{}'
        logging.info("OUTPUT_MUI: " + OUTPUT_MUI_EXEC)
        OUTPUT_PATH = OUTPUT_MUI_EXEC
        in_filename = os.path.basename(input_path).split(".")[0]
        self.out_dir = OUTPUT_PATH.format(in_filename, now)
        logging.info("OUTPUT_PATH: " + OUTPUT_PATH)
        os.makedirs(self.out_dir, exist_ok=True)

        # default polarity
        polarity = [-1, 0, 1]

        # set default threshold frequency terms // default: 10
        nterms = self.config_file_json['project']['threshold_freq_terms']

        # Get NPs and NEs
        # NPs: noun phrases
        # NEs: named entities

        start = time.time()  # timer
        kws_json = self.shellingKWs(self.config_file_json)
        end = time.time()  # timer
        self.print_timer("shellingKWs()", end-start)  # timer

        kws_common_path = self.out_dir + '/output-shellingKWs.json'
        with open(kws_common_path, 'w') as file:
            json.dump(kws_json, file)

        # calculate the best cluster size for LDA
        # will be done when taking the NPs
        if ntopics is False:
            # bestklda
            start = time.time()  # timer
            self.config_file_json["project"]["num_topics"] = self.shelling_bestklda(
                kws_json)
            end = time.time()  # timer
            self.print_timer("shelling_bestklda()", end-start)  # timer
        else:
            self.config_file_json["project"]["num_topics"] = ntopics

        '''
        df_kws = pd.DataFrame.from_dict(kws_json)
        df_kws.to_csv(self.out_dir+'/output-shellingKWs.csv')
        df_kws.to_json(self.out_dir+'/output-shellingKWs-2.json')
        '''

        # shellingLDA
        nps_and_nevents = kws_json
        start = time.time()  # timer
        df_lda_topic, df_lda_term = self.shellingLDA(nps_and_nevents)
        end = time.time()  # timer
        self.print_timer("shellingLDA()", end-start)  # timer
        df_lda_topic.to_csv(self.out_dir+'/df_lda_topic.csv')
        df_lda_term.to_csv(self.out_dir+'/df_lda_term.csv')

        # shelling_SA
        start = time.time()  # timer
        df_sa_doc = self.shellingSA()
        df_sa_doc.to_csv(self.out_dir+'/output_SA.csv')
        end = time.time()  # timer
        self.print_timer("shellingSA()", end-start)  # timer

        # Filtering the documents
        df_kws = pd.DataFrame.from_dict(kws_json)
        df_lda_term = pd.read_csv(self.out_dir+'/df_lda_term.csv')
        df_lda_topic = pd.read_csv(self.out_dir+'/df_lda_topic.csv')
        df_sa_doc = pd.read_csv(self.out_dir+'/output_SA.csv')
        start = time.time()  # timer
        json_docs = self.filter_topics_polarities(
            df_kws, df_lda_term, df_lda_topic, df_sa_doc, ntopics, nterms, polarity)
        end = time.time()  # timer
        self.print_timer("filter_topics_polarities()", end-start)  # timer

        #Calculate the topic matching (weapons, politics, sex, drugs)
        topics_matching = tp.topic_profiler(input_path)
        json_docs['topics'] = topics_matching

        # save the output in file
        output_filepath = '{}/output-{}-{}.json.txt'.format(
            OUTPUT_MUI_CASE, in_filename[0:10], now)
        abs_fpath = os.path.abspath(output_filepath)
        with open(output_filepath, 'w') as fout:
            json.dump(json_docs, fout)
        # create smylink for the GUI
        output_latest_name = '{}/last-output.json'.format(self.out_dir)
        os.symlink(abs_fpath, output_latest_name+'.tmp')
        os.rename(output_latest_name+'.tmp', output_latest_name)

        # return json_docs, os.path.abspath(output_filepath)
        out_path = os.path.abspath(output_filepath)
        gui_view = 'http://127.0.0.1.xip.io:55500/gui?infile={}'.format(
            out_path)
        out_json = {}
        out_json['topics'] = topics_matching
        out_json['out-file'] = out_path
        out_json['gui-url'] = gui_view
        return out_json

    def filter_topics_polarities(self, df_kws, lda_term_df, lda_topic_df, sa_df, ntopics, nterms, polarities):

        # set the number of topics in a list
        ncols = len(lda_term_df.columns.values.tolist())
        cols = lda_term_df.columns.values.tolist()
        nt = 0
        for c in cols:
            if isinstance(c, str) and c.startswith('Prob'):
                nt += 1

        if ntopics != nt:
            # error! not enough content
            # ntopics = nt
            raise Exception('File too small: minimum 15 lines and 1000 words.')
        else:
            pass

        logging.info(">>>>> NTOPICS = {}, <<<< NTOPICS = {}".format(
            str(lda_term_df.columns.values.tolist()), str(nt)))
        topics = list()
        for t in range(ntopics):
            topics.append(t)

        results = {}
        df_docs_full = pd.DataFrame(
            [], columns=["topic_lda", "bag_words", "sa_score", "events", "rank"])

        logging.info(">>>>> DATAFRAME columns = " +
                     str(lda_term_df.columns.values.tolist()))

        for t in topics:
            for p in polarities:

                self.casename = "t{}_p{}".format(t, p)

                # mkdir to store the results
                self.out_dir_t_p = "{}/t{}_p{}".format(self.out_dir, t, p)
                os.makedirs(self.out_dir_t_p, exist_ok=True)

                # call the filtering (one per polarity & topic)
                df_docs, top_topic_terms = self.filter_topic_nterms_polarity(
                    df_kws, lda_term_df, lda_topic_df, sa_df, t, nterms, p)

                df_docs.to_csv(self.out_dir_t_p+'/df_docs_filtered.csv')
                top_topic_terms.to_csv(self.out_dir_t_p+'/top_topic_terms.csv')

                df_docs_full.append(df_docs)

                # Get los docs del corpus inicial!
                df_corpus = pd.read_csv(
                    self.config_file_json['project']['matching_files'][0], sep='\n', header=None, names=['text'])

                '''
                # A) YES, drop duplicates when using stormfront doc_id csv
                df_selection = df_corpus.loc[df_docs.index.values, ['text']]
                df_selection['doc_id'] = df_selection.index
                df_selection.drop_duplicates(
                    subset=["text"], keep="first", inplace=True)
                df_selection.to_csv(self.out_dir_t_p+'/'+self.casename+'docs_dropped_duplicates.csv')

                '''
                # B) drop duplicates
                # requires setting an index
                # df_docs.reset_index()
                # df_docs.reindex(index=range(0, len(df_docs)))
                df_docs.to_csv(self.out_dir_t_p+'/df_docs.csv')
                print("df_docs.index.values", df_docs.index.values)
                df_selection = df_corpus.loc[df_docs.index.values, ['text']]
                df_selection['doc_id'] = df_selection.index
                df_selection['events'] = df_docs["events"]
                df_selection.drop_duplicates(
                    subset=["text"], keep="first", inplace=True)
                df_selection.to_csv(
                    self.out_dir_t_p+'/docs_dropped_duplicates.csv')
                '''
                df_docs['text'] = df_corpus.loc[df_docs.index.values, ['text']]
                df_docs['doc_id'] = df_selection.index
                df_docs.drop_duplicates(
                    subset=["text"], keep="first", inplace=True)
                df_docs.to_csv(
                    self.out_dir_t_p+'/'+self.casename+'df_docs_dp_duplicates_XXXX.csv')
                '''
                # convert to json
                json_df_docs = df_selection.to_json(orient='records')
                # msg_str = json.dumps(json_df_docs)
                json_df_docs = json.loads(json_df_docs)

                key = "t{}_p{}".format(t, p)
                results[key] = {
                    "posts": json_df_docs,
                    "relevant_terms": {
                        "text": top_topic_terms["Term"].tolist(),
                        "prob": top_topic_terms["Prob-"+str(t)].tolist()
                    }
                }
        df_docs_full.to_csv(self.out_dir+'/df_docs_full.csv')

        return results

    def filter_topic_nterms_polarity(self, df_kws, lda_term_df, lda_topic_df, sa_df, topic, nterms, polarity):

        df = pd.DataFrame(columns=['topic_lda', 'bag_words', 'sa_score'])
        df['bag_words'] = df_kws['nps_in_sentences']
        df['events'] = df_kws['ne_in_sentences']
        df['sa_score'] = sa_df['sa_score']
        df['topic_lda'] = lda_topic_df['label']
        df.to_csv(self.out_dir+'/joined.csv')

        # get nterms more probably pertain at each topic
        # filter the polarity
        df_polarized = df.loc[(df['sa_score'] == polarity)
                              & (df['topic_lda'] == topic)]
        fpath_polarize = '{}/{}_polarity.csv'.format(
            self.out_dir, self.casename)
        df_polarized.to_csv(fpath_polarize)

        # get topic terms
        # requires sorting probability of the topic
        topic_terms = {}
        colname = "Prob-{}".format(topic)
        sorted_df = lda_term_df.sort_values(by=[colname], ascending=False)
        # get the top nterms. highest probablity
        topic_terms['topic_terms'] = sorted_df.head(nterms)['Term'].values
        topic_terms['topic_terms_prob'] = sorted_df.head(nterms)[
            colname].values
        top_topic_terms = sorted_df.head(nterms)

        # df_topic_terms = df_polarized[df_polarized.apply(lambda x: len(
        # set(x['bag_words']).intersection(topic_terms['topic_terms'])) > 0, axis=1)]

        # get rows whose bag_words contains at least one "top 10 topic_terms"

        if len(df_polarized) > 0:
            df_polarized = df_polarized[df_polarized.apply(lambda x: len(
                set.intersection(set(x['bag_words']), set(topic_terms['topic_terms']))) > 0, axis=1)]
        else:
            pass

        def a_func(x):
            rank = 0
            inter = set.intersection(
                set(x['bag_words']), set(topic_terms['topic_terms']))
            for t in inter:
                prank = sorted_df.loc[sorted_df['Term']
                                      == t][colname].values[0]
                if prank > rank:
                    rank = prank
            return rank

        df_docs = df_polarized

        if(len(df_docs.index) > 0):
            df_docs['rank'] = df_polarized.apply(a_func, axis=1)
            df_docs = df_docs.sort_values(by=['rank'], ascending=False)
        else:
            pass

        return df_docs, top_topic_terms

    def test_case(self, req_in):
        """
        This function exectues the concatenation:
            1. selection_by_metada()
            2. lda_execution()
            3. filter_by_keywords()
            4. selection_by_content_sentiment_analysis()
            5. snippet_collector()
        """

        if self.monouser_locked == True:
            return {'status': 'Error(XXX) Monouser-restriction: another persons is Running. Please wait.'}
        else:

            self.monouser_locked = True
            try:
                input_JSON = json.loads(req_in)
                in1 = {}
                in1['metadata'] = input_JSON['metadata']

                output1 = self.selection_by_metada(json.dumps(in1))

                self.write_str(
                    '/ASGARD_DATA/bsc-shelling/out/output1.json', output1)

                output1_JSON = json.loads(output1)
                output1_JSON['matching-files'] = output1_JSON['matching-files'][:1000]

                output2 = self.lda_execution(json.dumps(output1_JSON))
                self.write_str(
                    '/ASGARD_DATA/bsc-shelling/out/output2.json', output2)

                output2_JSON = json.loads(output2)
                inputFByKeywords = {}
                inputFByKeywords['matching-files'] = output2_JSON['matching-files']
                inputFByKeywords['keywords'] = []
                for topic in output2_JSON['LDA']:
                    for k in output2_JSON['LDA'][topic]:
                        '''
                        for kw in input_JSON['keywords']:
                            if k['lemma'] == kw:
                                inputFByKeywords['keywords'].append(k)
                            else:
                                # for adding all the keywords (TESTING)
                                inputFByKeywords['keywords'].append(k)
                        '''
                        # test (add all lda-topics)
                        inputFByKeywords['keywords'].append(k)

                self.write_str(
                    '/ASGARD_DATA/bsc-shelling/in/input_keywords.json', json.dumps(inputFByKeywords))
                output3 = self.filter_by_keywords(json.dumps(inputFByKeywords))
                self.write_str(
                    '/ASGARD_DATA/bsc-shelling/out/output3.json', output3)
                output4 = self.selection_by_content_sentiment_analysis(output3)
                self.write_str(
                    '/ASGARD_DATA/bsc-shelling/out/output4.json', output4)
                output5 = self.snippet_collector(output4)
                self.write_str(
                    '/ASGARD_DATA/bsc-shelling/out/output5.json', output5)
            except:
                # there was an error
                logging.info(traceback.format_exc())
                pass

        self.monouser_locked = False

        return output5

    def write_str(self, filename, output):
        with open(filename, 'w') as file:
            file.write(output)


if __name__ == "__main__":

    si = ServiceInterface()
    '''
    df_corpus = pd.read_csv(
        'corpus/14Words_of_Truth.old', sep='\n', header=None, names=['text'])
    df_corpus.drop_duplicates(subset=['text'], keep="first", inplace=True)
    df_corpus.to_csv('corpus/output.new', header=False, index=False)
    
    
    
    # shellingKWs
    json_out = si.shellingKWs(si.config_file_json)
    with open('./output/output-shellingKWs.json', 'w+') as outfile:
        json.dump(json_out, outfile)
    '''
    req_in_0 = {"data": {"input_path": "./corpus/csv_55084_min.csv",
                         "topic": 1, "nterms": 10, "polarity": -1}}
    req_in_1 = {"data": {"input_path": "./corpus/14Words_of_Truth_min.txt",
                         "topic": [0, 1, 2, 3, 4], "nterms": 10, "polarity": [-1, 0, 1]}}
    req_in = {"data": {"input_path": "corpus/14Words_of_Truth_min.txt",
                       "topic": [0, 1, 2, 3, 4], "nterms": 10, "polarity": [-1, 0, 1]}}
    req_in = {"data": {"input_path": "corpus/14Words_of_Truth_min.txt",
                       "topic": [0, 1, 2, 3, 4], "nterms": 10, "polarity": [-1, 0, 1]}}

    req_in = json.dumps(req_in)

    # output = si.test_case_v1_3("corpus/14Words_of_Truth_min.txt", 5)
    '''
    # top 10 users testing
    in_files = ["docperline_572042.txt", "docperline_572095.txt", "docperline_572134.txt", "docperline_572149.txt", "docperline_572266.txt", "docperline_572449.txt",
                "docperline_572804.txt", "docperline_573148.txt", "docperline_573325.txt", "docperline_573700.txt", "docperline_575645.txt"]
    for f in in_files:
        start = time.time()
        output = si.test_case_v1_3("/tmp/"+f, 5)
        end = time.time()
        outstr = "FILE({}), TIME({}) ".format(f, end-start)
        print(outstr)
    '''
    # minitesting
    mini = "./corpus/14Words_of_Truth_min.txt"
    milky = "/tmp/docperline_572266.txt"
    output = si.test_case_v1_3(mini, 5)
    print(output)
    # si.corpus_filtering()

    # print(json.dumps(output))
    '''
    # Generating filtering for all topics and polarities
    for topic in range(0, 5):"Pr
        for polarity in (-1, "Pr
            req_in = {"data":"Pr
                             "Pr: polarity}}
            req_in = json.dum"Pr
            # print(json.dumps(output))
    '''
    '''
    # shellingKWs
    json_out = si.shellingKWs(si.config_file_json)
    with open('./output/output-shellingKWs.json', 'w+') as outfile:
        json.dump(json_out, outfile)

    # shellingSA
    output_shellingKWs = './output/output-shellingKWs.json'

    json_out = si.shellingSA(config_file_str)
    with open('./output/output-shellingSA.json', 'w+') as outfile:
        json.dump(json_out, outfile)
    '''
