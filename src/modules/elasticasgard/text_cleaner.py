import re
from bs4 import BeautifulSoup
from elasticsearch_dsl import Document, Date, Integer, Keyword, Text
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Index
from elasticsearch import Elasticsearch
from asgard_post import Post
from asgard_es_searches import SearchPost

'''
\n	Line Feed
\r	Carriage Return
\r\n	Carriage Return + Line Feed
\v or \x0b	Line Tabulation
\f or \x0c	Form Feed
\x1c	File Separator
\x1d	Group Separator
\x1e	Record Separator
\x85	Next Line (C1 Control Code)
\u2028	Line Separator
\u2029	Paragraph Separator
'''
unicode_chars_re = "[\n\r\v(\x0b)(\f)(\x0c)(\x1c)(\x1d)(\x1e)(\x85)(\u2028)(\u2029)]"
unicode_chars_re_hardcoded = "[(\\n)(\\r)(\\v)(\\x0b)(\\f)(\\x0c)(\\x1c)(\\x1d)(\\x1e)(\\x85)(\\u2028)(\\u2029]"


def posts_cleaner():
    es_client = Elasticsearch('localhost:59200')
    search_posts = SearchPost(
        using=es_client, index='stormfront', doc_type=Post)

    # for hit in response:
    for hit in search_posts.scan():

        p = Post.get(hit.meta.id)
        p.contents = clean_text(p.contents)
        p.save()


def split_paragraphs(text_str):
    lines_pattern = '[\n\r\v\x0b\f\x0c\x1c\x1d\x1e\x85\u2028\u2029]{2,}'
    return re.split(lines_pattern, text_str)
    # return re.split("\n{2,}", text_str)
    # return unicode.splitlines(text_str)


def remove_unnecessary(text_str):
    # strip
    return text_str.strip()


def replace_emoji_for_words(text_str):
    # rep
    return text_str


def replace_hardcoded_breaklines(text_str):
    # replace the \\n for break lines
    patt = ['\n', '\r', '\v', '\x0b', '\f', '\x0c',
            '\x1c', '\x1d', '\x1e', '\x85', '\u2028', '\u2029']
    patt_hard_coded = ['\\n', '\\r', '\\v', '\\x0b', '\\f', '\\x0c',
                       '\\x1c', '\\x1d', '\\x1e', '\\x85', '\\u2028', '\\u2029']

    for i in range(len(patt)):
        text_str = text_str.replace(patt_hard_coded[i], patt[i])
    #text_str = re.sub(unicode_chars_re+"{2,}", "\n", text_str)
    return text_str


def clean_text(text_str):
    soup = BeautifulSoup(text_str, 'html.parser')
    text_str = soup.get_text()
    text_str = remove_unnecessary(text_str)
    text_str = replace_hardcoded_breaklines(text_str)
    text_str = replace_emoji_for_words(text_str)
    return text_str
