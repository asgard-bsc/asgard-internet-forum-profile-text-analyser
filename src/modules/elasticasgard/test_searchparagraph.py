
from datetime import datetime
from elasticsearch_dsl import Document, Date, Integer, Keyword, Text
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search
from elasticsearch import Elasticsearch

from asgard_post import Post
from asgard_paragraph import Paragraph
from asgard_es_searches import SearchParagraph

import json

# create the mappings in elasticsearch
connections.create_connection(hosts=['localhost:59200'])
es_client = Elasticsearch('localhost:59200')
spagrph = SearchParagraph(
    using=es_client, index='stormfront_paragraphs', doc_type=Paragraph)


def test_get_all_paragraphs():
    response = spagrph.get_all(None)

    print(response)
    print(spagrph.count())

    for hit in response:
        print(hit.data)


def test_get_all_partial():
    response = spagrph.get_all(10)
    print(response)
    print(spagrph.count())

    for hit in response:
        '''
        id = hit.get("post_id")
        print (" ************* PARAGRAPHS OF POST ******************************")
        if id is None:
            print ("POST_ID:", hit.post_id)
        '''
        print(" ************* PARAGRAPHS OF POST ******************************")
        print(hit.data)


test_get_all_partial()
