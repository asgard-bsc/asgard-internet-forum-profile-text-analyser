
from datetime import datetime
from elasticsearch_dsl import Document, Date, Integer, Keyword, Text
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search
from elasticsearch import Elasticsearch

from .elasticsearch_modules.asgard_post import Post
from .elasticsearch_modules.asgard_es_searches import SearchPost

import json

# create the mappings in elasticsearch
connections.create_connection(hosts=['localhost'])
es_client = Elasticsearch('localhost:59200')
s = SearchPost(using=es_client, index='stormfront', doc_type=Post)


def test_get_post_by_id(post_id):

    # Post
    p = Post.get("umjw_GUBCp2DG5mguPe-")
    return p


def test_from_dict():
    authors = [119,  163282,  170796,  173893,  179327,
               185316,  219096,  46987,  72763,  78077]

    # create and save and Post
    for a in authors:
        test_author(a)


def test_author(author):
    response = s.posts_author(author)

    print("search size: ", response.count())
    print(response.count())

    for hit in response:
        print(hit.author_id)
        print(hit.date)
        print(hit.meta.id)


def test_from_json():
    query_json = {'metadata': {'author': 119, 'language': 'en'}}
    query_json_str = json.dumps(query_json)
    # query_json_dict = json.loads(query_json)

    print(query_json_str)
    response = s.selection_by_metadata(query_json_str)

    print(response)
    print(s.count())

    for hit in response:
        # print(hit.author_id)
        # print(hit.date)
        print(type(hit))
        print(hit.to_json())


def cleaning_posts():

    query = {
        "from": 0, "size": 10000,
        "query": {
            "query_string": {
                "default_field": "data",
                "query": "(\\n)"
            }
        }
    }
    print("QUERY of all forms", query)
    # only get ids, otherwise `fields` takes a list of field names
    response = s.from_dict(query)

    # print response
    print(response.count())

    for hit in response:
        # print(hit.author_id)
        # print(hit.date)
        post_to_clean = Post.get(hit.meta.id)
        #post_str = post_to_clean.data
        post_to_clean.data = post_to_clean.data.replace("\\n", "\n")
        print(post_to_clean.data)
        #print (post_to_clean.data)
        post_to_clean.save()
        with open('output_breakline.txt', 'w') as file:
            file.write(post_to_clean.data)


# test_author(163282)
# test_get_post_by_id("")
# test_from_dict()
# test_from_json()
cleaning_posts()
'''
# my_post = Post.get(id=25)

# Display cluster health
# print(connections.get_connection().cluster.health())
'''
