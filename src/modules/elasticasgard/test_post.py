
from datetime import datetime
from elasticsearch_dsl import Document, Date, Integer, Keyword, Text
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search
from elasticsearch import Elasticsearch

from asgard_post import Post
from asgard_es_searches import SearchPost
import json

# create the mappings in elasticsearch
connections.create_connection(hosts=['localhost:59200'])
es_client = Elasticsearch('localhost:59200')
s = SearchPost(using=es_client, index='stormfront', doc_type=Post)


def test_get_post_by_id(posts_id):

    # Post
    for p_id in posts_id:
        p = Post.get(p_id)
        print("author: ", p.author_id)
        print("data: ", p.data)


posts_id = ["umjw_GUBCp2DG5mguPe-"]
test_get_post_by_id(posts_id)
