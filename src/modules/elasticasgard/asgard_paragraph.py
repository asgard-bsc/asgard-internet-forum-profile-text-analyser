import json
from datetime import datetime
from elasticsearch_dsl import Document, Date, Integer, Keyword, Text
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search
from elasticsearch import Elasticsearch


class Paragraph(Document):
    post_id = Text()
    data = Text()

    class Index:
        name = 'stormfront_paragraphs'

    def save(self, **kwargs):
        self.created_at = datetime.now()
        return super(Paragraph, self).save(**kwargs)

    def to_json(self):
        output = {}
        output['post_id'] = self.post_id
        output['data'] = self.data


'''
# create and save and Post
query = {
    "query": {
        "match": {
            "language": "es"
        }
    }
}
response = PostSearch.from_dict(query)

print response
print s.count()

for hit in response:
    print(type(hit))
    print(hit.meta.id)
    print(hit.author_id)
    print(hit.date)
    print(hit.language)
    print(hit.repository)
'''
# my_post = Post.get(id=25)


# Display cluster health
# print(connections.get_connection().cluster.health())
