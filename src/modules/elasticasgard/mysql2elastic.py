import mysql.connector
import os
from os import walk
from datetime import datetime
import random
import chardet
import io
import json
from scripts.asgard_post import Post

from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search, Index
from elasticsearch_dsl import connections

'''
MySQL
'''
mydb = mysql.connector.connect(
    user='userAPI',
    passwd='userAPIPassword',
    host='the-redrock-ip',
    port='3306',
    database='StormFrom'
)
mycursor = mydb.cursor()
query = ("SELECT AuthorID, PostDate, PostText FROM StormFromPosts")
mycursor.execute(query)


'''
ElasticSearch
'''
connections.create_connection(hosts=['localhost'], timeout=20)
es_client = Elasticsearch('localhost:59200')

'''
Script code
'''
# Post.init()
#ind = Index('stormfront', using=es_client)
# ind.delete()
#ind = Index('stormfront', using=es_client)
# ind.create()


for (author_id, post_date, post_text) in mycursor:
    #post_date_str = post_date.strftime('%Y-%m-%d %H:%M:%S')
    # print((author_id, post_date, post_text))

    p = Post(author_id=author_id, date=post_date, language="en",
             repository="stormfront", data=post_text)
    p.save()

    # print(to_json["metadata"])

    # TODO: insert data into ElasticSearch

    # connections.create_connection(hosts=['localhost'], timeout=20)
    # s = Search(using=client, index="twitter").filter("term", category="search").query("match", title="python").exclude("match", description="beta")
    # s = Search(using=client, index="twitter").filter("term", category="search").query("match", repository="stormfront")
    # .query("match", title="python").exclude("match", description="beta")


mydb.close()

'''
for (dirpath, dirnames, filenames) in walk('../XSERGI'):
    print(dirpath)
    print(dirnames)
    print(filenames)
    print "***********************************"
'''
