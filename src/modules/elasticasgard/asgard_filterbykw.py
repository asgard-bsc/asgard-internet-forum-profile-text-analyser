import codecs
import json

from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search
from elasticsearch_dsl import connections
from asgard_post import Post
from asgard_paragraph import Paragraph
from asgard_es_searches import SearchPost, SearchParagraph
# import asgard_post
# import asgard_es_searcheu


class FilterByKW:
    '''
    This function returns the paragraphs where the given keywords appear.

    The Input contains the given keywords and each keywords has:
    - lemma: the lemma of the keyword
    - forms: all the forms of the lemma
    - ??? score: LDA score (probably not useful NOW)

    INPUT JSON format.
    {
        ‘keywords’: [ {
            'forms': [(string),...,(string)],
            'lemma': (string),
            'score_lda': 0.5 (int)
            },...,
            {}
        ],
        ‘matching-files’: [
            files-filtered-by-metadata1,
            files-filtered-by-metadata2,
            ...
        ],
    }

    OUTPUT JSON format:
    {
        keyword-1 : [
            {
                “paragraph_id”: (string), 
                “file_id”: (string),
                “score_es”: (float) 
            },
            ...
            {}
        ],
    }



    Attributes:
        attr1 (str): Description of `attr1`.
        attr2 (:obj:`int`, optional): Description of `attr2`.


    '''
    # setup ElasticSearch
    #es_client = Elasticsearch('bsc-shelling-elasticsearch:9200')
    es_client = Elasticsearch('localhost:59200')
    spagrph = SearchParagraph(
        using=es_client, index='stormfront_paragraphs', doc_type=Paragraph)
    kws = []
    files = []

    def __init__(self, input):
        '''
        This method loads the ``json string`` parameter and sets the attributes of the class
        :param input: str in JSON format, for setting the keywords dict
        :type input: str
        :return: dict with the matching results.
        '''

        # load input
        input_JSON = json.loads(input)
        self.kws = input_JSON['keywords']
        self.files = input_JSON['matching-files']

    def get_paragraphs_with_kw(self):
        '''
        Filters the documents for the specified keywords, in the parameter self.kws.
        '''
        return self.get_paragraphs_with_keywords(self.kws)

    def get_paragraphs_with_keywords(self, keywords):
        output = {}
        for kw in keywords:
            lemma = kw['lemma']
            forms = kw['forms']

            # clean forms (new_york -> new york)
            for ind, f in enumerate(forms):
                forms[ind] = f.replace("_", " ")

            # from/size
            response = self.spagrph.get_paragraphs_with_kws(
                forms, self.es_client)

            output[lemma] = []

            # TODO: the scan for doing to the whole code
            # https://elasticsearch-dsl.readthedocs.io/en/latest/api.html?highlight=scan
            # for hit in response.scan():
            '''
            my_es_client = Elasticsearch('bsc-shelling-elasticsearch:9200')
            my_spagrph = SearchParagraph(
                using=my_es_client, index='stormfront_paragraphs', doc_type=Paragraph)
            response = my_spagrph.query("match", data="(jew) or (jews)")
            response = response.filter('terms', tags=['jew'])
            '''

            for hit in response:
                var = {}
                if hasattr(hit, 'post_id'):
                    var['post_id'] = hit.post_id
                    #print ("post id INDEX(", hit._index, ")")
                else:
                    var['post_id'] = 'testing'
                    # print ("get_paragraphs_with_kw: ", type(hit), hit.meta.id)
                    #print ("no post id INDEX(", hit._index, ")")
                var['paragraph_id'] = hit.meta.id
                var['score_es'] = hit.meta.score
                var['forms'] = kw['forms']
                var['lemma'] = kw['lemma']
                #var['score_lda'] = kw['score']
                output[lemma].append(var)
            print("LEMMA-size(", lemma, ") : ", len(output[lemma]))
        return output
