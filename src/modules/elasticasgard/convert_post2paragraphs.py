import json
import re
from datetime import datetime
from elasticsearch_dsl import Document, Date, Integer, Keyword, Text
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search, Index
from elasticsearch import Elasticsearch
from asgard_post import Post
from asgard_paragraph import Paragraph
from asgard_es_searches import SearchPost
import text_cleaner

# create the mappings in elasticsearch
connections.create_connection(hosts=['localhost:59200'], timeout=20)
es_client = Elasticsearch('localhost:59200')
search_posts = SearchPost(using=es_client, index='stormfront', doc_type=Post)
search_paragraphs = Search(
    using=es_client, index='stormfront_paragraphs', doc_type=Paragraph)


def separate_paragraphs(post_str):
    '''
    method for separating strings by 
    - break-line 
    - white spaces >3 
    '''
    lines_pattern = '[\n\r\v\x0b\f\x0c\x1c\x1d\x1e\x85\u2028\u2029]{2,}'
    # return re.split('\n{2,}', post_str)
    return re.split(lines_pattern, post_str)


def delete_index(ind_name):
    ind = Index(ind_name, doc_type=Paragraph, using=es_client)
    ind.delete()


def create_index(ind_name):
    ind = Index(ind_name, doc_type=Paragraph, using=es_client)
    ind.create(using=es_client)


def all_redo():
    ind_name = 'stormfront_paragraphs'
    delete_index(ind_name)
    create_index(ind_name)

    total = 0
    posts = 0
    # for hit in response:
    for hit in search_posts.scan():

        # print ("*******************************************************")
        # print (hit.meta.id)
        # print(hit.data)
        p = Post.get(hit.meta.id)
        paragraphs = text_cleaner.split_paragraphs(p.contents)
        # print ("size: ", len(paragraphs))
        total += len(paragraphs)
        posts += 1
        for par in paragraphs:
            if hasattr(hit.meta, 'id'):
                pgrph = Paragraph(post_id=str(hit.meta.id), contents=str(par))
                pgrph.save()

        # print (p.data)
        # print (paragraphs)

    # response = search_paragraphs.from_dict(query)
    print("posts: ", posts)
    print("paragraphs created: ", search_paragraphs.count())
    avg = total / search_paragraphs.count()
    print(avg)


# create & save paragraph in ElasticSearch
all_redo()

'''
# create and save and Post
query = {
    "query": {
        "match": {
            "language": "es"
        }
    }
}
response = SearchPost.from_dict(query)

print response
print s.count()

for hit in response:
    print(type(hit))
    print(hit.meta.id)
    print(hit.author_id)
    print(hit.date)
    print(hit.language)
    print(hit.repository)

# my_post = Post.get(id=25)


# Display cluster health
# print(connections.get_connection().cluster.health())
'''
