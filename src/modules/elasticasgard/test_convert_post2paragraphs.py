import text_cleaner

with open('/tmp/Downloads/14_all.json', 'r') as file:
    text = file.read()
    text = text_cleaner.clean_text(text)
    paragraphs = text_cleaner.split_paragraphs(text)
    with open('/tmp/local/14_all.txt', 'w') as file:
        file.write(text)

print("paragraphs= ", len(paragraphs))

i = 0
for p in paragraphs:
    with open('/tmp/local/14_files/par_'+str(i)+'.txt', 'w') as file:
        file.write(p)
    i += 1
