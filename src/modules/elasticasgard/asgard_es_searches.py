from datetime import datetime
from elasticsearch_dsl import Document, Date, Integer, Keyword, Text
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search
from elasticsearch import Elasticsearch
import json
import os


class SearchPost(Search):
    #es_hosts = os.environ["ELASTIC_SEARCH_CLIENT"]
    #es_client = Elasticsearch(es_hosts)
    #es_client = Elasticsearch('bsc-shelling-elasticsearch:9200')
    es_client = Elasticsearch('localhost:59200')

    def get_all(self):
        query = {
            "from": 0, "size": 10000,
            "query": {
                "match_all": {}
            }
        }
        # only get ids, otherwise `fields` takes a list of field names
        response = self.from_dict(query)
        return response

    def match_language_exec(self):
        return self.query('match', language='es').execute()

    def match_metadata_doc_id(self, input, client):
        '''
        Searches in ElasticSearch with the matches specified by the parameter

        :param metadata: dict which the metadata that will be queried to ElasticSearch
        :return: dict with the matching results.
        '''
        input_json = json.loads(input)
        metadata = input_json['metadata']
        output = {}
        output["metadata"] = metadata
        output["matching-files"] = []
        query = {
            'from': 0, 'size': 10000,
            'query': {
                'term': metadata
            }
        }
        print("QUERY: ", query)
        s = self.from_dict(query)
        s = s.using(client).index('stormfront')
        response = s.execute()

        for hit in response:
            output["matching-files"].append(hit.meta.id)

        return json.dumps(output)

    def posts_author(self, author):
        query = {
            'from': 0, 'size': 10000,
            'query': {
                'match': {
                    'user_identifier': author
                }
            }
        }
        return self.from_dict(query)

    def posts_language(self, language):
        query = {
            'query': {
                'match': {
                    'language': language
                }
            }
        }
        return self.from_dict(query)

    def exec_query_dict(self, query, results_from, results_size):
        query['from'] = results_from
        query['size'] = results_size
        return self.from_dict(query)

    def selection_by_metadata(self, parameter_list):
        '''
        {
            'metadata': {
                'author': int(XXXX),
                'date': datetime(YYYY),
                'language': str(UZBECO),
             }
         }
        '''
        json_input = json.loads(parameter_list)
        query = json_input['metadata']
        return self.from_dict(query)

    def match_SNs(self, snominal, size):
        query = {
            "from": 0, "size": size,
            'query': {
                'match': {
                    'contents': snominal
                }
            }
        }

        s = self.from_dict(query)
        s = s.using(self.es_client).index('stormfront_paragraphs')
        response = s.execute()

        return response


class SearchParagraph(Search):
    '''
    Class for making paragraph searches
    '''

    def get_all(self, size):
        if size is None:
            size = 10000

        query = {
            "from": 0, "size": size,
            "query": {
                "match_all": {}
            }
        }
        # only get ids, otherwise `fields` takes a list of field names
        response = self.from_dict(query)
        return response

    def get_paragraphs_of_post(self, post_id):
        query = {
            "from": 0, "size": 10000,
            "query": {
                "term": {
                    "post_id": post_id
                }
            }
        }
        # only get ids, otherwise `fields` takes a list of field names
        response = self.from_dict(query)
        return response

    def query_gen(self, forms):
        '''
        query_string = "("+str(forms[0])+")"
        i = 1
        while i < len(forms):
            query_string += " OR ("+str(forms[i])+")"
            i += 1
        '''
        query_string = '" OR "'.join(forms)
        query_string = '"' + query_string + '"'
        query = {
            "min_score": 1.0,
            "from": 0, "size": 10000,
            "query": {
                "query_string": {
                    "default_field": "contents",
                    "query": query_string
                }
            }
        }
        print("QUERY(.query_gen()):", query)

        return query

    def query_gen_regexp(self, forms):
        regexp_or = str(forms[0])
        i = 1
        while i < len(forms):
            regexp_or += "|"+str(forms[i])
            i += 1
        regexp = ".* ("+regexp_or+") .*"
        print("regexp", regexp)

        query = {
            "min_score": 2.0,
            "from": 0, "size": 100,
            "query": {
                "regexp": {
                    "contents": {
                        "value": regexp,
                        "boost": 2.1
                    }
                }
            }
        }

        return query

    def get_paragraphs_with_kws(self, forms, client):

        query = self.query_gen(forms)
        s = self.from_dict(query)
        s = s.using(client).index('stormfront_paragraphs')
        response = s.execute()

        return response

    def match_language_exec(self):
        return self.query('match', language='es').execute()

    def match_metadata_doc_id(self, metadata):
        '''
        Searches in ElasticSearch with the matches specified by the parameter

        :param metadata: dict which the metadata that will be queried to ElasticSearch
        :return: dict with the matching results.
        '''
        output = []
        query = {
            'from': 0, 'size': 10000,
            'query': {
                'match': metadata
            }
        }
        response = self.from_dict(query)
        print(response.count())

        for hit in response:
            output.append(hit.meta.id)
        return output

    def posts_author(self, author):
        query = {
            'from': 0, 'size': 10000,
            'query': {
                'match': {
                    'author_id': author
                }
            }
        }
        return self.from_dict(query)

    def posts_language(self, language):
        query = {
            'query': {
                'match': {
                    'language': language
                }
            }
        }
        return self.from_dict(query)

    def exec_query_dict(self, query, results_from, results_size):
        query['from'] = results_from
        query['size'] = results_size
        return self.from_dict(query)

    def selection_by_metadata(self, parameter_list):
        '''
        {
            'metadata': {
                'author': int(XXXX),
                'date': datetime(YYYY),
                'language': str(UZBECO),
             }
         }
        '''
        json_input = json.loads(parameter_list)
        query = json_input['metadata']
        return self.from_dict(query)
