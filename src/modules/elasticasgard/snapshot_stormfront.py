import mysql.connector
import os
from os import walk
from datetime import datetime
import random
import chardet
import io
import json

from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search, Index
from elasticsearch_dsl import connections

import scripts.text_cleaner as txt_cleaner
from scripts.asgard_post import Post
from scripts.asgard_paragraph import Paragraph


es = Elasticsearch('localhost:59200')

# create repository
es.snapshot.create_repository(repository='test', body=snapshot_body)

index_body = {
    "indices": "stormfront,stormfront_paragraphs"
}
es.snapshot.create(repository='test',
                   snapshot='my_bsc_es_snapshot', body=index_body)
