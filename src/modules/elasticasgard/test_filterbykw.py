
from datetime import datetime
from elasticsearch_dsl import Document, Date, Integer, Keyword, Text
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search
from elasticsearch import Elasticsearch

from asgard_post import Post
from asgard_paragraph import Paragraph
from asgard_es_searches import SearchPost, SearchParagraph

import json

# create the mappings in elasticsearch
connections.create_connection(hosts=['localhost:59200'])
es_client = Elasticsearch('localhost:59200')
spagrph = SearchParagraph(
    using=es_client, index='stormfront_paragraphs', doc_type=Paragraph)


def test_filter_paragraphs_by_kw_forms():
    forms = ['anti-white']
    response = spagrph.get_paragraphs_with_kws(forms)

    print(response)
    print(spagrph.count())

    for hit in spagrph.scan():
        print(hit.data)


test_filter_paragraphs_by_kw_forms()
