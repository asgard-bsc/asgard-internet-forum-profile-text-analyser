import json
from datetime import datetime
from elasticsearch_dsl import Document, Date, Integer, Keyword, Text
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search
from elasticsearch import Elasticsearch


class SFUser(Document):
    # mysql parameters
    mysql_id = Integer()
    forum_id = Integer()
    scrape_id = Integer()
    add_date = Date()
    registered_date = Date()
    identifier = Text()
    username = Text()
    title = Text()
    profile = Text()
    lang_id_1 = Integer()
    lang_id_2 = Integer()
    lang_id_3 = Integer()
    lang_id_1_perc = Integer()
    lang_id_2_perc = Integer()
    lang_id_3_perc = Integer()

    # MERGE posts contents in a unique parameter

    # NPs

    class Index:
        name = 'stormfront_users'

    def save(self, **kwargs):
        self.created_at = datetime.now()
        return super(SFUser, self).save(**kwargs)

    def to_json(self):
        output = {}
        # TODO


'''
# create and save and Post
query = {
    "query": {
        "match": {
            "language": "es"
        }
    }
}
response = PostSearch.from_dict(query)

print response
print s.count()

for hit in response:
    print(type(hit))
    print(hit.meta.id)
    print(hit.author_id)
    print(hit.date)
    print(hit.language)
    print(hit.repository)
'''
# my_post = Post.get(id=25)


# Display cluster health
# print(connections.get_connection().cluster.health())
