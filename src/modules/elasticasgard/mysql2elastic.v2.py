import mysql.connector
import os
from os import walk
from datetime import datetime
import random
import chardet
import io
import json
from scripts.asgard_post import Post
from scripts.asgard_user import SFUser

from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search, Index
from elasticsearch_dsl import connections

import sys

'''
MySQL
'''


def convert(mydb):
    mycursor = mydb.cursor()
    query = ("SELECT * FROM stormfront.dbpost")
    mycursor.execute(query)
    for (size) in mycursor:
        print("counter:", size)


def rndDate():
    # random datetime
    year = random.randint(2015, 2018)
    month = random.randint(1, 12)
    day = random.randint(1, 28)
    hour = random.randint(0, 23)
    minute = random.randint(0, 59)
    sec = random.randint(0, 59)
    post_date = datetime(year, month, day, hour, minute, sec)
    return post_date


def convert_json_posts_to_es():
    '''
    [
        {
            "id" : 12837072,
            "forum_id" : 1,
            "scrape_id" : 769,
            "add_date" : "2015-02-09 15:12:32",
            "contents" : "<div class=\"monitor_post\"><div id=\"post_message_12125135\">\r\n\t\t\t\r\n\t\t\tHowdy!<br>\r\n<br>\r\nGlad you are here, looking forward to hearing your views.\r\n\t\t</div>\r\n\t\t</div>",
            "user_identifier" : "55084",
            "user_id" : 573148,
            "date" : "2014-05-26 03:55:00",
            "identifier" : 12125135,
            "topic_identifier" : 1042497,
            "topic_id" : 658155,
            "lang_id" : 19,
            "topic_start" : 0,
            "category_id" : 1343,
            "meta_data_id" : 0
        },
        ...
    ]
    '''
    with open('/tmp/local/posts_55084.json', 'r') as file:
        rows = json.load(file)
    for row in rows:
        p = Post(id=row['id'], forum_id=row['forum_id'], scrape_id=row['scrape_id'], add_date=row['add_date'], contents=row['contents'], user_identifier=row['user_identifier'], user_id=row['user_id'], date=row['date'],
                 identifier=row['identifier'], topic_identifier=row['topic_identifier'], topic_id=row['topic_id'], lang_id=row['lang_id'], topic_start=row['topic_start'], category_id=row['category_id'], meta_data_id=row['meta_data_id'])
        p.save()


def convert_json_dbuser_to_es():
    '''
    [
        {
            "id" : 227,
            "forum_id" : 14,
            "scrape_id" : 6,
            "add_date" : "2014-10-31 17:11:03",
            "identifier" : "12767",
            "username" : "bodyinaction",
            "registered_date" : "0000-00-00 00:00:00",
            "title" : "",
            "profile" : "",
            "lang_id_1" : 19,
            "lang_id_2" : 98,
            "lang_id_3" : -1,
            "lang_id_1_perc" : 68,
            "lang_id_2_perc" : 31,
            "lang_id_3_perc" : 0
            },
        ...
    ]
    '''

    ind = Index('stormfront_users', using=es_client, doc_type=SFUser)
    # ind.delete()
    ind.create()

    with open('/tmp/local/dbuser_all.json', 'r') as file:
        rows = json.load(file)
        for row in rows:
            try:
                if row['add_date'] == "0000-00-00 00:00:00":
                    row['add_date'] = rndDate().strftime('%Y-%m-%d 01:01:01')

                if row['registered_date'] == "0000-00-00 00:00:00":
                    row['registered_date'] = rndDate().strftime(
                        '%Y-%m-%d 01:01:01')

                user_es = SFUser(mysql_id=row['id'],
                                 forum_id=row['forum_id'],
                                 scrape_id=row['scrape_id'],
                                 add_date=row['add_date'],
                                 identifier=row['identifier'],
                                 username=row['username'],
                                 registered_date=row['registered_date'],
                                 title=row['title'],
                                 profile=row['profile'],
                                 lang_id_1=row['lang_id_1'],
                                 lang_id_2=row['lang_id_2'],
                                 lang_id_3=row['lang_id_3'],
                                 lang_id_1_perc=row['lang_id_1_perc'],
                                 lang_id_2_perc=row['lang_id_2_perc'],
                                 lang_id_3_perc=row['lang_id_3_perc'])

                user_es.save()
            except:
                print("EXCEPTION", row)


'''
ElasticSearch
'''
connections.create_connection(hosts=['localhost:59200'], timeout=20)
es_client = Elasticsearch('localhost:59200')

# add posts to ES
# convert_json_posts_to_es()

# add users to ES
convert_json_dbuser_to_es()
