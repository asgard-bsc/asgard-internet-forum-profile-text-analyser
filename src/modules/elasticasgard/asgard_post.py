import json
from datetime import datetime

from elasticsearch_dsl import Document, Date, Integer, Keyword, Text
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search
from elasticsearch import Elasticsearch


class Post(Document):

    # MySQL
    id = Integer()
    mysql_id = Integer()
    forum_id = Integer()
    scrape_id = Integer()
    add_date = Date()
    contents = Text()
    user_identifier = Integer()
    user_id = Integer()
    date = Date()
    identifier = Integer()
    topic_identifier = Integer()
    topic_id = Integer()
    lang_id = Integer()
    topic_start = Integer()
    category_id = Integer()
    meta_data_id = Integer()

    # mine
    contents_html = Text()

    # old
    author_id = user_identifier
    date = Date()
    filename = Text()
    language = Text()
    repository = Text()
    data = Text()

    class Index:
        name = 'stormfront'

    def save(self, **kwargs):
        self.created_at = datetime.now()
        return super(Post, self).save(**kwargs)

    def to_json(self):
        output = {}
        output['author_id'] = self.author_id
        output['date'] = self.date
        output['language'] = self.language
        output['repository'] = self.repository
        output['data'] = self.data


'''
# create and save and Post
query = {
    "query": {
        "match": {
            "language": "es"
        }
    }
}
response = PostSearch.from_dict(query)

print response
print s.count()

for hit in response:
    print(type(hit))
    print(hit.meta.id)
    print(hit.author_id)
    print(hit.date)
    print(hit.language)
    print(hit.repository)
'''
# my_post = Post.get(id=25)


# Display cluster health
# print(connections.get_connection().cluster.health())
