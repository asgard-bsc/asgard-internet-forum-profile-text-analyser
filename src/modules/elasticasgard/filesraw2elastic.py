import mysql.connector
import os
from os import walk
from datetime import datetime
import random
import chardet
import io
import json

from elasticsearch import Elasticsearch
from elasticsearch_dsl import Search, Index
from elasticsearch_dsl import connections

import scripts.text_cleaner as txt_cleaner
from scripts.asgard_post import Post
from scripts.asgard_paragraph import Paragraph


dir_posts_stormfront = "data/posts-stormfront/"

#os.environ["HOST_ES"] = "localhost:59200"

def rndDate():
    # random datetime
    year = random.randint(2015, 2018)
    month = random.randint(1, 12)
    day = random.randint(1, 28)
    hour = random.randint(0, 23)
    minute = random.randint(0, 59)
    sec = random.randint(0, 59)
    post_date = datetime(year, month, day, hour, minute, sec)
    return post_date

def execute_conversion():
    num_paragraphs = 0
    num_posts = 0
    num_words = 0
    (mypath, dirAuthors, fname) = next(os.walk(dir_posts_stormfront))
    print (dirAuthors)
    for author_id in dirAuthors:
        print (author_id)
        for (dirpath, dirnames, filenames) in os.walk(dir_posts_stormfront+author_id):
            print (author_id+' #posts= '+str(len(filenames)))
            for fname in filenames:
                with io.open(dirpath+'/'+fname, 'r') as openfile:
                    raw_data = openfile.read()
                    raw_data = raw_data.strip()
                    openfile.close()

                # random datetime
                post_date = rndDate()

                # clean the str
                raw_data = txt_cleaner.clean_text(raw_data)

                # ES: insert de post
                p = Post(author_id=author_id, date=post_date, filename=fname,
                         language="en", repository="stormfront", data=raw_data)
                p.save()
                post_id = p.meta.id

                # ES: insert de paragraph
                paragraphs = txt_cleaner.split_paragraphs(raw_data)
                for par in paragraphs:
                    num_words += len(par.split())
                    par_es = Paragraph(post_id=post_id, data=par)
                    par_es.save()

                # insert document in Index

                num_paragraphs += len(txt_cleaner.split_paragraphs(raw_data))
                num_posts += 1
    return (num_words, num_paragraphs, num_posts)


'''
ElasticSearch client
'''
connections.create_connection(hosts=['localhost:59200'], timeout=20)
es_client = Elasticsearch('localhost:59200')

'''
Script code
'''
Post.init()
#ind = Index('stormfrom', using=es_client, doc_type=Post)
#ind.delete()
#ind = Index('stormfrom_paragraphs', using=es_client, doc_type=Post)
#ind.delete()

ind = Index('stormfront', using=es_client, doc_type=Post)
ind.delete()
ind = Index('stormfront', using=es_client, doc_type=Post)
ind.create()
ind = Index('stormfront_paragraphs', using=es_client, doc_type=Paragraph)
ind.delete()
ind = Index('stormfront_paragraphs', using=es_client, doc_type=Paragraph)
ind.create()
ind = Index('stormfront_users', using=es_client, doc_type=Paragraph)
ind.delete()
ind = Index('stormfront_users', using=es_client, doc_type=Paragraph)
ind.create()
(num_words, num_paragraphs, num_posts) = execute_conversion()
print ("AVG (paragraphs/post) = ", num_paragraphs/num_posts)
print ("AVG (words/paragraph) = ", num_words/num_paragraphs)
