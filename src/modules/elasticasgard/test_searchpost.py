
from datetime import datetime
from elasticsearch_dsl import Document, Date, Integer, Keyword, Text
from elasticsearch_dsl.connections import connections
from elasticsearch_dsl import Search
from elasticsearch import Elasticsearch

from asgard_post import Post
from asgard_es_searches import SearchPost

import json
import pandas as pd

# create the mappings in elasticsearch
connections.create_connection(hosts=['localhost:59200'])
es_client = Elasticsearch('localhost:59200')
s = SearchPost(using=es_client, index='stormfront', doc_type=Post)


def test_get_match_posts_id():
    query_match = {'author_id': 55084}
    output = s.match_metadata_doc_id(query_match, es_client)

    print(type(output))
    print(len(output))
    print(output)


def test_from_dict():
    authors = [119,  163282,  170796,  173893,  179327,
               185316,  219096,  46987,  72763,  78077]

    # create and save and Post
    for a in authors:
        test_author(a)


def test_author(author):
    response = s.posts_author(author)

    print("search size: ", response.count())
    print(response.count())
    df = pd.DataFrame(columns=['doc_id', 'contents'])
    results = []

    for hit in response.scan():
        '''
        print(hit.user_identifier)
        print(hit.date)
        print(hit.meta.id)
        print(hit.contents)
        '''
        df = df.append({'doc_id': hit.meta.id, 'contents': hit.contents},
                       ignore_index=True)
    df.to_csv('./corpus/csv_55084.csv')


def test_from_json():
    query_json = {'metadata': {'author_id': 119, 'language': 'en'}}
    query_json_str = json.dumps(query_json)
    # query_json_dict = json.loads(query_json)

    print(query_json_str)
    response = s.selection_by_metadata(query_json_str)

    print(response)
    print(s.count())

    for hit in response:
        # print(hit.author_id)
        # print(hit.date)
        print(type(hit))
        print(hit.to_json())


test_author(55084)
# test_get_match_posts_id()
# test_from_dict()
# test_from_json()
'''
# my_post = Post.get(id=25)

# Display cluster health
# print(connections.get_connection().cluster.health())
'''
