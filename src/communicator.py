"""

This class undertakes all communication with the
Key-Value repository (Redis) and the external Queue (ActiveMQ)

In general, 4 structures are created in Redis:
The "tasks" list, which is a FIFO structure, working like a task queue
The "status" hash, for storing the enqueued and running task statuses
The "finished" hash, for storing the finished or failed task statuses
The "cancel" hash, for signaling the tool to cancel a task execution

There are also interfaces for sending PROCESSING, COMPLETED. FAILED and CANCELLED
messages to the external queue.

The only thing to update here is the REDIS_SERVER variable,
which is the docker service name for redis defined in the docker-compose.yml file.

"""

import json
import stomp
import redis
import logging

logging.basicConfig(level=logging.INFO)

REDIS_SERVER = "redis-server"

class Communicator():

    def __init__(self, srs):
        self.REDIS_TASKS_FIFO_KEY = srs + ':tasks'
        self.REDIS_STATUS_HASH_KEY = srs + ':status'
        self.REDIS_FINISHED_HASH_KEY = srs + ':finished'
        self.REDIS_FINISHED_FIFO_KEY_PREFIX = srs + ':unblock:'
        self.REDIS_CANCEL_HASH_KEY = srs + ':cancel'

        self.kv = redis.Redis(host=REDIS_SERVER, port=6379, db=0)
        self.credentials = None
        logging.info("Communicator initialized")

    def getNextTask(self):
        r_val = self.kv.brpop(self.REDIS_TASKS_FIFO_KEY)
        r_str = str(r_val[1].decode('utf8'))
        logging.info("Popped from kv: " + r_str)
        json_obj = json.loads(r_str)
        data = json_obj["data"]
        self.credentials = None
        if "brokerInfo" in data:
            self.credentials = data["brokerInfo"]
        return json_obj

    def pushTask(self, taskId, json_req):
        self.kv.lpush(self.REDIS_TASKS_FIFO_KEY, json.dumps(json_req))
        response = {"operation": "RES_STARTED", "reason": "Execution accepted"}
        self.kv.hset(self.REDIS_STATUS_HASH_KEY, taskId, json.dumps(response))
        logging.info("task inserted: " + taskId)
        return response

    def waitForTask(self, taskId):
        self.block(taskId)
        r_val = self.kv.hget(self.REDIS_FINISHED_HASH_KEY, taskId)
        response = None
        # Task is finished
        if r_val is not None:
            r_str = str(r_val.decode('utf8'))
            response = json.loads(r_str)
        logging.info("Got status: " + str(response))
        return response

    def block(self, taskId):
        # Only do that in SYNC mode
        if self.credentials is None:
            r_val = self.kv.brpop(self.REDIS_FINISHED_FIFO_KEY_PREFIX + taskId)

    def unblock(self, taskId):
        # Only do that in SYNC mode
        if self.credentials is None:
            self.kv.lpush(self.REDIS_FINISHED_FIFO_KEY_PREFIX + taskId, taskId)

    def getTaskStatus(self, taskId):
        r_val = self.kv.hget(self.REDIS_STATUS_HASH_KEY, taskId)

        # Task is accepted or running
        if r_val is not None:
            r_str = str(r_val.decode('utf8'))
            response = json.loads(r_str)
        else:
            r_val = self.kv.hget(self.REDIS_FINISHED_HASH_KEY, taskId)
            # Task is finished
            if r_val is not None:
                r_str = str(r_val.decode('utf8'))
                response = json.loads(r_str)
            else:
                # Task not found
                response = {"operation": "RES_FAILED", "reason": "Task id not found", "message": "Task id not found"}

        logging.info("Got status: " + str(response))
        return response

    def cancelTask(self, taskId):
        # We first check if the task id is there
        # Then we set a cancel flag
        # If the task is running, the tool will stop it asap
        # If the task is waiting, the tool will stop it when it pops out of the task queue

        r_val = self.kv.hget(self.REDIS_STATUS_HASH_KEY, taskId)
        if r_val is None:
            response = {"operation": "RES_FAILED", "reason": "Task id not found", "message": "Task id not found"}
        else:
            self.kv.hset(self.REDIS_CANCEL_HASH_KEY, taskId, "True")
            response = {"operation": "RES_CANCELLED", "reason": "Execution cancelled"}
            self.kv.hset(self.REDIS_STATUS_HASH_KEY, taskId, json.dumps(response))

        logging.info("Got status: " + str(response))
        return response

    def isCancelled(self, taskId):
        r_val = self.kv.hget(self.REDIS_CANCEL_HASH_KEY, taskId)
        if r_val:
            return r_val.decode('utf8')

    def add_json_to_amq(self, message):
        if self.credentials is not None:
            username = self.credentials['brokerUsername']
            password = self.credentials['brokerPassword']
            queue = self.credentials['brokerQueue']
            host = self.credentials['brokerURL']
            port = 61613
            if host.startswith("tcp://"):
                host = host[6:]
                host = host.split(":")[0]
            elif host.startswith("stomp://"):
                host = host[8:]
                port = int(host.split(":")[1])
                host = host.split(":")[0]
            port = self.credentials.get('brokerPort', port)
            conn = stomp.Connection([(host, port)])
            conn.connect(username, password, wait=True)
            conn.send(body=json.dumps(message), destination=queue)
            conn.disconnect()

    def send_inf_processing_message(self, taskId, reason, message=None, percentage=None, eta=None):
        msg = {"operation": "INF_PROCESSING", "reason": reason}
        if message is not None:
            msg["message"] = message
        if percentage is not None:
            msg["percentage"] = percentage
        if eta is not None:
            msg["eta"] = eta

        self.add_json_to_amq(msg)

        self.kv.hset(self.REDIS_STATUS_HASH_KEY, taskId, json.dumps(msg))


    # There are only 3 ways to finish a task: completed, failed or cancelled

    def send_res_completed_message(self, taskId, reason, data):
        msg = {"operation": "RES_COMPLETED", "reason": reason, "data": data}
        self.add_json_to_amq(msg)

        self.kv.hdel(self.REDIS_STATUS_HASH_KEY, taskId)
        self.kv.hset(self.REDIS_FINISHED_HASH_KEY, taskId, json.dumps(msg))

        # Unlock if locked
        self.unblock(taskId)

    def send_res_failed_message(self, taskId, reason, message):
        msg = {"operation": "RES_FAILED", "reason": reason, "message": message}
        self.add_json_to_amq(msg)

        self.kv.hdel(self.REDIS_STATUS_HASH_KEY, taskId)
        self.kv.hset(self.REDIS_FINISHED_HASH_KEY, taskId, json.dumps(msg))

        # Unlock if locked
        self.unblock(taskId)

    def send_res_cancelled_message(self, taskId, reason):
        msg = {"operation": "RES_CANCELLED", "reason": reason}
        self.add_json_to_amq(msg)

        self.kv.hdel(self.REDIS_STATUS_HASH_KEY, taskId)
        self.kv.hdel(self.REDIS_CANCEL_HASH_KEY, taskId)
        self.kv.hset(self.REDIS_FINISHED_HASH_KEY, taskId, json.dumps(msg))

        # Unlock if locked
        self.unblock(taskId)
