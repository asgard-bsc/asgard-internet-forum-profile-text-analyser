# Use the following if your tool runs on Python 3
FROM python:3.7

#my project
RUN pip --no-cache-dir install textblob \
       nltk \
       gensim \
       chardet \ 
       pandas \ 
       beautifulsoup4 \ 
       spacy \ 
       umap-learn \
       matplotlib
       #glove_python

# server / environment libraries
RUN pip --no-cache-dir install stomp.py redis flask 

RUN python -m textblob.download_corpora
RUN python -m spacy download en_core_web_sm
RUN [ "python", "-c", "import nltk; nltk.download('wordnet'); nltk.download('stopwords');nltk.download('punkt');nltk.download('averaged_perceptron_tagger')" ]

#gui
RUN pip --no-cache-dir install git+https://github.com/webpy/webpy#egg=web.py

#async service
RUN pip --no-cache-dir install \
       flask \
       gevent \
       pillow\
       flask-cors\
       stomp.py \
       redis

RUN mkdir -p /super

COPY ./src/ /super/

WORKDIR /super

CMD ["python", "-u", "run_service_async.py"]

#CMD ["/bin/bash", "./start_services.sh"]