# Internet Forum Profile Text Analyser (BSC)

## Description of the tool
The internet-forum-profile-text-analyser (IFPTA henceforth) is a tool that filters huge amounts of texts in order to get data that are relevant to profile users of internet fora . The IFPTA tool is useful to model the users' opinions, get bio information and track the events they participate in. These are pieces of information that can even have a predictive value.

The document filtering is applied according to these criteria:

- The topics the texts talk about
- The sentiment polarity expressed in the documents (neutral, positive, negative)
- The terms that are representative of the topics expressed in the documents
- The events referred to in the documents

The IFPTA tool provides documents that are relevant according to one or more criteria in the document filtering.

As an example, let's assume that the IFPTA tool is able to detect a topic whose top relevant terms are related to weapons. Besides, let's assume that the user of the tool wants to read documents belonging to the 'weapon' topic whose polarity is negative and contains the top relevant terms as well. The IFPTA tool is able to provide these documents.

The IFPTA detects the events referred to in the documents. The event filtering and its interaction with the topic and polarity filtering is currently being discussed in developed. Event filtering is expected to be fully implemented by the Lisbon Hackaton

## Tool status

Release: 20200615rc1stable

Input: 

- file with raw text

Output: 

- JSON file with:
  - GUI visualization URL
  - main topic
  - LDA results with:
    -  most relevant terms tag-cloud
    -  events detected in the document

GUI: Yes

## Documentation

We have split the documentation into two separated READMEs:

* **[Technical README](README_technical_part.md)** about how to set up the tool
* **[User README](README_user_part.md)** about how to use the tool


If we have omitted any relevant information that prevents you from understanding or running the tool following this documentation, you can contact us using the maintainers list below.

## Maintainers

| Email address                                     |
|---------------------------------------------------|
| [@Sergio Mendoza] (mailto:sergio.mendoza@bsc.es)  |
| [@Joaquim More] (mailto:joaquim.morelopez@bsc.es) |
| [@Albert Farres] (mailto:XXXX@bsc.es)             |


## License

[APACHE LICENSE, VERSION 2.0](LICENSE.txt)
